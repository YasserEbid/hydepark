<?php
/**
 * match 2 selected options for html select
 * @param string $option1 first option value
 * @param string $option2 second option value
 * @return string  return value selected=selected
 */
function selected($option1, $option2) {
    if(is_array($option2))
    {
        if(in_array($option1 ,$option2))
                return "selected='selected'";
    }
    else		
    if ($option1 == $option2) {
        return "selected='selected'";
    }
}

function checked($option1, $option2) {	
    if ($option1 == $option2) {
        return "checked ";
    }
}


/**
 * create thumb from an image 
 * @param type $src the iamge source
 * @param type $dest the thumb desination 
 * @param type $desired_width  the desired  width of thumb
 */
function make_thumb($src, $dest, $desired_width , $desired_height='auto') {
	if (!file_exists($src))
	return false ;
    //$ext = @substr($src, -3);
	$ext =  @pathinfo($src, PATHINFO_EXTENSION); 
    /* read the source image */
    //if($ext!="JPG" || $ext!="JPEG" || $ext!="jpg")
    if ($ext == "GIF" || $ext == "gif")
        $source_image = @imagecreatefromgif($src);
    elseif ($ext == "PNG" || $ext == "png")
        $source_image = @imagecreatefrompng($src);
	else
        $source_image = @imagecreatefromjpeg($src);


    $width = @imagesx($source_image);
    $height = @imagesy($source_image);

	if ($desired_height=='auto')
	{
		/* find the "desired height" of this thumbnail, relative to the desired width  */
		$desired_height = @floor($height * ($desired_width / $width));
	}
    
    /* create a new, "virtual" image */
    $virtual_image = @imagecreatetruecolor($desired_width, $desired_height);

    /* copy source image at a resized size */
    @imagecopyresized($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

    /* create the physical thumbnail image to its destination */
    @imagejpeg($virtual_image, $dest);
}
?>