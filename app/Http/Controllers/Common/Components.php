<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Components extends Controller
{

    public static function uploader($image = './images/avatar_image.png', $css_classes = '')
    {

        if($image == '' || !file_exists($image))
            $image = './images/avatar_image.png';
        $html = file_get_contents(__DIR__.'/uploader.html');
        $html = str_replace('profile_picture', $image, $html);
        $html = str_replace('{css_classes}', $css_classes, $html);

        return $html;
    }

    public static function uploaderFile($image = './images/avatar_image.png', $css_classes = '')
    {

        if($image == '' || !file_exists($image))
            $image = './images/avatar_image.png';
        $html = file_get_contents(__DIR__.'/uploader_file.html');
        $html = str_replace('profile_picture', $image, $html);
        $html = str_replace('{css_classes}', $css_classes, $html);

        return $html;
    }

    public static function tankers($required = 'required')
    {
        $tankers = \App\Models\Tankers::where('is_banned', 0)->get();
        $html = '<div class="form-group">                                
                    <select class="form-control selectpicker" name="tanker_id" data-live-search="true" '.$required.'>';
        $html.='<option value="">Search & choose Tanker</option>';
        foreach($tankers as $tanker)
        {
            if(isset($_GET['tanker_id']))
            {
                $html.='<option value="'.$tanker->id.'" '.Helpers::selected($_GET['tanker_id'], $tanker->id).'>'.$tanker->name.'</option>';
            }
            else
            {
                $html.='<option value="'.$tanker->id.'" >'.$tanker->name.'</option>';
            }
        }
        $html.='</select>
                </div>';
        return $html;
    }

    public static function skills($required = '', $value = '')
    {
        $skills = \App\Models\Skills::all();
        $html = '<select name="skill_id" class="form-control selectpicker" data-live-search="true" '.$required.'>';
        $html.='<option value="">Search & choose skill</option>';
        foreach($skills as $skill)
        {
            if(isset($_POST['skill_id']))
            {
                $html.='<option value="'.$skill->id.'" '.Helpers::selected($skill->id, $_POST['skill_id']).'>'.$skill->name.'</option>';
            }
            else if($value != '')
            {
                $html.='<option value="'.$skill->id.'" '.Helpers::selected($skill->id, $value).'>'.$skill->name.'</option>';
            }
            else
            {
                $html.='<option value="'.$skill->id.'">'.$skill->name.'</option>';
            }
        }
        $html.="</select>";
        return $html;
    }

    public static function addGoogleMap()
    {
        $html = file_get_contents(__DIR__."/googlemap.html");
        return $html;
    }

    public static function addGoogleMapShow($lat, $lng)
    {
        $html = file_get_contents(__DIR__."/googlemapshow.html");
        $html = str_replace("{lat}", $lat, $html);
        $html = str_replace('{lng}', $lng, $html);
        return $html;
    }

    public static function editGoogleMap($lat, $lng, $img, $desc, $zoom)
    {
        $html = file_get_contents(__DIR__."/googlemapedit.html");
        $html = str_replace("{lat}", $lat, $html);
        $html = str_replace('{lng}', $lng, $html);
        $html = str_replace('{img}', $img, $html);
        $html = str_replace('{desc}', $desc, $html);
        $html = str_replace('{zoom}', $zoom, $html);
        return $html;
    }
    
    public static function deleteRow($row_id, $url)
    {
        $html = file_get_contents(__DIR__."/delete_row.html");
//        $html = str_replace("{delete}", $nameAction, $html);
        $html = str_replace("{row_id}", $row_id, $html);
        $html = str_replace('{url}', $url, $html);
        return $html;
    }
    
    public static function imageFancy($name, $url)
    {
        $html = file_get_contents(__DIR__."/image_fancy.html");
        $html = str_replace("{name}", $name, $html);
        $html = str_replace('{url}', $url, $html);
        return $html;
    }
    
    public static function checkboxInput($is_active, $id, $url,$checked)
    {
        $html = file_get_contents(__DIR__."/checked.html");
        $html = str_replace("{is_active}", $is_active, $html);
        $html = str_replace('{id}', $id, $html);
        $html = str_replace('{url}', $url, $html);
        $html = str_replace('{checked}', $checked, $html);
        return $html;
    }
    
    public static function radioInput($name, $id, $url,$checked)
    {
        $html = file_get_contents(__DIR__."/radio.html");
        $html = str_replace("{name}", $name, $html);
        $html = str_replace('{id}', $id, $html);
        $html = str_replace('{url}', $url, $html);
        $html = str_replace('{checked}', $checked, $html);
        return $html;
    }

}
