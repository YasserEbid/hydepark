<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Intervention\Image\Image;
use Intervention\Image\Facades\Image;

class Helpers extends Controller
{

    public static function selectedPost($name, $value, $option2)
    {
        if(isset($_POST[$name]))
        {
            if($_POST[$name] == $option2)
            {
                return "selected='selected'";
            }
        }
        else
        {
            if($value == $option2)
            {
                return "selected='selected'";
            }
        }
    }

    public static function selectedGet($name, $value, $option2)
    {
        if(isset($_GET[$name]))
        {
            if($_GET[$name] == $option2)
            {
                return "selected='selected'";
            }
        }
        else
        {
            if($value == $option2)
            {
                return "selected='selected'";
            }
        }
    }

    public static function selectedArray($option1, $option2)
    {
        if(is_array($option2))
        {
            if(in_array($option1, $option2))
                return "selected='selected'";
        }
        else
        if($option1 == $option2)
        {
            return "selected='selected'";
        }
    }

    public static function checked($option1, $option2)
    {
        if($option1 == $option2)
        {
            return "checked='checked'";
        }
    }

    public static function checkedPost($name, $value, $option2)
    {
        if(isset($_POST[$name]))
        {
            if($_POST[$name] == $option2)
            {
                return "checked='checked'";
            }
        }
        else
        {
            if($value == $option2)
            {
                return "checked='checked'";
            }
        }
    }

    public static function checkedGet($name, $value, $option2)
    {
        if(isset($_GET[$name]))
        {
            if($_GET[$name] == $option2)
            {
                return "checked='checked'";
            }
        }
        else
        {
            if($value == $option2)
            {
                return "checked='checked'";
            }
        }
    }

    public static function genrateRegistartionCode()
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*';
        $string = '';
        for($i = 0; $i < 9; $i++)
        {
            $string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $string;
    }

    public static function createThumb($path, $image, $dest, $width = 150, $height = 150)
    {
        $img = Image::make($path.'/'.$image)->resize($width, $height);
        $img->save($dest.'/'.$image);
        return $dest.'/'.$image;
    }

    public static function makeThumb($src, $dest, $desired_width, $desired_height = 'auto')
    {
        //if (!file_exists($src))
        //    return false ;
        // image type
        $ext = exif_imagetype($src);

        /* read the source image */
        //if($ext!="JPG" || $ext!="JPEG" || $ext!="jpg")
        if($ext == '1') //GIF
            $source_image = @imagecreatefromgif($src);
        elseif($ext == "2") //jpg
            $source_image = @imagecreatefromjpeg($src);
        elseif($ext == "3") //png
            $source_image = @imagecreatefrompng($src);
        else
            $source_image = $src;
        $width = @imagesx($source_image);
        $height = @imagesy($source_image);
        if($desired_height == 'auto')
        {
            /* find the "desired height" of this thumbnail, relative to the desired width  */
            $desired_height = @floor($height * ($desired_width / $width));
        }

        /* create a new, "virtual" image */
        $virtual_image = @imagecreatetruecolor($desired_width, $desired_height);
        /* copy source image at a resized size */
        @imagecopyresized($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
        /* create the physical thumbnail image to its destination */
        @imagejpeg($virtual_image, $dest);
    }

    public static function detectHashtags($text)
    {
        $matches = [];
        preg_match_all('/#([^\s]+)/', $text, $matches);

        if(count($matches) > 0)
        {
            return $matches;
        }
        else
        {
            return false;
        }
    }

    public static function issetPost($name, $value)
    {
        if(isset($_POST[$name]))
        {
            return $_POST[$name];
        }
        else
        {
            return $value;
        }
    }

    public static function issetGet($name, $value)
    {
        if(isset($_GET[$name]))
        {
            return $_GET[$name];
        }
        else
        {
            return $value;
        }
    }

    public static function CalculateTime($time)
    {
        $starttime = $time;
        $stoptime = date('Y-m-d H:i:s');
        $diff = (strtotime($stoptime) - strtotime($starttime));
        $total = $diff / 60;
        return $result_hours = sprintf("%02dH %02dM", floor($total / 60), $total % 60);
    }

    public static function CalculateDate($date)
    {
        $timestamp = strtotime($date);
        $datetime1 = date_create(date('Y-m-d', $timestamp));
        $datetime2 = date_create(date('Y-m-d'));
        $interval = date_diff($datetime1, $datetime2);
        return $interval->format('%a Days');
    }

}
