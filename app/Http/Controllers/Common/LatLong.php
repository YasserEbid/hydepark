<?php

namespace App\Http\Controllers\Common;

class LatLong
{

    static function get_lat_long($address)
    {
        preg_match('#@(\d+.\d+),(\d+.\d+)#', $_POST['map'], $latlng);
        
        $lat = $latlng[1];
        $long = $latlng[2];

        return [$lat, $long];
    }
}
