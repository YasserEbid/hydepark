<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\OurDevelopment;
use App\Models\OurDevelopmentAttachments;

class OurDevelopmentController extends BackendController
{

    public function anyIndex()
    {
        $data['developments'] = OurDevelopment::orderBy('id', 'asc')->get();

        return view('backend.our-development.index', $data);
    }

    public function anyCreate()
    {
        $data = [];

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $development = new OurDevelopment;

            if($development->validate($_POST))
            {
                if(isset($_POST['image']) && $_POST['image'] != '')
                {
                    rename('./uploads/temp/'.$_POST['image'], './uploads/'.$_POST['image']);
                }

                $_POST['created_by'] = $_SESSION['id'];
                $development->fill($_POST);
                $development->save();
                return redirect('backend/our-development/create')->with('success', 'Successfully insert');
            }
            else
            {
                $data['validate_errors'] = $development->errors();
            }
        }

        return view('backend.our-development.edit', $data);
    }

    public function anyEdit($id)
    {
        $data['development'] = OurDevelopment::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $development = $data['development'];

            if($development->validate($_POST))
            {
                if(isset($_POST['image']) && file_exists('./uploads/temp/'.$_POST['image']) && $_POST['image'] !='')
                {
                    rename('./uploads/temp/'.$_POST['image'], './uploads/'.$_POST['image']);
                }

                $_POST['created_by'] = $_SESSION['id'];
                $development->fill($_POST);
                $development->save();
                return redirect('backend/our-development/edit/'.$id)->with('success', 'Successfully update');
            }
            else
            {
                $data['validate_errors'] = $development->errors();
            }
        }

        return view('backend.our-development.edit', $data);
    }

    public function anyAttachments()
    {
        $data['attachments'] = OurDevelopmentAttachments::paginate(25);


        return view('backend.our-development.attachments.index', $data);
    }

    public function anyCreateAttachments()
    {
        $data['our_developement'] = OurDevelopment::all();

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $attachments = new OurDevelopmentAttachments;

            if($attachments->validate($_POST))
            {
                if(isset($_POST['image']) && $_POST['image'] != '')
                {
                    rename('./uploads/temp/'.$_POST['image'], './uploads/'.$_POST['image']);
                }
                
                if(isset($_POST['file']) && $_POST['file'] != '')
                {
                    rename('./uploads/temp/'.$_POST['file'], './uploads/'.$_POST['file']);
                }

                $_POST['created_by'] = $_SESSION['id'];
                $attachments->fill($_POST);
                $attachments->save();
                return redirect('backend/our-development/create-attachments/')->with('success', 'Successfully insert');
            }
            else
            {
                $data['validate_errors'] = $attachments->errors();
            }
        }

        return view('backend.our-development.attachments.create', $data);
    }

    public function anyEditAttachments($id)
    {
        $data['our_developement'] = OurDevelopment::all();

        $data['attachments'] = OurDevelopmentAttachments::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $attachments = $data['attachments'];

            if($attachments->validate($_POST))
            {
                if(isset($_POST['image']) && file_exists('./uploads/temp/'.$_POST['image']))
                {
                    rename('./uploads/temp/'.$_POST['image'], './uploads/'.$_POST['image']);
                }
                if(isset($_POST['file']) && file_exists('./uploads/temp/'.$_POST['file']))
                {
                    rename('./uploads/temp/'.$_POST['file'], './uploads/'.$_POST['file']);
                }

                $_POST['created_by'] = $_SESSION['id'];
                $attachments->fill($_POST);
                $attachments->save();
                return redirect('backend/our-development/edit-attachments/'.$id)->with('success', 'Successfully update');
            }
            else
            {
                $data['validate_errors'] = $attachments->errors();
            }
        }

        return view('backend.our-development.attachments.edit', $data);
    }

    public function anyDeleteAttachments($id)
    {
        $attachments = OurDevelopmentAttachments::find($id);

        $file_path = 'uploads/'.$attachments->image;
        if(file_exists($file_path))
            unlink($file_path);

        $attachments->delete();

        return redirect('./backend/our-development/attachments')->with('success', 'Successfully delete');
    }

}
