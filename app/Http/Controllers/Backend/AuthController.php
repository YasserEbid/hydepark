<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\BackendUsers;
use App\Models\Roles;
use App\Http\Controllers\Helpers\Helpers;

class AuthController extends BackendController
{

    public function anyIndex()
    {
        return redirect()->to('./backend/auth/logout');
    }

    public function anyLogin()
    {
        $data = array();
        if(isset($_POST['email']) && isset($_POST['password']))
        {
            $user = BackendUsers::
                    where('password', md5($_POST['password']));
            $user = $user->where(function($q)
                    {
                        $q->where('name', $_POST['email']);
                        $q->Orwhere('email', $_POST['email']);
                        $q->Orwhere('phone', $_POST['email']);
                    })
                    ->first();

            if(is_object($user))
            {
                $_SESSION['id'] = $user->id;
                $_SESSION['name'] = $user->name;
                $_SESSION['email'] = $user->email;
                $_SESSION['phone'] = $user->phone;
                $_SESSION['image'] = $user->image;
                $rule = Roles::find($user->role_id);
                if($rule)
                {
                    $_SESSION['role_name'] = $rule->name;
                    if($rule->is_super == 1)
                        $_SESSION['is_super'] = 1;
                }

                $_SESSION['role_id'] = $user->role_id;
                $_SESSION['admin'] = 1;
                if($_SESSION["admin"] === 1)
                {
                    if($user->is_active == 0)
                    {
                        $data['error'] = "Your account is blocked";
                    }
                    else
                    {
                        return redirect()->to('./backend/home');
                    }
                }
                else
                {
                    //return redirect()->guest('./front/home');
                }
            }
            else
            {

                $data['error'] = "Wrong email or password";
            }
        }
        return view('backend.auth.login', $data);
    }

    public function anyLogout()
    {
        if(!empty($_SESSION))
        {
            foreach($_SESSION as $key => $value)
            {
                unset($_SESSION[$key]);
            }

            session_unset();
            session_destroy();
        }
        return redirect("./backend/auth/login");
    }

    public function anyForgotPassword()
    {
        $data = [];

        if(isset($_POST['send']))
        {
            session(['code' => \App\Http\Controllers\Common\Helpers::genrateRegistartionCode()]);
            echo session('code');
            $user = BackendUsers::where('email', $_POST['email'])->first();

            if(is_object($user))
            {
                session(['email' => $user->email]);

//                $data = array(
//                    'reciver_name' => $user->name,
//                    'reciver_code' => session('code'),
//                    'reciver_image' => url('uploads/backend_users/'.$user->image),
//                    'url_login' => url('backend/auth/login')
//                );
//
//                $to = $user->email;
//
//                $subject = 'Forgot password';
//
//                $message = response()->view('emails.forgotpassword', $data);
//
//                $headers = 'MIME-Version: 1.0'."\r\n";
//                $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
//
//                $headers .= 'From: Ropot Backend <info@tarekmagdy.com>'."\r\n";
//
//                $mail = mail($to, $subject, $message, $headers);
                $mail = true;

                if($mail == TRUE)
                {
                    $data['warning'] = 'CMS send to you mail in it code <br /> Copy code from your mail <br /> And paste in here';

                    return view('backend.auth.restcode', $data);
                }
                else
                {
                    $data['error'] = "Failed send email";
                }
            }
            else
            {
                $data['error'] = "Wrong mail";
            }
        }

        if(isset($_POST['check']))
        {
            if($_POST['code'] == session('code'))
            {
                return view('backend.auth.resetpassword', $data);
            }
            else
            {
                $data['error'] = 'Wrong code';
                return view('backend.auth.restcode', $data);
            }
        }

        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            $user = BackendUsers::where('email', session('email'))->first();
            $_POST['name'] = $user->name;
            $_POST['email'] = $user->email;
            $_POST['phone'] = $user->phone;
            $_POST['image'] = $user->image;
            if($user->validate($_POST))
            {
                unset($_POST['save']);
                unset($_POST['c_password']);
                $_POST['password'] = md5($_POST['password']);
                $user->fill($_POST);
                $user->save();
                if(session() != NULL)
                {
                    session()->flush();
                }
                return redirect('backend/auth/login');
            }
            else
            {
                foreach($user->errors() as $error)
                {
                    $validate[] = $error.'<br />';
                }
                $data['validate_errors'] = $validate;

                return view('backend.auth.resetpassword', $data);
            }
        }

        return view('backend.auth.forgotpassword', $data);
    }

    public function anyLockedscreen()
    {
        if(empty($_SESSION))
            return \Redirect::to('./backend/auth/login')->send();


        return view('backend.auth.lockedscreen');
    }

}
