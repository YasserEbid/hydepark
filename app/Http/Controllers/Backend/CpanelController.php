<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Cpanel;
use App\Http\Controllers\Common\xmlapi;

class CpanelController extends BackendController
{

    /**
     * 
     * Cpanel
     * 
     * */
    public function anyIndex()
    {
        $data['cpanels'] = Cpanel::orderBy('id', 'desc')->paginate(10);

        return view('backend.cpanel.index', $data);
    }

    public function anyCreateCpanel()
    {
        $data = [];

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $cpanel = new Cpanel;

            if($cpanel->validate($_POST))
            {
                $_POST['created_by'] = $_SESSION['id'];
                $cpanel->fill($_POST);
                $cpanel->save();
                return redirect(\URL::Current())->with('success', 'Successfully insert');
            }
            else
            {
                $data['validate_errors'] = $cpanel->errors();
            }
        }

        return view('backend.cpanel.create', $data);
    }

    public function anyEditCpanel($cpanel_id)
    {
        $data['cpanel'] = Cpanel::find($cpanel_id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $cpanel = $data['cpanel'];

            if($cpanel->validate($_POST))
            {
                $_POST['created_by'] = $_SESSION['id'];
                $cpanel->fill($_POST);
                $cpanel->save();
                return redirect(\URL::Current())->with('success', 'Successfully update');
            }
            else
            {
                $data['validate_errors'] = $cpanel->errors();
            }
        }

        return view('backend.cpanel.edit', $data);
    }

    public function anyDeleteCpanel($cpanel_id)
    {
        $cpanel = Cpanel::find($cpanel_id);

        $cpanel->delete();

        return redirect('backend/cpanel')->with('success', 'Successfully delete');
    }

    /**
     * 
     * Cpanel Accounts
     * 
     * */
    public function anyAccounts()
    {
        $data['cpanels'] = Cpanel::all();

        if(isset($_GET['find']))
        {
            $cpanel = Cpanel::find($_GET['cpanel_id']);

            $xmlapi = new xmlapi($cpanel->ip);
            $xmlapi->password_auth($cpanel->username, $cpanel->password);
            $xmlapi->set_port($cpanel->port);
            $xmlapi->set_output('json');
            $xmlapi->set_debug(1);
//            $output = $xmlapi->api1_query($cpanel->username, "SubDomain", "cplistsubdomains");
//            $output = $xmlapi->api1_query($cpanel->username, "Mysql", "listdbs");
//            $xmlapi->api1_query($cpuser, "Mysql", "adduserdb" array($newdb, $newuser, 'USAGE'));
//            print $xmlapi->api2_query($account, "Email", "addforward", array(domain=>"kin.org", email=>$source_email, fwdopt=>"fwd", fwdemail=>$dest_email) );
//            $xmlapi->api1_query($user, 'Email','delforward', array('email_fake@test.com=box_i_check@personal.net') );
//            $xmlapi->api1_query($cpuser, "Mysql", "adddb", array($newdb));
//            $xmlapi->api1_query($cpuser, "Mysql", "adduser", array($newuser, $newpass));
//            $xmlapi->api1_query($cpuser, "Mysql", "addusertodb" array($newdb, $newuser, 'SELECT INSERT UPDATE DELETE' ));


            $output = json_decode($output);
            dd($output);
            $data['accounts'] = $output->cpanelresult->data;
        }
        else
        {
            $data['accounts'] = [];
        }

        return view('backend.cpanel.accounts.index', $data);
    }

    /**
     * 
     * Cpanel Emails
     * 
     * */
    public function anyEmails()
    {
        $data['cpanels'] = Cpanel::all();

        if(isset($_GET['find']))
        {
            $cpanel = Cpanel::find($_GET['cpanel_id']);

            $xmlapi = new xmlapi($cpanel->ip);
            $xmlapi->password_auth($cpanel->username, $cpanel->password);
            $xmlapi->set_output('json');
            $xmlapi->set_port($cpanel->port);
            $output = $xmlapi->api2_query($cpanel->username, "Email", "listpopswithdisk");
            $output = json_decode($output);
            $data['emails'] = $output->cpanelresult->data;
        }
        else
        {
            $data['emails'] = [];
        }

        return view('backend.cpanel.emails.index', $data);
    }

    public function anyCreateEmail()
    {
        $data['cpanels'] = Cpanel::all();

        if(isset($_POST['save']))
        {
            $cpanel = Cpanel::find($_POST['cpanel_id']);

            $xmlapi = new xmlapi($cpanel->ip);
            $xmlapi->password_auth($cpanel->username, $cpanel->password); //the server login info for the user you want to create the emails under
            $xmlapi->set_output('json');
            $xmlapi->set_port($cpanel->port);
            if(@$_POST['unlimited_quota'] == 0)
            {
                $qouta = $_POST['quota'];
            }
            else
            {
                $qouta = 'unlimited';
            }
            $params = [
                'domain' => $cpanel->domain,
                'email' => $_POST['email'],
                'password' => $_POST['password'],
                'quota' => $qouta //quota is in MB
            ];

            $addEmail = json_decode($xmlapi->api2_query($cpanel->username, "Email", "addpop", $params), true);

            if($addEmail['cpanelresult']['data'][0]['result'])
            {
                return redirect(\URL::Current())->with('success', 'Successfully insert');
            }
            else
            {
                return redirect(\URL::Current())->with('validate_errors', $addEmail['cpanelresult']['data'][0]['reason']);
            }
        }

        return view('backend.cpanel.emails.create', $data);
    }

    public function anyChangePasswordEmail($cpanel_id, $user)
    {
        $data = [];

        if(isset($_POST['save']))
        {
            $cpanel = Cpanel::find($cpanel_id);

            $xmlapi = new xmlapi($cpanel->ip);
            $xmlapi->password_auth($cpanel->username, $cpanel->password); //the server login info for the user you want to create the emails under
            $xmlapi->set_output('json');
            $xmlapi->set_port($cpanel->port);

            $params = [
                'domain' => $cpanel->domain,
                'email' => $user,
                'password' => $_POST['password'],
//                'quota' => $qouta //quota is in MB
            ];

            $changePassword = json_decode($xmlapi->api2_query($cpanel->username, "Email", "passwdpop", $params), true);

            if($changePassword['cpanelresult']['data'][0]['result'])
            {
                return redirect(\URL::Current())->with('success', 'Successfully change password');
            }
            else
            {
                return redirect(\URL::Current())->with('validate_errors', $changePassword['cpanelresult']['data'][0]['reason']);
            }
        }

        return view('backend.cpanel.emails.change_password', $data);
    }

    public function anyEditQouta($cpanel_id, $user)
    {
        $data = [];

        if(isset($_POST['save']))
        {
            $cpanel = Cpanel::find($cpanel_id);

            $xmlapi = new xmlapi($cpanel->ip);
            $xmlapi->password_auth($cpanel->username, $cpanel->password); //the server login info for the user you want to create the emails under
            $xmlapi->set_output('json');
            $xmlapi->set_port($cpanel->port);

            if(@$_POST['unlimited_quota'] == 0)
            {
                $qouta = $_POST['quota'];
            }
            else
            {
                $qouta = 'unlimited';
            }

            $params = [
                'domain' => $cpanel->domain,
                'email' => $user,
//                'password' => $cpanel->password,
                'quota' => $qouta //quota is in MB
            ];

            $editQouta = json_decode($xmlapi->api2_query($cpanel->username, "Email", "editquota", $params), true);

            if($editQouta['cpanelresult']['data'][0]['result'])
            {
                return redirect(\URL::Current())->with('success', 'Successfully edit qouta');
            }
            else
            {
                return redirect(\URL::Current())->with('validate_errors', $editQouta['cpanelresult']['data'][0]['reason']);
            }
        }

        return view('backend.cpanel.emails.edit_qouta', $data);
    }

    public function anyDeleteEmail($cpanel_id, $user)
    {
        $cpanel = Cpanel::find($cpanel_id);

        $xmlapi = new xmlapi($cpanel->ip);
        $xmlapi->password_auth($cpanel->username, $cpanel->password);
        $xmlapi->set_output('json');
        $xmlapi->set_port($cpanel->port);
        $params = [
            'domain' => $cpanel->domain,
            'email' => $user
        ];

        $delEmail = $xmlapi->api2_query($cpanel->username, "Email", "delpop", $params);

//        if($delEmail['cpanelresult']['data'][0]['result'])
//        {
//            return redirect('backend/sign-up-email')->with('success', 'Successfully delete');
//        }
//        else
//        {
//            return redirect('backend/sign-up-email')->with('validate_errors', $delEmail['cpanelresult']['data'][0]['reason']);
//        }

        return redirect('backend/cpanel/emails')->with('success', 'Successfully delete');
    }

}
