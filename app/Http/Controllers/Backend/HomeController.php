<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends BackendController
{

    public function anyIndex()
    {
        return redirect('backend/about-us');
        $data = [];

        return view('backend.home', $data);
    }

}
