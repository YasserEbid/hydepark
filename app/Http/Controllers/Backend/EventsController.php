<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\EventsSections;
use App\Models\Events;
use App\Models\EventVideos;
use App\Models\EventPhotos;

class EventsController extends BackendController
{

    public function anyIndex()
    {
        $data['events'] = Events::paginate(10);

        return view('backend.events.index', $data);
    }

    public function anyCreate()
    {
        $data['sections'] = EventsSections::all();

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $event = new Events;

            if($event->validate($_POST))
            {
                $event->fill($_POST);
                $event->created_by = $_SESSION['id'];
                $event->save();
                return redirect('backend/events/photos/' . $event->id)->with('success', 'Successfully insert');
            }
            else
            {
                foreach($event->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.events.create', $data);
    }

    public function anyEdit($id)
    {
        $data['event'] = Events::find($id);
        $data['sections'] = EventsSections::all();

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $event = $data['event'];

            if($event->validate($_POST))
            {
                $event->fill($_POST);
                $event->created_by = $_SESSION['id'];
                $event->save();
                return redirect('backend/events/edit/' . $id)->with('success', 'Successfully update');
            }
            else
            {
                foreach($event->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.events.create', $data);
    }

    public function anyDelete($id)
    {
        $event = Events::find($id);
        $event->delete();
    }

    /**
     * 
     * Photos
     * 
     */
    public function anyPhotos($id)
    {
        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            foreach($_POST['sort'] as $key => $value)
            {
                $sort = EventPhotos::find($value);
                $sort->sort = $key;
                $sort->save();
            }
            return redirect(\URL::Current())->with('success', 'Sorting successfully');
        }

        $data['sorting'] = EventPhotos::where('event_id', $id)->orderBy('sort', 'ASC')->get();
        $data['table_id'] = $id;
        $data['photos'] = EventPhotos::where('event_id', $id)->orderBy('sort', 'ASC')->paginate(10);

        return view('backend.events.photos.index', $data);
    }

    public function anyCreatePhotos($event_id)
    {
        $data['table_id'] = $event_id;

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $event = new EventPhotos;
            $_POST['event_id'] = $event_id;

            if($event->validate($_POST))
            {
                if(isset($_POST['image']) && $_POST['image'] != '')
                {
                    rename('./uploads/temp/' . $_POST['image'], './uploads/events/' . $_POST['image']);
                }

                $event->fill($_POST);
                $event->sort = 3000;
                $event->created_by = $_SESSION['id'];
                $event->save();
                return redirect('backend/events/create-photos/' . $event_id)->with('success', 'Successfully insert');
            }
            else
            {
                foreach($event->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.events.photos.create', $data);
    }

    public function anyEditPhotos($id)
    {
        $data['event'] = $event = EventPhotos::find($id);
        $data['table_id'] = $event->event_id;

        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            $_POST['event_id'] = $event->event_id;

            if($event->validate($_POST))
            {
                if(isset($_POST['image']) && file_exists('./uploads/temp/' . $_POST['image']))
                {
                    rename('./uploads/temp/' . $_POST['image'], './uploads/events/' . $_POST['image']);
                }
                $event->fill($_POST);
                $event->created_by = $_SESSION['id'];
                $event->save();
                return redirect('backend/events/edit-photos/' . $id)->with('success', 'Successfully update');
            }
            else
            {
                foreach($event->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.events.photos.edit', $data);
    }

    public function anyDeletePhotos($id)
    {
        $event = EventPhotos::find($id);
        $event->delete();
    }

    /**
     * 
     * Videos
     * 
     */
    public function anyVideos()
    {
        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            foreach($_POST['sort'] as $key => $value)
            {
                $sort = EventVideos::find($value);
                $sort->sort = $key;
                $sort->save();
            }
            return redirect(\URL::Current())->with('success', 'Sorting successfully');
        }

        $data['sorting'] = EventVideos::orderBy('sort', 'ASC')->get();
        $data['videos'] = EventVideos::orderBy('sort', 'ASC')->paginate(10);

        return view('backend.events.videos.index', $data);
    }

    public function anyCreateVideos()
    {
        $data = [];

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $event = new EventVideos;

            if($event->validate($_POST))
            {
                $event->fill($_POST);
                $event->video = \App\Http\Controllers\Common\youtube::getId($_POST['video_link']);
                $event->video_image = \App\Http\Controllers\Common\youtube::img($_POST['video_link']);
                $event->created_by = $_SESSION['id'];
                $event->sort = 3000;
                $event->save();
                return redirect('backend/events/create-videos')->with('success', 'Successfully insert');
            }
            else
            {
                foreach($event->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.events.videos.create', $data);
    }

    public function anyEditVideos($id)
    {
        $data['event'] = $event = EventVideos::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            if($event->validate($_POST))
            {
                $event->fill($_POST);
                $event->video = \App\Http\Controllers\Common\youtube::getId($_POST['video_link']);
                $event->video_image = \App\Http\Controllers\Common\youtube::img($_POST['video_link']);
                $event->created_by = $_SESSION['id'];
                $event->save();
                return redirect('backend/events/edit-videos/' . $id)->with('success', 'Successfully update');
            }
            else
            {
                foreach($event->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.events.videos.edit', $data);
    }

    public function anyDeleteVideos($id)
    {
        $event = EventVideos::find($id);
        $event->delete();
    }

    /**
     * 
     * 
     * Sections
     * 
     */
    public function anySections()
    {
        $data['events'] = EventsSections::paginate(10);

        return view('backend.events.sections.index', $data);
    }

    public function anyCreateSection()
    {
        $data = [];

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $event = new EventsSections;

            if($event->validate($_POST))
            {
                $event->fill($_POST);
                $event->save();
                return redirect('backend/events/create-section/')->with('success', 'Successfully insert');
            }
            else
            {
                foreach($event->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.events.sections.create', $data);
    }

    public function anyEditSection($id)
    {
        $data['event'] = EventsSections::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $event = $data['event'];

            if($event->validate($_POST))
            {
                $event->fill($_POST);
                $event->save();
                return redirect('backend/events/edit-section/' . $id)->with('success', 'Successfully update');
            }
            else
            {
                foreach($event->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.events.sections.create', $data);
    }

    public function anyDeleteSection($id)
    {
        $event = EventsSections::find($id);
        $event->delete();
    }

}
