<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SectionsHome;
use App\Models\SectionTwoBanners;

class HomeSectionsController extends BackendController
{

    public function anyIndex()
    {
        $data['section'] = SectionsHome::where('type', 'one')->first();

        return view('backend.section-home.one', $data);
    }

    public function anyTwo()
    {
        $data['section'] = SectionsHome::where('type', 'two')->first();

        return view('backend.section-home.two', $data);
    }

    public function anyEdit($id)
    {
        $data['section'] = SectionsHome::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $section = $data['section'];

            if($section->validate($_POST))
            {
                $_POST['created_by'] = $_SESSION['id'];
                $section->fill($_POST);
                $section->save();
                return redirect('backend/home-sections/edit/'.$id)->with('success', 'Successfully edit');
            }
            else
            {
                $data['validate_errors'] = $section->errors();
            }
        }

        return view('backend.section-home.edit', $data);
    }

    public function anyBanners()
    {
        $data['banners'] = SectionTwoBanners::orderBy('id', 'asc')->paginate(10);

        return view('backend.section-home.banners.index', $data);
    }

    public function anyCreateBanner()
    {
        $data = [];

        if(isset($_POST['save']))
        {
            unset($_POST['save']);


            $banner = new SectionTwoBanners;

            if($banner->validate($_POST))
            {
                if(isset($_POST['image']) && file_exists('./uploads/temp/'.$_POST['image']))
                {
                    rename('./uploads/temp/'.$_POST['image'], './uploads/'.$_POST['image']);
                }
                $_POST['created_by'] = $_SESSION['id'];
                $banner->fill($_POST);
                $banner->save();
                return redirect('backend/home-sections/create-banner/')->with('success', 'Successfully create');
            }
            else
            {
                $data['validate_errors'] = $banner->errors();
            }
        }

        return view('backend.section-home.banners.create', $data);
    }
    
     public function anyEditBanner($id)
    {
        $data['banner'] = SectionTwoBanners::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);


            $banner = $data['banner'];

            if($banner->validate($_POST))
            {
                if(isset($_POST['image']) && file_exists('./uploads/temp/'.$_POST['image']))
                {
                    rename('./uploads/temp/'.$_POST['image'], './uploads/'.$_POST['image']);
                }
                $_POST['created_by'] = $_SESSION['id'];
                $banner->fill($_POST);
                $banner->save();
                return redirect('backend/home-sections/create-banner/')->with('success', 'Successfully edit');
            }
            else
            {
                $data['validate_errors'] = $banner->errors();
            }
        }

        return view('backend.section-home.banners.edit', $data);
    }

    public function anyDelete($id)
    {

        $banner = SectionTwoBanners::find($id);

        $file_path = 'uploads/'.$banner->image;
        if(file_exists($file_path))
            unlink($file_path);

        $banner->delete();

        return redirect('backend/home-sections/banners')->with('success', 'Successfully delete');
    }

}
