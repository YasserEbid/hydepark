<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MailBox;

class SetupMailsController extends BackendController
{

    public function anyIndex()
    {
        $data = [];

        return view('backend.backend_options.setup_mails.index', $data);
    }

    public function anyCreate()
    {
        $data = [];

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $setup = new MailBox;

            if($setup->validate($_POST))
            {
                $_POST['is_active'] = 1;
                $_POST['created_by'] = $_SESSION['id'];
                $setup->fill($_POST);
                $setup->save();
                redirect('backend/setup-mails/create')->with('success', 'Successfully insert');
            }
            else
            {
                foreach($setup->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.setup_mails.create', $data);
    }

    public function anyEdit($id)
    {
        $data['setup'] = MailBox::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $setup = $data['setup'];
            
            if(empty($_POST['password']))
            {
                $_POST['password'] = $setup->password;
            }
                        
            if($setup->validate($_POST))
            {
                $setup->fill($_POST);
                $setup->save();
                redirect('backend/setup-mails/edit/'.$id)->with('success', 'Successfully update');
            }
            else
            {
                foreach($setup->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.setup_mails.edit', $data);
    }
    
    public function anyActivated($id)
    {
        $setup = MailBox::find($id);
        if($setup->is_active == 0)
            $setup->is_active = 1;
        else
            $setup->is_active = 0;

        $setup->save();

        return redirect('./backend/setup-mails')->with('success', 'Successfully insert');
    }
    
    public function anyDelete($id)
    {
        $setup = MailBox::find($id);

        $setup->delete();

        return redirect('./backend/setup-mails')->with('success', 'Successfully delete');
    }

    public function anyMailsDomain()
    {
        $data['titlPage'] = 'Mails domain';
        $data['dataPage'] = 'Mails domain data';
        
        $data['allSetup'] = MailBox::where('type','domain')->orderBy('id','DESC')->paginate(10);

        return view('backend.backend_options.setup_mails.page_table', $data);
    }

    public function anyGmail()
    {
        $data['titlPage'] = 'Gmail';
        $data['dataPage'] = 'Gmail data';

        $data['allSetup'] = MailBox::where('type','gmail')->orderBy('id','DESC')->paginate(10);

        return view('backend.backend_options.setup_mails.page_table', $data);
    }

    public function anyYahoo()
    {
        $data['titlPage'] = 'Yahoo';
        $data['dataPage'] = 'Yahoo data';
        
        $data['allSetup'] = MailBox::where('type','yahoo')->orderBy('id','DESC')->paginate(10);

        return view('backend.backend_options.setup_mails.page_table', $data);
    }

    public function anyOutlook()
    {
        $data['titlPage'] = 'Outlook';
        $data['dataPage'] = 'Outlook data';
        
        $data['allSetup'] = MailBox::where('type','outlook')->orderBy('id','DESC')->paginate(10);

        return view('backend.backend_options.setup_mails.page_table', $data);
    }

}
