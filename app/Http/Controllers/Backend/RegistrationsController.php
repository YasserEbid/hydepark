<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Registrations;

class RegistrationsController extends BackendController
{

    public function anyIndex()
    {
        $data['registrations'] = Registrations::orderBy('id', 'desc')->paginate(20);

        return view('backend.backend_options.registrations.index', $data);
    }

    public function anyExport()
    {
        $registrations = Registrations::all();
        if($registrations->count() > 0)
        {
            $data = array();
            $data[] = array("Name", "Phone", "Email", "Message", "Date");
            foreach($registrations as $one)
            {
                $temp = array();
                $temp[] = $one->name;
                $temp[] = $one->phone;
                $temp[] = $one->email;
                $temp[] = $one->message;
                $temp[] = $one->createdAT();
                $data[] = $temp;
            }
            \Excel::create('Hyde Park Registrations', function($excel) use($data)
            {
                // Set the title
                $excel->setTitle('Our new awesome title');


                $excel->sheet('Sheetname', function($sheet) use($data)
                {
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });
            })->download('xlsx');
        }
        return redirect('backend/registrations');
    }

    public function anyWriteNotes($id)
    {
        $data['note'] = Registrations::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $note = $data['note'];

            $note->note = $_POST['note'];
            $note->contacted_by = $_SESSION['id'];

            $note->save();

            return redirect('backend/registrations/write-notes/'.$id)->with('success', 'Successfully update');
        }

        return view('backend.backend_options.registrations.notes', $data);
    }

    public function anyDelete($id)
    {
        $contact = Registrations::find($id);

        $contact->delete();

        return redirect('./backend/registrations')->with('success', 'Successfully delete');
    }

}
