<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Contacts;

class ContactsController extends BackendController
{

    public function anyIndex()
    {
        $data['contacts'] = Contacts::orderBy('id', 'desc')->paginate(20);

        return view('backend.backend_options.contacts.index', $data);
    }

    public function anyExport()
    {
        $contacts = Contacts::all();
        if($contacts->count() > 0)
        {
            $data = array();
            $data[] = array("Name", "Phone", "Email", "Message", "Date");
            foreach($contacts as $one)
            {
                $temp = array();
                $temp[] = $one->name;
                $temp[] = $one->phone;
                $temp[] = $one->email;
                $temp[] = $one->message;
                $temp[] = $one->createdAT();
                $data[] = $temp;
            }
            \Excel::create('Hyde Park Contacts', function($excel) use($data)
            {
                // Set the title
                $excel->setTitle('Our new awesome title');


                $excel->sheet('Sheetname', function($sheet) use($data)
                {
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });
            })->download('xlsx');
        }
        return redirect('backend/contacts');
    }

    public function anyWriteNotes($id)
    {
        $data['note'] = Contacts::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $note = $data['note'];

            $note->note = $_POST['note'];
            $note->contacted_by = $_SESSION['id'];

            $note->save();

            return redirect('backend/contacts/write-notes/'.$id)->with('success', 'Successfully update');
        }

        return view('backend.backend_options.contacts.notes', $data);
    }

    public function anyDelete($id)
    {
        $contact = Contacts::find($id);

        $contact->delete();

        return redirect('./backend/contacts')->with('success', 'Successfully delete');
    }

}
