<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MediaCenter;
use App\Models\Newsletter;

class NewsletterController extends BackendController
{

    public function anyIndex()
    {
        $data['newsletters'] = Newsletter::orderBy('id','desc')->paginate(10);

        return view('backend.newsletter.index', $data);
    }

    public function anyExport()
    {
        $contacts = Newsletter::all();
        if($contacts->count() > 0)
        {
            $data = array();
            $data[] = array("Name", "Phone", "Email", "type", "Code", "Unit number", "Date");
            foreach($contacts as $one)
            {
                if($one->type == 1){$type = 'Homeowner';}else{$type = 'Interested In Hyde Park';}
                $temp = array();
                $temp[] = $one->name;
                $temp[] = $one->phone;
                $temp[] = $one->email;
                $temp[] = $type;
                $temp[] = $one->code;
                $temp[] = $one->unit_number;
                $temp[] = $one->createdAT();
                $data[] = $temp;
            }
            \Excel::create('Hyde Park Newsletter', function($excel) use($data)
            {
                // Set the title
                $excel->setTitle('Our new awesome title');


                $excel->sheet('Sheetname', function($sheet) use($data)
                {
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data);
                });
            })->download('xlsx');
        }
        return redirect('backend/newsletter');
    }

    public function anyWriteNotes($id)
    {
        $data['note'] = Newsletter::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $note = $data['note'];

            $note->note = $_POST['note'];
            $note->contacted_by = $_SESSION['id'];

            $note->save();

            return redirect('backend/newsletter/write-notes/'.$id)->with('success', 'Successfully update');
        }

        return view('backend.newsletter.notes', $data);
    }

    public function anyDelete($id)
    {
        $contact = Newsletter::find($id);

        $contact->delete();

        return redirect('./backend/newsletter')->with('success', 'Successfully delete');
    }

}
