<?php
namespace App\Http\Controllers\Backend\Roles;

use Illuminate\Routing\Route;

class Roles 
{
    public static $skiped=['auth.login','auth.logout','auth.forgotpassword','auth.lockedscreen','home.index','backendusers.profile'];
    
    public static function getUri(Route $route) 
    {        
        $route = explode("\\" , $route->getActionName());
        $route = end($route);
        $route = explode("@" , $route );
        $controller = strtolower(str_replace('Controller','',$route[0]));
        $action = strtolower(str_replace(['any' , 'post' , 'get'],'',$route[1]));
        return $controller.'.'. $action;
    }
        
    public static function check($action) 
    {
        if(in_array($action , Roles::$skiped)) return true ;
        if(isset($_SESSION['is_super'])) return true ; 
        if(!isset($_SESSION['role_id'])) return false ;
        $pageAction = \App\Models\PagesActions::where('action' ,$action )->first();
        
        if(!$pageAction){return false ;}
        
        $actionRole = \App\Models\RoleActions::where('role_id' , $_SESSION['role_id'])->where('action_id' ,$pageAction->id )->count() ;  
        
        if($actionRole==0){return false;}
        
        return true;
    }
}

?>