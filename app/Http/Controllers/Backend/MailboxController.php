<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MailBox;

class MailboxController extends BackendController
{

    public function anyIndex()
    {
        $data['mails'] = MailBox::where('is_active', true)->orderBy('type', 'DESC')->get();

        return view('backend.backend_options.mail_box.index', $data);
    }

}
