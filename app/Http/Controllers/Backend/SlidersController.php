<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Sliders;

class SlidersController extends BackendController
{

    public function anyIndex()
    {
        $data['sliders'] = Sliders::paginate(10);

        return view('backend.sliders.index', $data);
    }

    public function anyCreate()
    {
        $data = [];

        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            $slider = new Sliders;

            if($slider->validate($_POST))
            {
                if(isset($_POST['image']) && file_exists('./uploads/temp/'.$_POST['image']))
                {
                    rename('./uploads/temp/'.$_POST['image'], './uploads/'.$_POST['image']);
                }

                $_POST['created_by'] = $_SESSION['id'];
                $slider->fill($_POST);
                $slider->save();

                return redirect(\URL::Current())->with('success', 'Successfully insert');
            }
            else
            {
                $data['validate_errors'] = $slider->errors();
            }
        }


        return view('backend.sliders.create', $data);
    }

    public function anyDelete($id)
    {
        $slider = Sliders::find($id);

        $file_path = 'uploads/'.$slider->image;
        if(file_exists($file_path))
            unlink($file_path);

        $slider->delete();

        return redirect('./backend/sliders')->with('success', 'Successfully delete');
    }

}
