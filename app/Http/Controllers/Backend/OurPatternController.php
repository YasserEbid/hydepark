<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\OurPatternPhotos;

class OurPatternController extends BackendController
{

    public function anyIndex()
    {
        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            foreach ($_POST['sort'] as $key => $value)
            {
                $sort = OurPatternPhotos::find($value);
                $sort->sort = $key;
                $sort->save();
            }
            return redirect(\URL::Current())->with('success', 'Sorting successfully');
        }

        $data['sorting'] = OurPatternPhotos::orderBy('sort', 'ASC')->get();
        $data['events'] = OurPatternPhotos::orderBy('sort', 'ASC')->paginate(10);

        return view('backend.our_pattern.index', $data);
    }

    public function anyCreate()
    {
        $data = [];
        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $event = new OurPatternPhotos;

            if($event->validate($_POST))
            {
                if(isset($_POST['image']) && $_POST['image'] != '')
                {
                    rename('./uploads/temp/' . $_POST['image'], './uploads/' . $_POST['image']);
                }

                $event->fill($_POST);
                $event->sort = 3000;
                $event->created_by = $_SESSION['id'];
                $event->save();
                return redirect(\URL::Current())->with('success', 'Insert successfully');
            }
            else
            {
                foreach ($event->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.our_pattern.create', $data);
    }

    public function anyEdit($id)
    {
        $data['event'] = $event = OurPatternPhotos::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            if($event->validate($_POST))
            {
                if(isset($_POST['image']) && file_exists('./uploads/temp/' . $_POST['image']))
                {
                    rename('./uploads/temp/' . $_POST['image'], './uploads/' . $_POST['image']);
                }
                $event->fill($_POST);
                $event->created_by = $_SESSION['id'];
                $event->save();
                return redirect(\URL::Current())->with('success', 'Update successfully');
            }
            else
            {
                foreach ($event->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.our_pattern.edit', $data);
    }

    public function anyDelete($id)
    {
        $event = OurPatternPhotos::find($id);
        $event->delete();
    }

}
