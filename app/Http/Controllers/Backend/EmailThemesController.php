<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\EmailThemes;

class EmailThemesController extends BackendController
{

    public function anyIndex()
    {
        $data['themes'] = EmailThemes::paginate(10);

        return view('backend.email_themes.index', $data);
    }

    public function anyCreate()
    {
        $data = [];

        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            $theme = new EmailThemes;

            if($theme->validate($_POST))
            {
                $_POST['created_by'] = $_SESSION['id'];
                $theme->fill($_POST);
                $theme->save();
                return redirect(\URL::Current())->with('success', 'Successfully insert');
            }
            else
            {
                foreach($theme->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.email_themes.create', $data);
    }

    public function anyEdit($id)
    {
        $data['theme'] = EmailThemes::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            $theme = $data['theme'];

            if($theme->validate($_POST))
            {
                $_POST['created_by'] = $_SESSION['id'];
                $theme->fill($_POST);
                $theme->save();
                return redirect(\URL::Current())->with('success', 'Successfully update');
            }
            else
            {
                foreach($theme->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.email_themes.edit', $data);
    }
    
    public function anyDelete($id)
    {
        $theme = EmailThemes::find($id);
        
        $theme->delete();
        
        return redirect('backend/email-themes')->with('success', 'Successfully delete');
    }

}
