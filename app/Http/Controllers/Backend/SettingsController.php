<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Settings;

class SettingsController extends BackendController
{

    public function anyIndex()
    {
        if(isset($_POST['save']))
        {
            foreach($_POST['value'] as $key => $row)
            {
                $setting = Settings::find($key);
                if($setting)
                {
                    $setting->value = $row;
                    $setting->save();
                }
            }
            if(isset($_POST['image']) && file_exists('./uploads/temp/'.$_POST['image']))
            {
                rename('./uploads/temp/'.$_POST['image'], './uploads/'.$_POST['image']);
                $setting = Settings::find(10);
                $setting->value = $_POST['image'];
                $setting->save();
            }
            return redirect(\URL::Current())->with('success', 'Successfully update');
        }

        $data['settings'] = Settings::Orderby('order','asc')->get();
        $setting = $data['settings'];

        return view('backend.settings.index', $data);
    }

}
