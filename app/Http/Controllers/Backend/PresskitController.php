<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Presskit;

class PresskitController extends BackendController
{

    public function anyIndex()
    {
        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            foreach ($_POST['sort'] as $key => $value)
            {
                $sort = Presskit::find($value);
                $sort->sort = $key;
                $sort->save();
            }
            return redirect(\URL::Current())->with('success', 'Sorting successfully');
        }
        
        $data['result'] = Presskit::orderBy('sort', 'ASC')->paginate(10);
        $data['sorting'] = Presskit::orderBy('sort', 'ASC')->get();

        return view('backend.media_center.presskit.index', $data);
    }

    public function anyCreate()
    {
        $data = [];

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $object = new Presskit;

            if($object->validate($_POST))
            {
                if(isset($_POST['image']) && $_POST['image'] != '')
                {
                    rename('./uploads/temp/' . $_POST['image'], './uploads/' . $_POST['image']);
                }

                if(isset($_POST['file']) && $_POST['file'] != '')
                {
                    rename('./uploads/temp/' . $_POST['file'], './uploads/' . $_POST['file']);
                }

                $object->fill($_POST);
                $object->sort = 3000;
                $object->created_by = $_SESSION['id'];
                $object->save();
                return redirect(\URL::current())->with('success', 'Insert successfully');
            }
            else
            {
                foreach ($object->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.media_center.presskit.create', $data);
    }

    public function anyEdit($id)
    {
        $data['object'] = Presskit::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $object = $data['object'];

            if($object->validate($_POST))
            {
                if(isset($_POST['image']) && file_exists('./uploads/temp/' . $_POST['image']))
                {
                    rename('./uploads/temp/' . $_POST['image'], './uploads/' . $_POST['image']);
                }
                if(isset($_POST['file']) && file_exists('./uploads/temp/' . $_POST['file']))
                {
                    rename('./uploads/temp/' . $_POST['file'], './uploads/' . $_POST['file']);
                }

                $object->fill($_POST);
                $object->created_by = $_SESSION['id'];
                $object->save();
                return redirect(\URL::current())->with('success', 'Update successfully');
            }
            else
            {
                foreach ($object->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.media_center.presskit.edit', $data);
    }

    public function anyDelete($id)
    {
        $object = Presskit::find($id);
        $object->delete();
    }

}
