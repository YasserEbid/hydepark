<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Roles;
use App\Models\Pages;
use App\Models\RoleActions;

class RolesController extends BackendController
{

    public function anyIndex()
    {
        if(isset($_SESSION['is_super']))
        {
            $data['roles'] = Roles::paginate(10);
        }
        else
        {
            $data['roles'] = Roles::where('is_super', false)->paginate(10);
        }

        return view('backend.backend_options.roles.index', $data);
    }

    public function anyCreate()
    {
        $data = [];

        if(isset($_POST['save']))
        {
            $role = new Roles;

            unset($_POST['save']);

            if($role->validate($_POST))
            {
                $_POST['created_by'] = $_SESSION['id'];
                $role->fill($_POST);
                $role->save();
                return redirect('backend/roles/role-actions/'.$role->id)->with('success', 'Successfully insert role please choose actions role');
            }
            else
            {
                foreach($role->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.roles.create', $data);
    }

    public function anyEdit($id)
    {
        $data['role'] = Roles::find($id);

        if(isset($_POST['save']))
        {
            $role = $data['role'];

            unset($_POST['save']);

            if($role->validate($_POST))
            {
                $role->fill($_POST);
                $role->save();
                return redirect(\URL::Current())->with('success', 'Successfully update');
            }
            else
            {
                foreach($role->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.roles.edit', $data);
    }

    public function anyDelete($id)
    {
        $role = Roles::find($id);

        $role->delete();

        return redirect('./backend/roles')->with('success', 'Successfully delete');
    }

    /*
     * -- Actions Roles -- * 
     */

    public function anyRoleActions($id)
    {
        $data['pages'] = Pages::Orderby('sort', 'asc')->get();
        $data['role'] = Roles::find($id);

        if(isset($_POST['save']))
        {
            $validate_errors = [];
//            unset($_POST['sort']);
            unset($_POST['save']);

            if(empty($_POST['actions']))
            {
                $validate_errors[] = 'Please choose actions';
                $data['validate_errors'] = $validate_errors;
            }
            else
            {
                $oldAction = [];
                $OldRoleActions = RoleActions::where('role_id', $id)->get();
                foreach($OldRoleActions as $OldActionsRole)
                {
                    $oldAction[] = $OldActionsRole->action_id;
                }

                $deletedArray = array_diff($oldAction, $_POST['actions']);
                $newArray = array_diff($_POST['actions'], $oldAction);

                foreach($newArray as $row)
                {
                    $action = new RoleActions;
                    $action->action_id = $row;
                    $action->role_id = $id;
                    $action->save();
                }

                foreach($deletedArray as $row)
                {
                    $action = RoleActions::where('action_id', $row)->where('role_id', $id)->first()->delete();
                }

                foreach($_POST['sort'] as $key => $value)
                {
                    $sort = Pages::find($value);
                    $sort->sort = $key;
                    $sort->save();
                }
                
                return redirect('backend/roles/role-actions/'.$id)->with('success', 'Successfully change actions');
            }
        }

        return view('backend.backend_options.roles.role_actions.index', $data);
    }

}
