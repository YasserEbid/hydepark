<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Models\MediaSections;

class MediaController extends BackendController
{

    public function anyIndex()
    {
        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            foreach($_POST['sort'] as $key => $value)
            {
                $sort = Media::find($value);
                $sort->sort = $key;
                $sort->save();
            }
            return redirect(\URL::Current())->with('success', 'Sorting successfully');
        }

        $data['result'] = Media::orderBy('sort', 'ASC')->paginate(10);
        $data['sorting'] = Media::orderBy('sort', 'ASC')->get();

        return view('backend.media_center.media.index', $data);
    }

    public function anyCreate()
    {
        $data['sections'] = MediaSections::where('parent_id',0)->orderBy('id','ASC')->get();

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $object = new Media;

            if($object->validate($_POST))
            {
                if(isset($_POST['image']) && $_POST['image'] != '')
                {
                    rename('./uploads/temp/' . $_POST['image'], './uploads/' . $_POST['image']);
                }

                if(isset($_POST['file']) && $_POST['file'] != '')
                {
                    rename('./uploads/temp/' . $_POST['file'], './uploads/' . $_POST['file']);
                }

                $object->fill($_POST);
                $object->sort = 3000;
                $object->created_by = $_SESSION['id'];
                $object->save();
                return redirect(\URL::current())->with('success', 'Insert successfully');
            }
            else
            {
                foreach($object->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.media_center.media.create', $data);
    }

    public function anyEdit($id)
    {
        $data['sections'] = MediaSections::where('parent_id',0)->orderBy('id','ASC')->get();
        $data['object'] = Media::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $object = $data['object'];

            if($object->validate($_POST))
            {
                if(isset($_POST['image']) && file_exists('./uploads/temp/' . $_POST['image']))
                {
                    rename('./uploads/temp/' . $_POST['image'], './uploads/' . $_POST['image']);
                }
                if(isset($_POST['file']) && file_exists('./uploads/temp/' . $_POST['file']))
                {
                    rename('./uploads/temp/' . $_POST['file'], './uploads/' . $_POST['file']);
                }

                $object->fill($_POST);
                $object->created_by = $_SESSION['id'];
                $object->save();
                return redirect(\URL::current())->with('success', 'Update successfully');
            }
            else
            {
                foreach($object->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.media_center.media.edit', $data);
    }

    public function anyDelete($id)
    {
        $object = Media::find($id);
        $object->delete();
    }

    /**
     * 
     * 
     * Sections
     * 
     */
    public function anySections()
    {
        $data['media'] = MediaSections::paginate(10);

        return view('backend.media_center.media.sections.index', $data);
    }

    public function anyCreateSection()
    {
        $data['sections'] = MediaSections::where('parent_id',0)->orderBy('id','ASC')->get();

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $event = new MediaSections;

            if($event->validate($_POST))
            {
                $event->fill($_POST);
                $event->save();
                return redirect('backend/media/create-section/')->with('success', 'Successfully insert');
            }
            else
            {
                foreach($event->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.media_center.media.sections.create', $data);
    }

    public function anyEditSection($id)
    {
        $data['sections'] = MediaSections::where('parent_id',0)->orderBy('id','ASC')->get();
        $data['event'] = MediaSections::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $event = $data['event'];

            if($event->validate($_POST))
            {
                $event->fill($_POST);
                $event->save();
                return redirect('backend/media/edit-section/' . $id)->with('success', 'Successfully update');
            }
            else
            {
                foreach($event->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.media_center.media.sections.create', $data);
    }

    public function anyDeleteSection($id)
    {
        $event = MediaSections::find($id);
        $event->delete();
    }

}
