<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Subscribers;
use App\Models\EmailThemes;

class SubscribersController extends BackendController
{

    public function anyIndex()
    {
        $data['subscribers'] = Subscribers::paginate(10);

        return view('backend.subscribers.index', $data);
    }

    public function anySendEmail()
    {
        $data['themes'] = EmailThemes::all();

        if(isset($_POST['send']))
        {
            unset($_POST['send']);

            $themes = EmailThemes::where('id', $_POST['themes'])->first();
            $subscribersEmails = Subscribers::where('follower', TRUE)->get();

            $subscribers = new Subscribers;

            if($subscribers->validate($_POST))
            {
                foreach($subscribersEmails as $sub)
                {
                    $to = $sub->email;

                    $subject = $_POST['subject'];

                    $message = $themes->header.$_POST['content'].$themes->footer;

                    $headers = 'MIME-Version: 1.0'."\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";

                    $headers .= 'From: Matar Bin Lahej <info@tarekmagdy.com>'."\r\n";

                    $mail = mail($to, $subject, $message, $headers);
                }

                return redirect('./backend/subscribers/create')->with('success', 'Successful Send Mail');
            }
            else
            {
                $data['validate_errors'] = $sub->errors();
            }
        }

        return view('backend.subscribers.send_email', $data);
    }

    public function anyDelete($id)
    {
        $sub = Subscribers::find($id);

        $sub->delete();

        return redirect()->to('backend/subscribers')->with('success', 'Successfully delete');
    }

    public function anyDeleteUnFollower()
    {
        $subscribers = Subscribers::where('follower', false)->get();
        foreach($subscribers as $sub)
        {
            $sub = Subscribers::find($sub->id);
            $sub->delete();
        }

        return redirect()->to('backend/subscribers')->with('success', 'Successfully delete unfollower');
    }
    
    public function anyActivated($id)
    {
        $sub = Subscribers::find($id);
        if($sub->follower == 0)
            $sub->follower = 1;
        else
            $sub->follower = 0;

        $sub->save();

        return redirect('./backend/subscribers')->with('success', 'Successfully activated');
    }

}
