<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Routing\Route;
use App\Http\Controllers\Backend\Roles\Roles;

class BackendController extends BaseController
{

    public function __construct(Route $route)
    {
        $uri = Roles::getUri($route);
        if(!Roles::check($uri))
        {
            if(!empty($_SESSION))
            {
                return \Redirect::to('./backend/auth/lockedscreen')
                                ->with('error', "You don't have role to access this action")
                                ->send();
            }
            else
            {
                return \Redirect::to('./backend/auth/login')
                                ->with('error', "You don't have role to access this action")
                                ->send();
            }
        }
        
        
    }

}
