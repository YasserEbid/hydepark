<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Languages;

class LanguagesController extends BackendController
{

    public function anyIndex()
    {
        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            foreach($_POST['sort'] as $key => $value)
            {
                $sort = Languages::find($value);
                $sort->sort = $key;
                $sort->save();
            }
            return redirect('backend/languages')->with('success', 'Successfully sorting');
        }
        
        $data['sorting'] = Languages::orderBy('sort', 'ASC')->get();
        $data['languages'] = Languages::orderBy('sort', 'ASC')->paginate(10);

        return view('backend.languages.index', $data);
    }

    public function anyCreate()
    {
        $data = [];
        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            $language = new Languages;
            if($language->validate($_POST))
            {
                if(isset($_POST['image']) && file_exists('./uploads/temp/'.$_POST['image']))
                {
                    rename('./uploads/temp/'.$_POST['image'], './uploads/languages/'.$_POST['image']);
                    $file_path = 'uploads/temp/'.$_POST['image'];
                    if(file_exists($file_path))
                        unlink($file_path);
                }
                $_POST['is_active'] = 1;
                $language->fill($_POST);
                $language->save();
                return redirect(\URL::Current())->with('success', 'Successfully insert');
            }
            else
            {
                foreach($language->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.languages.create', $data);
    }

    public function anyEdit($id)
    {
        $data['language'] = Languages::find($id);
        $language = $data['language'];

        if(isset($_POST['save']))
        {
            if($language->validate($_POST))
            {
                unset($_POST['save']);

                if(isset($_POST['image']) && file_exists('./uploads/temp/'.$_POST['image']))
                {
                    rename('./uploads/temp/'.$_POST['image'], './uploads/languages/'.$_POST['image']);
                    $file_path = 'uploads/temp/'.$_POST['image'];
                    if(file_exists($file_path))
                        unlink($file_path);
                }
                $language->fill($_POST);
                $language->save();
                return redirect(\URL::Current())->with('success', 'Successfully update');
            }
            else
            {
                foreach($language->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.languages.edit', $data);
    }

    public function anyDelete($id)
    {
        $language = Languages::find($id);

        $file_path = 'uploads/languages/'.$language->image;
        if(file_exists($file_path))
            unlink($file_path);

        $language->delete();

        return redirect('./backend/languages')->with('success', 'Successfully delete');
    }

    public function anyActivated($id)
    {
        $language = Languages::find($id);
        if($language->is_active == 0)
            $language->is_active = 1;
        else
            $language->is_active = 0;

        $language->save();

//        return redirect('./backend/languages')->with('success', 'Successfully change');
    }

    public function anyDefaulting($id)
    {
        $languages = Languages::all();

        foreach($languages as $language)
        {
            $lang = Languages::find($language->id);
            if($lang->id == $id)
            {
                $language->is_default = 1;
            }
            else
            {
                $language->is_default = 0;
            }

            $language->save();
        }


//        return redirect('./backend/languages')->with('success', 'Successfully change');
    }

}
