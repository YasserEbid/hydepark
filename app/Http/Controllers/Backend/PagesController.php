<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Pages;
use App\Models\PagesActions;

class PagesController extends BackendController
{

    public function anyIndex()
    {
        $data['pages'] = Pages::paginate(10);

        return view('backend.backend_options.pages.index', $data);
    }

    public function anyCreate()
    {
        $data = [];

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $page = new Pages;

            if($page->validate($_POST))
            {
                $_POST['created_by'] = $_SESSION['id'];
                $page->fill($_POST);
                $page->save();

                return redirect('backend/pages/page-actions/'.$page->id)->with('success', 'Successfully insert page insert actions page');
            }
            else
            {
                foreach($page->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.pages.create', $data);
    }

    public function anyEdit($id)
    {
        $data['page'] = Pages::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $page = $data['page'];

            if($page->validate($_POST))
            {
                $page->fill($_POST);
                $page->save();

                return redirect(\URL::Current())->with('success', 'Successfully update');
            }
            else
            {
                foreach($page->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.pages.edit', $data);
    }

    public function anyDelete($id)
    {
        $page = Pages::find($id);

        $page->delete();

        return redirect('backend/pages')->with('success', 'Successfully insert');
    }

    /*
     * -- Actions Pages -- * 
     */

    public function anyPageActions($id)
    {
        $data['page_id'] = $id;

        $data['pagesActions'] = PagesActions::where('page_id', $id)->paginate(10);

        return view('backend.backend_options.pages.page_actions.index', $data);
    }

    public function anyCreatePageActions($id)
    {
        $data['page_id'] = $id;

        $data['pagesActions'] = PagesActions::where('page_id', $id)->paginate(10);

        if(isset($_POST['save']))
        {
            $search = ['/', '-'];
            $replace = ['.', ''];
            $action = str_replace($search, $replace, $_POST['action']);

            $page = new PagesActions;

            if($page->validate($_POST))
            {
                unset($_POST['save']);
                $page->page_id = $id;
                $page->name = $_POST['name'];
                $page->action =  $action;
                $page->created_by = $_SESSION['id'];
                $page->save();
                return redirect('backend/pages/create-page-actions/'.$id)->with('success', 'Successfully insert');
            }
            else
            {
                foreach($page->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.pages.page_actions.create', $data);
    }

    public function anyEditPageActions($id, $page_id)
    {
        $data['page_id'] = $page_id;
        $data['page'] = PagesActions::find($id);

        if(isset($_POST['save']))
        {
            $search = ['/', '-'];
            $replace = ['.', ''];
            $name = str_replace($search, $replace, $_POST['name']);

            $page = $data['page'];

            unset($_POST['save']);
            if($page->validate($_POST))
            {
                $page->page_id = $page_id;
                $page->name = $name;
                $page->action = $_POST['action'];
                $page->created_by = $_SESSION['id'];
                $page->save();
                return redirect(\URL::Current().'/'.$id)->with('success', 'Successfully insert');
            }
            else
            {
                foreach($page->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.pages.page_actions.edit', $data);
    }

    public function anyDeletePageActions($page_id, $id)
    {
        $page = PagesActions::find($id);

        $page->delete();

        return redirect('backend/pages/create-page-actions/'.$page_id)->with('success', 'Successfully delete');
    }

}
