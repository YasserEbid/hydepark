<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\BackendUsers;
use App\Models\Roles;

class BackendUsersController extends BackendController
{

    public function anyIndex()
    {
        if(isset($_SESSION['is_super']))
        {
            $data['users'] = BackendUsers::where('id', '!=', $_SESSION['id'])->paginate(10);
        }
        else
        {
            $data['users'] = BackendUsers::where('role_id', '!=', 1)->where('id', '!=', $_SESSION['id'])->paginate(10);
        }

        return view('backend.backend_options.backend_users.index', $data);
    }

    public function anyCreate()
    {
        if(isset($_SESSION['is_super']))
        {
            $data['roles'] = Roles::all();
        }
        else
        {
            $data['roles'] = Roles::where('is_super', false)->get();
        }

        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            $user = new BackendUsers;
            if($user->validate($_POST))
            {
                unset($_POST['c_password']);
                $_POST['password'] = md5($_POST['password']);
                $_POST['is_active'] = 1;
                $_POST['created_by'] = $_SESSION['id'];
                if(isset($_POST['image']) && file_exists('./uploads/temp/'.$_POST['image']))
                {
                    rename('./uploads/temp/'.$_POST['image'], './uploads/backend_users/'.$_POST['image']);
                }
                $user->fill($_POST);
                $user->save();
                return redirect(\URL::Current())->with('success', 'Successfully insert');
            }
            else
            {
                foreach($user->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.backend_users.create', $data);
    }

    public function anyEdit($id)
    {
        $data['user'] = BackendUsers::find($id);
        $user = $data['user'];

        if(isset($_SESSION['is_super']))
        {
            $data['roles'] = Roles::all();
        }
        else
        {
            $data['roles'] = Roles::where('is_super', false)->get();
        }

        if(isset($_POST['save']))
        {
            if(!empty($_POST['password']))
            {
                $_POST['password'] = md5($_POST['password']);
                $_POST['c_password'] = md5($_POST['c_password']);
            }
            else
            {
                $_POST['password'] = $user->password;
            }

            if($user->validate($_POST))
            {
                unset($_POST['save']);
                unset($_POST['c_password']);

                if(isset($_POST['image']) && file_exists('./uploads/temp/'.$_POST['image']))
                {
                    rename('./uploads/temp/'.$_POST['image'], './uploads/backend_users/'.$_POST['image']);
                }
                $user->fill($_POST);
                $user->save();
                return redirect(\URL::Current())->with('success', 'Successfully update');
            }
            else
            {
                foreach($user->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.backend_users.edit', $data);
    }

    public function anyDelete($id)
    {
        $user = BackendUsers::find($id);

        $file_path = 'uploads/backend_users/'.$user->image;
        if(file_exists($file_path))
            unlink($file_path);

        $user->delete();

        return redirect('./backend/backend-users')->with('success', 'Successfully delete');
    }

    public function anyActivated($id)
    {
        $user = BackendUsers::find($id);
        if($user->is_active == 0)
            $user->is_active = 1;
        else
            $user->is_active = 0;

        $user->save();

//        return redirect('./backend/backend-users')->with('success', 'Successfully insert');
    }

    public function anyProfile()
    {
        $data['user'] = BackendUsers::find($_SESSION['id']);
        $user = $data['user'];

        if(isset($_POST['save']))
        {
            if(!empty($_POST['password']))
            {
                $_POST['password'] = md5($_POST['password']);
                $_POST['c_password'] = md5($_POST['c_password']);
            }
            else
            {
                $_POST['password'] = $user->password;
            }

            if($user->validate($_POST))
            {
                unset($_POST['save']);
                unset($_POST['c_password']);

                if(isset($_POST['image']) && file_exists('./uploads/temp/'.$_POST['image']))
                {
                    rename('./uploads/temp/'.$_POST['image'], './uploads/backend_users/'.$_POST['image']);
                }
                $user->fill($_POST);
                $user->save();
                return redirect(\URL::Current())->with('success', 'Successfully update');
            }
            else
            {
                foreach($user->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.backend_users.profile', $data);
    }

}
