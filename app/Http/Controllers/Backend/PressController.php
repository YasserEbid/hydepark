<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Pressrelease;

class PressController extends BackendController
{

    public function anyIndex()
    {
        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            foreach ($_POST['sort'] as $key => $value)
            {
                $sort = Pressrelease::find($value);
                $sort->sort = $key;
                $sort->save();
            }
            return redirect(\URL::Current())->with('success', 'Sorting successfully');
        }
        
        $data['result'] = Pressrelease::orderBy('sort', 'ASC')->paginate(10);
        $data['sorting'] = Pressrelease::orderBy('sort', 'ASC')->get();

        return view('backend.media_center.press_release.index', $data);
    }

    public function anyCreate()
    {
        $data = [];

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $object = new Pressrelease;

            if($object->validate($_POST))
            {
                if(isset($_POST['file']) && $_POST['file'] != '')
                {
                    rename('./uploads/temp/' . $_POST['file'], './uploads/' . $_POST['file']);
                }

                $object->fill($_POST);
                $object->sort = 3000;
                $object->created_by = $_SESSION['id'];
                $object->save();
                return redirect(\URL::current())->with('success', 'Insert successfully');
            }
            else
            {
                foreach ($object->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.media_center.press_release.create', $data);
    }

    public function anyEdit($id)
    {
        $data['object'] = Pressrelease::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);

            $object = $data['object'];

            if($object->validate($_POST))
            {
                if(isset($_POST['file']) && file_exists('./uploads/temp/' . $_POST['file']))
                {
                    rename('./uploads/temp/' . $_POST['file'], './uploads/' . $_POST['file']);
                }

                $object->fill($_POST);
                $object->created_by = $_SESSION['id'];
                $object->save();
                return redirect(\URL::current())->with('success', 'Update successfully');
            }
            else
            {
                foreach ($object->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.media_center.press_release.edit', $data);
    }

    public function anyDelete($id)
    {
        $object = Pressrelease::find($id);
        $object->delete();
    }

}
