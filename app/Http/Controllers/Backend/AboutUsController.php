<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AboutUs;

class AboutUsController extends BackendController
{

    public function anyIndex()
    {
        $data['aboutus'] = AboutUs::orderBy('id', 'asc')->get();

        return view('backend.about-us.index', $data);
    }

    public function anyEdit($id)
    {
        $data['about'] = AboutUs::find($id);

        if(isset($_POST['save']))
        {
            unset($_POST['save']);
            
            $about = $data['about'];
            if($about->validate($_POST))
            {
                $about->fill($_POST);
                $about->save();
                return redirect('backend/about-us/edit/'.$id)->with('success', 'Successfully update');
            }
            else
            {
                $data['validate_errors'] = $about->errors();
            }
        }

        return view('backend.about-us.edit', $data);
    }

}
