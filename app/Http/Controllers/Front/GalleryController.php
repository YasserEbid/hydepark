<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\EventsSections;
use App\Models\Events;
use App\Models\EventVideos;
use App\Models\EventPhotos;

class GalleryController extends FrontController
{

    public function anyIndex()
    {
        $data['sectionsGallery'] = EventsSections::all();

        /**
         * 
         * Videos
         */
        $data['videos'] = EventVideos::orderBy('sort', 'asc');
        if(isset($_GET['month']) && $_GET['month'] != '')
            $data['videos'] = $data['videos']->whereRaw('Month(date) = ?', [$_GET['month']]);

        if(isset($_GET['year']) && $_GET['year'] != '')
            $data['videos'] = $data['videos']->whereRaw('YEAR(date) = ?', [$_GET['year']]);

        $data['videos'] = $data['videos']->paginate(50);

        return view('front.gallery.index', $data);
    }

    public function getEvents($section_id = 0)
    {
        $data['events'] = Events::orderBy('id', 'desc');

        if(isset($_GET['name']) && $_GET['name'] != '')
            $data['events'] = $data['events']->where('name', $_GET['name']);

        if(isset($_GET['month']) && $_GET['month'] != '')
            $data['events'] = $data['events']->whereRaw('Month(date) = ?', [$_GET['month']]);

        if(isset($_GET['year']) && $_GET['year'] != '')
            $data['events'] = $data['events']->whereRaw('YEAR(date) = ?', [$_GET['year']]);

        if($section_id != 0)
            $data['events'] = $data['events']->where('section_id', $section_id);

        $data['events'] = $data['events']->paginate(50);

        if(isset($data["events"]) && $data["events"]->count() > 0)
        {
            $result_view = view('front.gallery.events-partial', $data);
            $result_content = $result_view->render();
            $status = 'success';
            $result_has_more = $data['events']->hasMorePages();
            $result_next_url = $data['events']->nextPageUrl();
            foreach($_GET as $key => $value)
            {
                if($key == 'page')
                    continue;
                $result_next_url.="&$key=$value";
            }

            $response = array(
                'status' => $status,
                'data' => $result_content,
                'more' => $result_has_more,
                'nexturl' => $result_next_url,
            );
            echo json_encode($response);
        }
        else
        {
            $data['status'] = 'null';
            $response = ['status' => 'null', 'data' => view('front.gallery.events-partial', $data)->render()];
            echo json_encode($response);
        }
    }

    public function getNameEvents($section_id = 0)
    {
        $data['events'] = Events::orderBy('id', 'desc');

        if($section_id != 0)
            $data['events'] = $data['events']->where('section_id', $section_id);

        $data['events'] = $data['events']->get();

        if(isset($data["events"]) && $data["events"]->count() > 0)
        {
            if($section_id != 0)
            {
                $section = EventsSections::find($section_id);
                if($section->is_month == true){
                    $data['is_month'] = true;
                }
                else{
                    $data['is_month'] = false;
                }
            }
            else
            {
                    $data['is_month'] = false;
            }

            $result_view = view('front.gallery.filter-partial', $data);
            $result_content = $result_view->render();
            $status = 'success';

            $response = array(
                'status' => $status,
                'data' => $result_content,
            );
            echo json_encode($response);
        }
        else
        {
            $data['status'] = 'null';
            $response = ['status' => 'null', 'data' => view('front.gallery.filter-partial', $data)->render()];
            echo json_encode($response);
        }
    }

    public function getVideos()
    {
        $data['videos'] = EventVideos::orderBy('sort', 'asc');

        if(isset($_GET['year']) && $_GET['year'] != '')
            $data['videos'] = $data['videos']->whereRaw('YEAR(date) = ?', [$_GET['year']]);

        $data['videos'] = $data['videos']->paginate(50);

        if(isset($data["videos"]) && $data["videos"]->count() > 0)
        {
            $result_view = view('front.gallery.videos-partial', $data);
            $result_content = $result_view->render();
            $status = 'success';
            $result_has_more = $data['videos']->hasMorePages();
            $result_next_url = $data['videos']->nextPageUrl();
            foreach($_GET as $key => $value)
            {
                if($key == 'page')
                    continue;
                $result_next_url.="&$key=$value";
            }

            $response = array(
                'status' => $status,
                'data' => $result_content,
                'more' => $result_has_more,
                'nexturl' => $result_next_url,
            );
            echo json_encode($response);
        }
        else
        {
            $data['status'] = 'null';
            $response = ['status' => 'null', 'data' => view('front.gallery.videos-partial', $data)->render()];
            echo json_encode($response);
        }
    }

}
