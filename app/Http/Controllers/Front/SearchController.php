<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\OurDevelopment;
use App\Models\OurDevelopmentAttachments;

class SearchController extends FrontController
{

    public function anyIndex()
    {
        if(!isset($_GET['find']))
            return redirect('.');

        $data['find'] = $_GET['find'];

        if(isset($_GET['find']))
        {
            $page_name = str_replace(' ', '-', $_GET['find']);

            $development = OurDevelopment::where('page_name', 'LIKE', '%'.$page_name.'%')->first();

            if(is_object($development))
            {
                return redirect('./our-development/'.$development->page_name);
            }
            else
            {
                $data['result'] = OurDevelopmentAttachments::
                        where('title', 'LIKE', '%'.$_GET['find'].'%');
                $data['count'] = $data['result']->count();
                $data['result'] = $data['result']->get();

                if(is_object($data['result']))
                {
                    return view('front.search', $data);
                }
                else
                {
                    return redirect('.');
                }
            }
        }
    }

}
