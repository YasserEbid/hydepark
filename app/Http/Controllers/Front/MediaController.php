<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Media;

class MediaController extends FrontController
{

    public function anyIndex()
    {
        $data['sections'] = \App\Models\MediaSections::where('parent_id',0)->orderBy('id', 'DESC')->get();
        $data['result'] = \App\Models\MediaSections::where('parent_id',0)->orderBy('id', 'DESC');
        if(isset($_GET['name']) && $_GET['name'] != '')
            $data['result'] = $data['result']->where('name', [$_GET['name']]);
        $data['result'] = $data['result']->with(['media' => function($q)
                    {
                        if(isset($_GET['year']) && $_GET['year'] != '')
                            $q->whereRaw('YEAR(date) = ?', [$_GET['year']]);
                    }])->paginate(10);

                return view('front.media.index', $data);
            }

            public function getMore()
            {
                $data['result'] = \App\Models\MediaSections::where('parent_id',0)->orderBy('id', 'DESC');
                if(isset($_GET['name']) && $_GET['name'] != '')
                    $data['result'] = $data['result']->where('name', [$_GET['name']]);
                $data['result'] = $data['result']->with(['media' => function($q)
                            {
                                if(isset($_GET['year']) && $_GET['year'] != '')
                                    $q->whereRaw('YEAR(date) = ?', [$_GET['year']]);
                            }])->paginate(10);

                        if(isset($data["result"]) && $data["result"]->count() > 0)
                        {
                            echo view('front.media.more-partial', $data);
                        }
                        else
                        {
                            $data['result'] = [];
                        }
                    }

                }
                