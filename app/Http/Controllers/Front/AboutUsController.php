<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\OurPatternPhotos;

class AboutUsController extends FrontController
{

    public function anyIndex()
    {
        $data['about_hydepark'] = AboutUs::where('tab_name', 'about_hydepark')->first();
        $data['message_from_ceo'] = AboutUs::where('tab_name', 'message_from_ceo')->first();
        $data['our_poeple'] = AboutUs::where('tab_name', 'our_poeple')->first();
        $data['our_values'] = AboutUs::where('tab_name', 'our_values')->first();
        $data['shareholders'] = AboutUs::where('tab_name', 'shareholders')->first();
        $data['partners'] = AboutUs::where('tab_name', 'partners')->first();

        return view('front.about-us', $data);
    }

    public function anyOurPattern()
    {
        $data['result'] = OurPatternPhotos::orderBy('sort', 'asc')->get();

        return view('front.our_pattern.index', $data);
    }

    public function getMorePhotos()
    {
        $data['result'] = OurPatternPhotos::orderBy('sort', 'asc')->paginate(3);

        if(isset($data["result"]) && $data["result"]->count() > 0)
        {
            echo view('front.our_pattern.more-partial', $data);
        }
        else
        {
            $data['result'] = [];
        }
    }
    
    public function privacyPolicy()
    {
        return view('front.privacy_policy');
    }
}
