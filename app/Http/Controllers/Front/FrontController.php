<?php

namespace App\Http\Controllers\Front;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class FrontController extends BaseController
{

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function __construct()
    {
        foreach ($_POST as $key => $value)
        {
            $_POST[$key] = strip_tags($value);
        }
        
        foreach ($_GET as $key => $value)
        {
            $_GET[$key] = strip_tags($value);
        }
        
        view()->share(['settings' => $this->settings()]);
    }

    private function settings()
    {
        $settings = \App\Models\Settings::all();
        foreach($settings as $setting)
        {
            $_settings[$setting->key] = $setting->value;
        }
        $this->settings = $_settings;

        return $_settings;
    }
    
}
