<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\OurDevelopment;
use App\Models\OurDevelopmentAttachments;
use App\Models\Registrations; 

class OurDevelopmentController extends FrontController
{

    public function anyIndex($name)
    {
        $data['result'] = OurDevelopment::where('page_name',$name)->first();
        $page_id = $data['result']->id;
        
        $data['attachments'] = OurDevelopmentAttachments::where('developement_id',$page_id)->orderBy('sort','ASC')->get();

        if($_SERVER["REQUEST_METHOD"] == "POST")
        {
            unset($_POST['send']);

//            $recaptcha = $_POST['g-recaptcha-response'];

                $google_url = "https://www.google.com/recaptcha/api/siteverify";
                $secret = '6LfQGhEUAAAAAPOuQszTcvLxDBhgxs7KvfdKTCl_';
                $ip = $_SERVER['REMOTE_ADDR'];
//                $url = $google_url . "?secret=" . $secret . "&response=" . $recaptcha . "&remoteip=" . $ip;

                    //Don't Ropot
                    //unset($_POST['g-recaptcha-response']);
                    $registeration = new Registrations;
                    if($registeration->validate($_POST))
                    {
                        $registeration->fill($_POST);
                        $registeration->save();
                        return redirect()->back()->with('success', 'Successful Send Thank You');
                    }
                    else
                    {
                        $data['validate_errors'] = $registeration->errors();
                    }

            }

        return view('front.our-development.pages', $data);
    }
    
    public function anyMasterplan()
    {
        $data = [];

        return view('front.our-development.masterplan', $data);
    }
    
}
