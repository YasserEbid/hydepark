<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Sliders;
use App\Models\SectionsHome;
use App\Models\SectionTwoBanners;

class HomeController extends FrontController
{

    public function anyIndex()
    {
        $data['sliders'] = Sliders::all();
        $data['sectionOne'] = SectionsHome::where('type','one')->first();
        $data['sectionTwo'] = SectionsHome::where('type','two')->first();
        $data['banners'] = SectionTwoBanners::where('sort','desc')->get();

        return view('front.index', $data);
    }

}
