<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Presskit;

class PresskitController extends FrontController
{

    public function anyIndex()
    {
        $data['result'] = Presskit::orderBy('sort', 'ASC')->paginate(12);

//        if(isset($_GET['month']) && $_GET['month'] != '')
//            $data['result'] = $data['result']->whereRaw('Month(date) = ?', [$_GET['month']]);
//
//        if(isset($_GET['year']) && $_GET['year'] != '')
//            $data['result'] = $data['result']->whereRaw('YEAR(date) = ?', [$_GET['year']]);
//
//        $data['result'] = $data['result']->paginate(12);

        return view('front.presskit.index', $data);
    }

    public function getMore()
    {
        $data['result'] = Presskit::orderBy('sort', 'ASC')->paginate(12);

//        if(isset($_GET['month']) && $_GET['month'] != '')
//            $data['result'] = $data['result']->whereRaw('Month(date) = ?', [$_GET['month']]);
//
//        if(isset($_GET['year']) && $_GET['year'] != '')
//            $data['result'] = $data['result']->whereRaw('YEAR(date) = ?', [$_GET['year']]);
//
//        $data['result'] = $data['result']->paginate(12);

        if(isset($data["result"]) && $data["result"]->count() > 0)
        {
            echo view('front.presskit.more-partial', $data);
        }
        else
        {
            $data['result'] = [];
        }
    }

}
