<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Sliders;
use App\Models\SectionsHome;
use App\Models\SectionTwoBanners;

class ConstructionProgressViewerController extends FrontController
{

    public function anyIndex()
    {
        $data = [];

        return view('front.construction-progress-viewer', $data);
    }

}
