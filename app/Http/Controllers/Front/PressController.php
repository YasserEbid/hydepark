<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Pressrelease;

class PressController extends FrontController
{

    public function anyIndex()
    {
        $data['resultEN'] = Pressrelease::where('language', 'english')->orderBy('sort', 'ASC');

        if(isset($_GET['month']) && $_GET['month'] != '')
            $data['resultEN'] = $data['resultEN']->whereRaw('Month(date) = ?', [$_GET['month']]);

        if(isset($_GET['year']) && $_GET['year'] != '')
            $data['resultEN'] = $data['resultEN']->whereRaw('YEAR(date) = ?', [$_GET['year']]);

        $data['resultEN'] = $data['resultEN']->paginate(12);
        
        $data['resultAR'] = Pressrelease::where('language', 'arabic')->orderBy('sort', 'ASC');

        if(isset($_GET['month']) && $_GET['month'] != '')
            $data['resultAR'] = $data['resultAR']->whereRaw('Month(date) = ?', [$_GET['month']]);

        if(isset($_GET['year']) && $_GET['year'] != '')
            $data['resultAR'] = $data['resultAR']->whereRaw('YEAR(date) = ?', [$_GET['year']]);

        $data['resultAR'] = $data['resultAR']->paginate(12);

        return view('front.pressrelease.index', $data);
    }

    public function getMore()
    {
        $data['resultEN'] = Pressrelease::where('language', 'english')->orderBy('sort', 'ASC');

        if(isset($_GET['month']) && $_GET['month'] != '')
            $data['resultEN'] = $data['resultEN']->whereRaw('Month(date) = ?', [$_GET['month']]);

        if(isset($_GET['year']) && $_GET['year'] != '')
            $data['resultEN'] = $data['resultEN']->whereRaw('YEAR(date) = ?', [$_GET['year']]);

        $data['resultEN'] = $data['resultEN']->paginate(12);
        
        $data['resultAR'] = Pressrelease::where('language', 'arabic')->orderBy('sort', 'ASC');

        if(isset($_GET['month']) && $_GET['month'] != '')
            $data['resultAR'] = $data['resultAR']->whereRaw('Month(date) = ?', [$_GET['month']]);

        if(isset($_GET['year']) && $_GET['year'] != '')
            $data['resultAR'] = $data['resultAR']->whereRaw('YEAR(date) = ?', [$_GET['year']]);

        $data['resultAR'] = $data['resultAR']->paginate(12);

        if(isset($data["resultEN"]) && $data["resultEN"]->count() > 0)
        {
            echo view('front.pressrelease.more-partial', $data);
        }
        else
        {
            $data['resultEN'] = [];
        }
        
        if(isset($data["resultAR"]) && $data["resultAR"]->count() > 0)
        {
            echo view('front.pressrelease.more-partial', $data);
        }
        else
        {
            $data['resultAR'] = [];
        }
    }

}
