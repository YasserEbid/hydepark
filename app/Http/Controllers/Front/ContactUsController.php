<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Contacts;

class ContactUsController extends FrontController
{

    public function anyIndex()
    {
        $data = [];

        if($_SERVER["REQUEST_METHOD"] == "POST")
        {
            unset($_POST['send']);

            $recaptcha = $_POST['g-recaptcha-response'];
            if(!empty($recaptcha))
            {
                $google_url = "https://www.google.com/recaptcha/api/siteverify";
                $secret = '6LfQGhEUAAAAAPOuQszTcvLxDBhgxs7KvfdKTCl_';
                $ip = $_SERVER['REMOTE_ADDR'];
                $url = $google_url . "?secret=" . $secret . "&response=" . $recaptcha . "&remoteip=" . $ip;
                if(!empty($url))
                {
                    //Don't Ropot
                    unset($_POST['g-recaptcha-response']);
                    $contact = new Contacts;
                    if($contact->validate($_POST))
                    {
                        $contact->fill($_POST);
                        $contact->save();
                        return redirect('./contact-us')->with('success', 'Successful Send Thank You');
                    }
                    else
                    {
                        $data['validate_errors'] = $contact->errors();
                    }
                }
                else
                {
                    $data['validate_errors'] = ["Please re-enter your reCAPTCHA."];
                }
            }
            else
            {
                $data['validate_errors'] = ["Please click on the reCAPTCHA box."];
            }
        }

        return view('front.contact-us', $data);
    }

}
