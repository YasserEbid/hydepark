<?php namespace App\Http\Middleware;

use Closure;

class isUser {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
            if(!isset($_SESSION['admin']) && !strstr($_SERVER['REQUEST_URI'], 'admins/login') && !strstr($_SERVER['REQUEST_URI'], 'admins/forgot-password') ) 
            {
                return redirect('./admins/login');
            }
            return $next($request);
	}

}
