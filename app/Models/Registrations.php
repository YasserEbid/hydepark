<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Registrations extends BaseModel
{
    protected $table = 'registrations';

    public $rules=[
        'name'=>'required',
        'phone_number'=>"required|numeric",
        'email'=>"required|Between:3,64|E-Mail",
        'job_title'=>'required',
    ];
    
    protected $guarded=['id'];
}
