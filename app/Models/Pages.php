<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends BaseModel
{
    protected $table = 'pages';
    public $timestamps=true;
    public $ignored_unique = [
        'page',
        'link',
    ] ;
    public $rules=[
        'page'=>'required|unique:pages,page',
        'link'=>'required|unique:pages,link'
    ];
    
    protected $guarded=['id'];
    
    function actions()
    {
        return $this->hasMany('\App\Models\PagesActions','page_id','id');
    }
    
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
    
}
