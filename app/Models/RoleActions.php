<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleActions extends BaseModel
{
    protected $table = 'role_actions';
    public $timestamps=true;
    public $rules=[
        'action_id'=>'required',
        //'role_id'=>'required'
    ];
    
    protected $guarded=['id'];
       
}
