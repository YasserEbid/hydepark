<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailBox extends BaseModel
{

    protected $table = 'mailbox';
    public $timestamps = true;
    public $ignored_unique = [
        'email'
    ];
    public $rules = [
        'email' => 'required|unique:setup_mails,email',
        'password' => 'required',
        'type' => 'required',
    ];
    protected $guarded = ['id'];
    protected $_stream;
    protected $_search;

    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }

    public function setup($type, $domain, $email, $password, $state)
    {
        if($type == 'domain')
        {
            $imap = '{mail.'.$domain.'.com:993/imap/ssl}'.$state;
        }
        else
        if($type == 'gmail')
        {
            $imap = '{imap.gmail.com:993/imap/ssl}'.$state;
        }
        else
        if($type == 'yahoo')
        {
            $imap = '{imap.mail.yahoo.com:993/imap/ssl}'.$state;
        }
        else
        if($type == 'outlook')
        {
            $imap = '{imap-mail.outlook.com:993/imap/ssl}'.$state;
        }

        $mailbox = array(
            'mailbox' => $imap,
            'username' => $email,
            'password' => $password
        );

        $stream = imap_open($mailbox['mailbox'], $mailbox['username'], $mailbox['password'])
                or die('Cannot connect to mailbox: '.imap_last_error());

        $search = imap_search($stream, 'SINCE '.date('d-M-Y', strtotime("-1 week")));
//        $search = imap_search($stream, 'UNSEEN');
//        $search = imap_search($stream, 'SEEN');

        $this->_search = $search;
        $this->_stream = $stream;
    }

    public function getMails()
    {
        if(!empty($this->_search))
            return $this->_search;
        else
            return [];
    }

    function header($email_id)
    {
        return imap_header($this->_stream, $email_id);
    }
    
    function mailName($email_id)
    {
        $sender = imap_header($this->_stream, $email_id)->sender;
        return $sender[0]->mailbox.'@'.$sender[0]->host;
    }
    
    function url($email_id)
    {
//       dd(imap_fetchstructure($this->_stream, $email_id));
//       dd(quoted_printable_decode($this->_stream));
    }

    function count()
    {
        $count = count($this->_search);

        return $count;
    }

    function closeMail()
    {
        return imap_close($this->_stream);
    }

}
