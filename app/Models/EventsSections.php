<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventsSections extends BaseModel
{

    protected $table = 'events_sections';
    public $timestamps = true;
    public $ignored_unique = [
        'name',
    ];
    public $rules = [
        'name' => 'required|unique:events_sections,name',
//        'date' => 'required'
    ];
    protected $guarded = ['id'];

    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
    
    function events()
    {
        return $this->hasMany('\App\Models\Events', 'section_id', 'id')->orderBy('date','DESC');
    }
    
}
