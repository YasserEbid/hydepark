<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Events extends BaseModel
{

    protected $table = 'events';
    public $timestamps = true;
    public $ignored_unique = [
        'name',
    ];
    public $rules = [
        'name' => 'required|unique:events,name',
//        'date' => 'required'
    ];
    protected $guarded = ['id'];

    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
    
    function photos()
    {
        return $this->hasMany('\App\Models\EventPhotos', 'event_id', 'id')->orderBy('sort','asc');
    }
    
    function section()
    {
        return $this->hasOne('\App\Models\EventsSections', 'id', 'section_id');
    }
    
}
