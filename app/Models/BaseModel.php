<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;

class BaseModel extends Model
{

    public $errors;
    public $ignored_unique; // the column that we want to ignore the id on unique validation 
  

    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules());

        // check for failure
        if($v->fails())
        {
            // set errors and return false
            $this->errors = $v->errors()->all();
            return false;
        }

        // validation pass
        return true;
    }
    
    public function rules()
    {
        if(!empty($this->ignored_unique))
        foreach($this->ignored_unique as $column)
        {
            $this->rules[$column] = $this->rules[$column].','.$this->id ;
        }
        return $this->rules ;
    }
    
    public function errors()
    {
        return $this->errors;
    }

    function mylang($lang)
    {
        $langs = [
            'en' => 1,
            'ar' => 2,
            'fr' => 3
                ];

        return $langs[$lang];
    }
    
    function createdAt()
    {
        $time = strtotime($this->created_at);
        return date('Y/m/d &\nb\sp; g:i A ', $time);
    }
    
    function updatedAt()
    {
        $time = strtotime($this->updated_at);
        return date('Y/m/d &\nb\sp; g:i A ', $time);
    }

    function deletedAt()
    {
        $time = strtotime($this->deleted_at);
        return date('Y/m/d &\nb\sp; g:i A ', $time);
    }

}
