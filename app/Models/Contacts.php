<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contacts extends BaseModel
{
    protected $table = 'contacts';
    public $timestamps=true;
    public $ignored_unique = [
//        'email',
//        'phone',
    ] ;
    public $rules=[
        'name'=>'required',
        'phone'=>"required|numeric",
        'email'=>"required|Between:3,64|E-Mail",
        'message'=>'required',
    ];
    
    protected $guarded=['id'];
        
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'contacted_by');
    }
    
}
