<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectionsHome extends BaseModel
{
    protected $table = 'sections_home';
    public $timestamps=true;
    public $ignored_unique = [
//        'tab',
//        'phone',
    ] ;
    public $rules=[
        'title'=>"required",
//        'sub_title'=>"required",
        'description'=>"required"
    ];
    
    protected $guarded=['id'];
        
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
    
}
