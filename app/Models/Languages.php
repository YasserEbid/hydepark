<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Languages extends BaseModel
{
    protected $table = 'languages';
    public $timestamps=false;
    public $ignored_unique = [
        'name',
        'symbol',
    ] ;
    public $rules=[
        'name'=>'required|unique:languages,name',
        'symbol'=>'required|unique:languages,symbol',
        'rtl'=>'required',
        'image'=>'required'
    ];
    protected $guarded=['id'];       
       
}
