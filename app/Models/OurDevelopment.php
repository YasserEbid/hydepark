<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OurDevelopment extends BaseModel
{
    protected $table = 'our_development';
    public $timestamps=true;
    public $ignored_unique = [
//        'page_name',
//        'phone',
    ] ;
    public $rules=[
//        'page_name'=>"required|unique:our_development,page_name",
        'description'=>"required"
    ];
    
    protected $guarded=['id'];
        
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
    
}
