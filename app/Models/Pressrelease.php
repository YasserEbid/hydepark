<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pressrelease extends BaseModel
{

    protected $table = 'pressrelease';
    public $timestamps = true;
    public $ignored_unique = [
//        'name',
    ];
    public $rules = [
        'title' => 'required',
        'file' => 'required',
    ];
    protected $guarded = ['id'];

    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }

}
