<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailThemes extends BaseModel
{

    protected $table = 'email_themes';
    public $timestamps = true;
     public $ignored_unique = [
        'name'
    ] ;
    public $rules = [
        'name' => 'required|unique:email_themes,name',
        'header' => 'required',
        'footer' => 'required'
    ];
    protected $guarded = ['id'];
    
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }

}
