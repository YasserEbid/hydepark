<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Presskit extends BaseModel
{

    protected $table = 'presskit';
    public $timestamps = true;
    public $ignored_unique = [
//        'name',
    ];
    public $rules = [
        'title' => 'required',
        'image' => 'required',
        'file' => 'required',
    ];
    protected $guarded = ['id'];

    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }

}
