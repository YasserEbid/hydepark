<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends BaseModel
{
    protected $table = 'roles';
    public $timestamps=true;
    public $ignored_unique = [
        'name'
    ] ;
    public $rules=[
        'name'=>'required|unique:roles,name'
    ];
    
    protected $guarded=['id'];
    
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
       
}
