<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventPhotos extends BaseModel
{

    protected $table = 'event_photos';
    public $timestamps = true;
    public $ignored_unique = [
//        'name',
    ];
    public $rules = [
        'event_id' => 'required',
//        'name' => 'required',
        'image' => 'required'
    ];
    protected $guarded = ['id'];

    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }

}
