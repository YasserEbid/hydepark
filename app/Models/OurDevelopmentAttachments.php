<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OurDevelopmentAttachments extends BaseModel
{
    protected $table = 'our_development_attachments';
    public $timestamps=true;
    public $ignored_unique = [
//        'tab',
//        'phone',
    ] ;
    public $rules=[
        'developement_id'=>"required",
        'title'=>"required",
        'image'=>"required"
    ];
    
    protected $guarded=['id'];
        
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
    
    function pageName()
    {
        return $this->hasOne('\App\Models\OurDevelopment', 'id', 'developement_id');
    }
    
}
