<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscribers extends BaseModel
{

    protected $table = 'subscribers';
    public $timestamps = true;
    public $rules = [
        'themes'   => 'required',
        'subject'   => 'required',
        'content'   => 'required'
    ];
    protected $guarded = ['id'];

}
