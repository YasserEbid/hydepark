<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PagesActions extends BaseModel
{
    protected $table = 'pages_actions';
    public $timestamps=true;
    public $rules=[
        'action'=>'required',
        'name'=>'required'
    ];
    
    protected $guarded=['id'];
    
    function roleActions()
    {
        return $this->hasMany('\App\Models\RoleActions','action_id','id');
    }
    
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
    
}
