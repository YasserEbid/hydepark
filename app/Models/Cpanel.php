<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cpanel extends BaseModel
{

    protected $table = 'cpanel';
    public $timestamps = true;
    public $ignored_unique = [
//        'email',
//        'phone',
            ];
    public $rules = [
        'ip' => 'required',
        'domain' => "required",
        'username' => "required",
        'password' => 'required',
        'port' => 'required',
    ];
    protected $guarded = ['id'];

    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }

    function checkCpanelAuthentication($account)
    {
        $xmlapi = new \App\Http\Controllers\Common\xmlapi($account->ip);
        $xmlapi->password_auth($account->username, $account->password);
        $xmlapi->set_port($account->port);
        $xmlapi->set_debug(1);
        $states = $xmlapi->listips();
        if($states == null)
            return true;
        else
            return false;
    }

}
