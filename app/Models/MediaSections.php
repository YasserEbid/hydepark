<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaSections extends BaseModel
{

    protected $table = 'media_sections';
    public $timestamps = true;
    public $ignored_unique = [
        'name',
    ];
    public $rules = [
        'name' => 'required|unique:media_sections,name',
//        'date' => 'required'
    ];
    protected $guarded = ['id'];

    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }

    function media()
    {
        return $this->hasMany('\App\Models\Media', 'section_id', 'id')->orderBy('date', 'DESC');
    }

    function children()
    {
        return $this->hasMany('\App\Models\MediaSections', 'parent_id')->orderBy('id', 'ASC');
    }

}
