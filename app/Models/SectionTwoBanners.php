<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectionTwoBanners extends BaseModel
{
    protected $table = 'section_two_banners';
    public $timestamps=true;
    public $ignored_unique = [
//        'tab',
//        'phone',
    ] ;
    public $rules=[
        'name'=>"required",
        'image'=>"required"
    ];
    
    protected $guarded=['id'];
        
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
    
}
