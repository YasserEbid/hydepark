<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BackendUsers extends BaseModel
{
    protected $table = 'backend_users';
    public $timestamps=true;
    public $ignored_unique = [
        'email',
        'phone',
    ] ;
    public $rules=[
        'name'=>'required',
        'email'=>"required|Between:3,64|unique:backend_users,email",
        'phone'=>"required|numeric|unique:backend_users,phone",
        'password'=>'min:5|required',
        'c_password'=>'same:password',
        'image'=>'required'
    ];
    
    protected $guarded=['id'];
    function role()
    {
        return $this->hasOne('\App\Models\Roles','id','role_id');
    }
    
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
    
}
