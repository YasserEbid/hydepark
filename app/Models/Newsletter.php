<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends BaseModel
{
    protected $table = 'newsletter';
    public $timestamps=true;
    public $ignored_unique = [
//        'email',
//        'phone',
    ] ;
    public $rules=[
        'name'=>'required',
        'phone'=>"required|numeric",
        'email'=>"required|Between:3,64|E-Mail",
        'type'=>'required',
//        'unit_number'=>'required',
    ];
    
    protected $guarded=['id'];
        
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'contacted_by');
    }
    
}
