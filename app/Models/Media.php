<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends BaseModel
{

    protected $table = 'media';
    public $timestamps = true;
    public $ignored_unique = [
//        'name',
    ];
    public $rules = [
        'title' => 'required',
//        'image' => 'required',
        'file' => 'required',
    ];
    protected $guarded = ['id'];

    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
    
    function section()
    {
        return $this->hasOne('\App\Models\MediaSections', 'id', 'section_id');
    }
    
}
