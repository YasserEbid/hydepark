<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends BaseModel
{
    protected $table = 'about_us';
    public $timestamps=true;
    public $ignored_unique = [
//        'tab_name',
//        'phone',
    ] ;
    public $rules=[
//        'tab_name'=>"required|unique:about_us,tab_name",
        'description'=>"required"
    ];
    
    protected $guarded=['id'];
        
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
    
}
