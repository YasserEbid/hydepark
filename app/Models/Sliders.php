<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sliders extends BaseModel
{
    protected $table = 'sliders';
    public $timestamps=true;
    public $ignored_unique = [
//        'tab',
//        'phone',
    ] ;
    public $rules=[
        'image'=>"required"
    ];
    
    protected $guarded=['id'];
        
    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }
    
}
