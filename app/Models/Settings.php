<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends BaseModel
{
    protected $table = 'settings';
    public $timestamps=true;
    public $ignored_unique = [
        'key'
    ] ;
    public $rules=[
        'value'=>'required',
        'key'=>'required|unique:settings,key'
    ];
    protected $guarded=['id'];       
       
}
