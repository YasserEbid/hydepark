<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OurPatternPhotos extends BaseModel
{

    protected $table = 'our_pattern_photos';
    public $timestamps = true;
    public $ignored_unique = [
//        'name',
    ];
    public $rules = [
        'name' => 'required',
        'image' => 'required',
    ];
    protected $guarded = ['id'];

    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }

}
