<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventVideos extends BaseModel
{

    protected $table = 'event_videos';
    public $timestamps = true;
    public $ignored_unique = [
//        'name',
    ];
    public $rules = [
//        'type' => 'required',
//        'name' => 'required',
        'video_link' => 'required',
//        'date' => 'required'
    ];
    protected $guarded = ['id'];

    function admin()
    {
        return $this->hasOne('\App\Models\BackendUsers', 'id', 'created_by');
    }

}
