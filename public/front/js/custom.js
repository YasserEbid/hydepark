(function($) {
	"use strict";
	
	$(document).ready(function() {
		/*try {
			$("#header").sticky({
				topSpacing: 0,
				className: 'sticky'
			});
		} catch (err) {
		
		}* /
		var HHieght = $('#header').height();
		headerStyle(HHieght);*/
		$(window).on('scroll', function() {
			//headerStyle(HHieght);
			var windowpos = $(window).scrollTop();
			if (windowpos >= 100) {
				$('#header').addClass('sticky');
				$('#SideNav').addClass('stickSide');
			} else {
				$('#header').removeClass('sticky');
				$('#SideNav').removeClass('stickSide');
			}
		});
		
		if($('#SideNav').length) {
			var sideNavTop = $('#SideNav').offset().top;
			$(window).on('scroll', function() {
				var windowpos = $(window).scrollTop();
				if (windowpos >= sideNavTop) {
					$('#SideNav').addClass('stickSide');
				} else {
					$('#SideNav').removeClass('stickSide');
				}
			});
			/*
			$('#SideNav ul a').click(function() {
				$('html, body').animate({
					scrollTop: (sideNavTop - $('header').height())
				});
			});*/
		}
		
		$(".radio input").change(function() {
			if ($(this).val() == 1) {
				$(".unitNo").removeClass('hide');
			} else {
				$(".unitNo").addClass('hide');
			}
		});
	});
	
	$(document).on('click', '.search', function(e) {
		e.preventDefault();
		openSearch();
	});
	$(document).on('click', '.search-close', function() {
		closeSearch();
	});
	
	function openSearch() {
		$('header .topLinks .search').addClass('active');
		$('.searchHolder').addClass('active');
		$('.search-input').focus().val('');
	}
	
	function closeSearch() {
		$('header .topLinks .search').removeClass('active');
		$('.searchHolder').removeClass('active');
	}
	function headerStyle(HHieght) {
		if($('#header').length){
			var windowpos = $(window).scrollTop();
			if (windowpos >= 50) {
				$('body').css('padding-top', HHieght);
				$('#header').addClass('sticky');
			} else {
				$('body').css('padding-top', 0);
				$('#header').removeClass('sticky');
			}
		}
	}
})(window.jQuery);
