@extends('front.layout')
@section('title', 'Search')
@section('content')

<div id="Breadcrumps">
    <div class="col-md-10 col-md-offset-1">
        <a href="<?= url('.'); ?>">HOME</a> > <a>OUR DEVELOPMENT</a> > <?= strtoupper('search');?> > <?= $find;?>
    </div>
    <div class="clearfix"></div>
</div>

<div id="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="contentWithBGInner MinHeight clearfix">
                <div class="col-xs-12 col-sm-10">
                    <div class="hyde-park-media-p">

                        <div>
                            <div class="headTitleHolder">
                                <h1 class="headTitle headTitleWzstyle"><?= ucwords('result search');?> (<?= $count;?>)</h1>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                    <?php foreach($result as $attachment){?>
                        <div class="BrochureItem col-md-4">
                            <div class="image-holder">
                                <img src="<?= url('uploads/'.$attachment->image);?>">
                                <?php if($attachment->file !=''){?>
                                <a href="<?= url('uploads/'.$attachment->file);?>" target="_blank"></a>
                                <?php }?>
                            </div>
                            <div class="BrochureName">
                                <?= $attachment->title;?>
                            </div>
                        </div>
                    <?php }?>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>


@stop()