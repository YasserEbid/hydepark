@extends('front.layout')
@section('title', 'Master Plan')
@section('content')

<div id="wrapper">

    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="headTitleHolder">
                <h1 class="headTitle headTitleWzstyle">Master Plan</h1>
            </div>
        </div>
    </div>

    <nav id="SideNav" class="navbar navbar-default sidebar" role="navigation">
        <div class="">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li id="ApartmentsLink" class="Links">
                        <a onclick="SelectArea('Apartments',300)">
                            <div><img src="<?= url('./front/images/Icons/1.png');?>"></div>
                            Apartments</a>
                    </li>
                    <li id="VillasLink" class="Links">
                        <a onclick="SelectArea('Villas',250)">
                            <div><img src="<?= url('./front/images/Icons/3.png');?>"></div>
                            Villas</a>
                    </li>
                    <li id="ParkLink" class="hidden" style="display:none; visibility:hidden;">
                        <a onclick="SelectArea('Park',200)">
                            <div><img src="<?= url('./front/images/Icons/12.png');?>"></div>
                            The Park</a>
                    </li>
                    <li id="OfficesLink" class="Links">
                        <a onclick="SelectArea('Offices',300)">
                            <div><img src="<?= url('./front/images/Icons/7.png');?>"></div>
                            Office Park</a>
                    </li>
                    <li id="RetailLink" class="Links">
                        <a onclick="SelectArea('Retail',200)">
                            <div><img src="<?= url('./front/images/Icons/5.png');?>"></div>
                            Retail Complex</a>
                    </li>
                    <li id="HospitalLink" class="Links">
                        <a onclick="SelectArea('Hospital',300)" >
                            <div><img src="<?= url('./front/images/Icons/2.png');?>"></div>
                            Medical Centre</a>
                    </li>
                    <li id="SchoolsLink" class="Links">
                        <a onclick="SelectArea('Schools',225)">
                            <div><img src="<?= url('./front/images/Icons/6.png');?>"></div>
                            Schools</a>
                    </li>
                    <li id="NurseryLink" class="Links">
                        <a onclick="SelectArea('Nursery',300)" >
                            <div><img src="<?= url('./front/images/Icons/4.png');?>"></div>
                            Nursery</a>
                    </li>
                    <li id="ClubHouseLink" class="Links">
                        <a onclick="SelectArea('ClubHouse',250)">
                            <div><img src="<?= url('./front/images/Icons/8.png');?>"></div>
                            Clubhouse</a>
                    </li>
                    <!--
                    <li id="CountryclubLink" class="Links">
                            <a onclick="SelectArea('Countryclub',100)">
                                    <div><img src="../images/Icons/9.png"></div>
                                    Country club</a>
                    </li> -->
                    <li  id="SportingclubLink" class="Links">
                        <a onclick="SelectArea('Sportingclub',100)">
                            <div><img src="<?= url('./front/images/Icons/10.png');?>"></div>
                            Sporting Club</a>
                    </li>
                    <li class="hidden" style="display:none; visibility:hidden;">
                        <a onclick="SelectArea('Gasstation',300)" id="GasstationLink" class="Links">
                            <div><img src="<?= url('./front/images/Icons/11.png');?>"></div>
                            Gas station</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">

        <div class="row">
            <div class="text-center">
                <div id="MasterPlan">
                    <img src="<?= url('./front/images/masterPlan/MasterPlanMain.jpg');?>" id="MasterPlanPhotoMain" >
                    <img src="<?= url('./front/images/masterPlan/MasterPlan.jpg');?>" id="MasterPlanPhoto" >
                    <img src="<?= url('./front/images/masterPlan/Apartments.png');?>" id="Apartments" class="MasterItem" >
                    <img src="<?= url('./front/images/masterPlan/Hospital.png');?>" id="Hospital" class="MasterItem">
                    <img src="<?= url('./front/images/masterPlan/Villas.png');?>" id="Villas" class="MasterItem">
                    <img src="<?= url('./front/images/masterPlan/Park.png');?>" id="Park" class="MasterItem">
                    <img src="<?= url('./front/images/masterPlan/Nursery.png');?>" id="Nursery" class="MasterItem">
                    <img src="<?= url('./front/images/masterPlan/Retail.png');?>" id="Retail" class="MasterItem">
                    <img src="<?= url('./front/images/masterPlan/Schools.png');?>" id="Schools" class="MasterItem">
                    <img src="<?= url('./front/images/masterPlan/Offices.png');?>" id="Offices" class="MasterItem">
                    <img src="<?= url('./front/images/masterPlan/club.png');?>" id="ClubHouse" class="MasterItem">
                    <!--<img src="<?= url('./front/images/masterPlan/ClubHouse.png');?>" id="ClubHouse" class="MasterItem">
                    <img src="<?= url('./front/images/masterPlan/Countryclub.png');?>" id="Countryclub" class="MasterItem">-->
                    <img src="<?= url('./front/images/masterPlan/Sportingclub.png');?>" id="Sportingclub" class="MasterItem">
                    ><img src="<?= url('./front/images/masterPlan/Gasstation.html');?>" id="Gasstation" class="MasterItem">
                </div>
            </div>

            <script>
                function SelectArea(AreaID,scroll) {
                    $('#MasterPlanPhotoMain').hide();
                    $('#MasterPlanPhoto').show();
                    $(".MasterItem").removeClass("ShowMasterItem");
                    $(".Links").removeClass("active");

                    $("#" + AreaID).addClass("ShowMasterItem");
                    $("#" + AreaID + "Link").addClass("active");
                    //window.scrollTo(0,0);
                    $("html, body").animate({ scrollTop: $('#SideNav').offset().top+50 }, "slow");
	             
			
                }
            </script>
        </div>

    </div>

</div>



@stop()