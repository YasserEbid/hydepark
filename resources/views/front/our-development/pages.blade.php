@extends('front.layout')
@section('title', strtoupper(str_replace('-', ' ', $result->page_name)))
@section('content')

<?php if($result->image != ''){?>
<img src="<?= url('uploads/'.$result->image); ?>"><div class="clearfix"></div>
<?php }?>

<?php if(Request::segment(1) == "our-development" && Request::segment(2) == "retail-complex"){?>
        <div id="retails-page">
        <div id="owl-demo" class="owl-bannerCarousel">

            <div class="item"><img src="./front/files/retail/1.jpg" /></div>

            <div class="item"><img src="./front/files/retail/2.jpg" /></div>

            <div class="item"><img src="./front/files/retail/3.jpg" /></div>
        </div>
        <script>
                $(document).ready(function() {
                        $("#owl-demo").owlCarousel({
                                items : 1,
                                navigation : false,
                                singleItem : true,
                                autoPlay : true,
                                pagination : true
                });
            });
        </script>
    </div>
<div class="clearfix"></div>
<?php }?>

<?php if(Request::segment(1) == "our-development" && Request::segment(2) == "office-park"){?>
        <div id="retails-page">
        <div id="owl-demo" class="owl-bannerCarousel">

            <div class="item"><img src="./front/files/offices/1.jpg" /></div>
            <div class="item"><img src="./front/files/offices/2.jpg" /></div>
            <div class="item"><img src="./front/files/offices/3.jpg" /></div>
            <div class="item"><img src="./front/files/offices/4.jpg" /></div>
            <div class="item"><img src="./front/files/offices/5.jpg" /></div>
            <div class="item"><img src="./front/files/offices/6.jpg" /></div>
            <div class="item"><img src="./front/files/offices/7.jpg" /></div>
            <div class="item"><img src="./front/files/offices/8.jpg" /></div>
            <div class="item"><img src="./front/files/offices/9.jpg" /></div>

        </div>
        <script>
                $(document).ready(function() {
                        $("#owl-demo").owlCarousel({
                                items : 1,
                                navigation : false,
                                singleItem : true,
                                autoPlay : true,
                                pagination : true
                });
            });
        </script>
    </div>
<div class="clearfix"></div>
<?php }?>

<div id="Breadcrumps">
    <div class="col-md-10 col-md-offset-1">
        <a href="<?= url('.'); ?>">HOME</a> > <a>OUR DEVELOPMENT</a> > 
        <?php $string = str_replace('-', ' ', $result->page_name); echo strtoupper($string);?>
    </div>
    <div class="clearfix"></div>
</div>

<div id="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="contentWithBGInner MinHeight clearfix">
            <?php
            use App\Http\Controllers\Common\Helpers;
            if(session('success')!='')
            $success = session('success') ;
            ?>
            <?php if(isset($validate_errors) or isset($success)){?>
            <?php if(isset($validate_errors)){?>

            <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php foreach($validate_errors as $error){?>
            <p><?php echo $error; ?></p>
            <?php }?>
            </div>
            <?php } ?>
            <?php if(!empty($success)){?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><?php echo $success; ?></p>
            </div>
            <?php } ?>

            <?php } ?>
                <div class="col-xs-12 col-sm-10">
                    <div class="hyde-park-media-p">
                        <div>
                            <div class="headTitleHolder">
                                <h1 class="headTitle headTitleWzstyle"><?php $string = str_replace('-', ' ', $result->page_name); echo ucwords($string);?></h1>
                            </div>
                            <?= $result->description; ?>
                        </div>
                    </div>
                    
                    <?php if(!empty($attachments)){?>
                    <div class="row">
                    <?php foreach($attachments as $attachment){?>
                        <div class="BrochureItem col-md-4">
                            <div class="image-holder">
                                <img src="<?= url('uploads/'.$attachment->image);?>">
                                <?php if($attachment->file !=''){?>
                                <a href="<?= url('uploads/'.$attachment->file);?>" target="_blank"></a>
                                <?php }?>
                            </div>
                            <div class="BrochureName">
                                <?= $attachment->title;?>
                            </div>
                        </div>
                    <?php }?>
                    </div>
                    <?php }?>
                    
                </div>
                <?php if(Request::segment(1) == "our-development" && Request::segment(2) == "residential"){?>
                <br /><br /><br />
                <div class="row">
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-md-6">
                                <a class="resid-sections" href="<?= url('./our-development/apartments');?>">
                                    <img src="./front/files/residential/1.jpg" />
                                    <h2 class="text-center">Apartments</h2>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a class="resid-sections" href="<?= url('./our-development/villas');?>">
                                    <img src="./front/files/residential/2.jpg" />
                                    <h2 class="text-center">Villas</h2>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
                
                <?php if(Request::segment(1) == "our-development" && Request::segment(2) == "office-park"){ //remove the && false to view the registrastion form
                ?>
                    <div class="col-sm-10">
                        <div class="headTitleHolder">
                            <h1 class="">Registration Form</h1>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-10" id="Form">
                        <form action="" method="post">
                            <div class="col-md-6">
                                <input type="text" name="name" class="col-md-12 Input" placeholder="Name" value="{{Helpers::issetPost('name', '')}}" required >
                            </div>
                            <div class="col-md-6">
                                <script>
                            function isNumber(evt) {
                                evt = (evt) ? evt : window.event;
                                var charCode = (evt.which) ? evt.which : evt.keyCode;
                                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                                    return false;
                                }
                                return true;
                            }
                            </script>
                                <input type="text" name="phone_number" onkeypress="return isNumber(event)" class="col-md-12 Input" value="{{Helpers::issetPost('phone', '')}}" placeholder="Contact Number" required>
                            </div>
                            <div class="col-md-6">
                                <input type="email" name="email" class="col-md-12 Input" value="{{Helpers::issetPost('email', '')}}" placeholder="Email" required>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="Input" name="job_title" rows="12" placeholder="Job title" value="{{Helpers::issetPost('job_title', '')}}" required>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                            <div class="clearfix" style="margin-bottom: 15px;"></div>

                            <div class="col-sm-offset-4 col-sm-4">
                                <div class="btn-container">
                                    <input type="submit" name="send" id="RegistrationSubmit" class="btn">
                                </div>
                            </div>
                        </form>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>


@stop()