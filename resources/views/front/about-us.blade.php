@extends('front.layout')
@section('title', 'About us')
@section('content')

<img src="./front/files/Item_1471943346.jpg"><div class="clearfix"></div>
<div id="Breadcrumps">
    <div class="col-md-10 col-md-offset-1">
        <a href="<?= url('.');?>">HOME</a> >  <?= strtoupper('About Hyde Park');?>
    </div>
    <div class="clearfix"></div>
</div>

<div id="wrapper" class="aboutPage">
    <div class="container-fluid">
        <div class="row">
            <div class="contentWithBGInner clearfix">
                <div id="About" class="aboutSection col-xs-12 col-sm-10">
                    <div class="headTitleHolder">
                        <h1 class="headTitle headTitleWzstyle">About Hyde Park</h1>
                    </div>
                   <?= $about_hydepark->description;?>
                    <div class="headTitleHolder">
                        <h1 class="headTitle headTitleWzstyle">WHERE TO FIND US?</h1>
                    </div>
                  <iframe class="about-video" src="https://www.youtube.com/embed/g_fAKUDOXmE?rel=0" frameborder="0" allowfullscreen></iframe>
<!--                    <video style='width:100%;' controls>
                        <source src="<?= url('./uploads/HPD26.mp4');?>" type="video/mp4">
                        <source src="movie.ogg" type="video/ogg">
                    </video>-->
                </div>

                <div id="MessageCEO" class="aboutSection col-xs-12 col-sm-10">
                    <div class="headTitleHolder">
                        <h1 class="headTitle headTitleWzstyle">MESSAGE FROM THE CEO</h1>
                    </div>

                    <?= $message_from_ceo->description;?>
                    
                </div>

                <div id="OurPeople" class="aboutSection col-xs-12 col-sm-10">
                    <div class="headTitleHolder">
                        <h1 class="headTitle headTitleWzstyle">OUR PEOPLE</h1>
                    </div>
                    <?= $our_poeple->description;?>
                </div>

                <div id="OurValues" class="aboutSection col-xs-12 col-sm-10">
                    <div class="headTitleHolder">
                        <h1 class="headTitle headTitleWzstyle">OUR VALUES</h1>
                    </div>

                    <?= $our_values->description;?>
                </div>

                <div id="Shareholders" class="aboutSection col-xs-12 col-sm-10">
                    <div class="headTitleHolder">
                        <h1 class="headTitle headTitleWzstyle">SHAREHOLDERS</h1>
                    </div>

                    <?= $shareholders->description;?>
                </div>

                <div id="Partners" class="aboutSection col-xs-12 col-sm-10">
                    <div class="headTitleHolder">
                        <h1 class="headTitle headTitleWzstyle">PARTNERS</h1>
                    </div>

                    <?= $partners->description;?>
                </div>
            </div>
        </div>


    </div>

</div>


@stop()