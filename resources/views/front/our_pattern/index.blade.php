@extends('front.layout')
@section('title', 'Our Pattern')
@section('content')
<link type="text/css" rel="stylesheet" href="./front/About us _ Hyde Park_files/owl.carousel.css">
<script type="text/javascript" src="./front/About us _ Hyde Park_files/owl.carousel.js"></script>
<!-- Carousel -->
<script type="text/javascript" src="./front/About us _ Hyde Park_files/TweenMax.min.js"></script>
<script type="text/javascript" src="./front/About us _ Hyde Park_files/jquery.waterwheelCarousel.js"></script>
<script type="text/javascript" src="./front/About us _ Hyde Park_files/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript" src="./front/About us _ Hyde Park_files/custom.js"></script>
<link rel="stylesheet" href="./front/About us _ Hyde Park_files/lightbox.css">
<link type="text/css" rel="stylesheet" href="./front/About us _ Hyde Park_files/stylesheet.css">
<link rel="stylesheet" type="text/css" href="./front/About us _ Hyde Park_files/mainstyle-gallery.css">
<link rel="stylesheet" type="text/css" href="./front/About us _ Hyde Park_files/mainstyle.css">
<link rel="stylesheet" type="text/css" media="screen and (min-width: 992px) and (max-width: 1199px)" href="./front/About us _ Hyde Park_files/width992-1199.css">
<link rel="stylesheet" type="text/css" media="screen and (min-width: 768px) and (max-width:991px)" href="./front/About us _ Hyde Park_files/width768-991.css">
<link rel="stylesheet" type="text/css" media="screen and (max-width: 480px)" href="./front/About us _ Hyde Park_files/width480.css">
<script type="text/javascript" src="./About us _ Hyde Park_files/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="./About us _ Hyde Park_files/bootstrap.min.js"></script>
<div class="clearfix"></div>
<div id="Breadcrumps">
  <div class="col-md-10 col-md-offset-1">
    <a href="<?= url('.'); ?>">HOME</a> > <?= strtoupper('Our Pattern'); ?>
  </div>
  <div class="clearfix"></div>
</div>

<div id="wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="contentWithBGInner clearfix">
        <div id="About" class="aboutSection col-xs-12 col-sm-10">
          <div class="headTitleHolder">
            <h1 class="headTitle headTitleWzstyle">Our Pattern</h1>
          </div>

          <div class="galleryContent photos activeShow result_data result_scroll">

            @foreach($result as $row)
            <div class="col-md-12 col-xs-12">
                <div class="item-text" style="margin-bottom:25px;">
                    <p class="title pull-left"><span style="font-size: 20px; margin-bottom: 10px;"><?= $row->name; ?></span>
                        <br />
                        <br />
                        <span style="font-size: 15px; margin-bottom: 20px; color: #4E5356;"><?= $row->description; ?></span></p>
                <!--<p class="date pull-right">JAN 2016</p>-->
              </div><br /><br />
              <div class="item" style="margin-bottom: 25px;">
<!--                <div>-->
                  <!--<a class="example-image-link" href="<?= url('./uploads/' . $row->image); ?>" data-lightbox="example-set" data-title="<?= $row->name; ?>">-->
                    <img class="example-image" src="<?= url('./uploads/' . $row->image); ?>" style="margin-top: 30px;" alt="" /> 
                  <!--</a>-->
                <!--</div>-->
<!--                <div id="myModal" class="modal">

                   The Close Button 
                  <span class="close" onclick="hide(this)">&times;</span>

                   Modal Content (The Image) 
                  <img class="modal-content">

                </div>-->
              </div>
              
            </div>
            @endforeach

          </div>
          <div class="clear" style="margin-bottom: 25px;"></div>
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              @if(isset($settings['our_pattern_video_link']) && $settings['our_pattern_video_link'] !='')
              <iframe class="about-video" src="{{$settings['our_pattern_video_link']}}" frameborder="0" allowfullscreen></iframe>
              @endif
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
</div>
<script>

    $(".modal").on('click', function () {
      $('.modal').css('display', 'none');
    });

    function myFunction(x) {
      var modal = x.parentElement.nextElementSibling;
      var modalimg = modal.lastElementChild;
      modal.style.display = "block";
      modalimg.src = x.src;
    }

    function hide(z) {
      var hide = z.parentElement;
      hide.style.display = "none";
    }
    $(document).ready(function () {
      loadGallery(true, 'a.thumbnail');
      //This function disables buttons when needed
      function disableButtons(counter_max, counter_current) {
        $('#show-previous-image, #show-next-image').show();
        if (counter_max == counter_current) {
          $('#show-next-image').hide();
        } else if (counter_current == 1) {
          $('#show-previous-image').hide();
        }
      }
      /**
       *
       * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
       * @param setClickAttr  Sets the attribute for the click handler.
       */
      function loadGallery(setIDs, setClickAttr) {
        var current_image, selector, counter = 0;
        $('#show-next-image, #show-previous-image').click(function () {
          if ($(this).attr('id') == 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }
          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

        function updateGallery(selector) {
          var $sel = selector;
          current_image = $sel.data('image-id');
          $('#image-gallery-caption').text($sel.data('caption'));
          $('#image-gallery-title').text($sel.data('title'));
          $('#image-gallery-image').attr('src', $sel.data('image'));
          disableButtons(counter, $sel.data('image-id'));
        }
        if (setIDs == true) {
          $('[data-image-id]').each(function () {
            counter++;
            $(this).attr('data-image-id', counter);
          });
        }
        $(setClickAttr).on('click', function () {
          updateGallery($(this));
        });
      }
    });
</script>
@stop()