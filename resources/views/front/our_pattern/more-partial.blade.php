@foreach($result as $row)
<div class="col-md-4 col-xs-6">
  <div class="item">
    <div>
      <a class="example-image-link" href="<?= url('./uploads/' . $row->image); ?>" data-lightbox="example-set" data-title="<?= $row->name; ?>"><img class="example-image" src="<?= url('./uploads/' . $row->image); ?>" alt="" /> </a>
    </div>
    <div id="myModal" class="modal">

      <!-- The Close Button -->
      <span class="close" onclick="hide(this)">&times;</span>

      <!-- Modal Content (The Image) -->
      <img class="modal-content">

    </div>
  </div>
  <div class="item-text">
    <p class="title pull-left"><?= $row->name; ?></p>
    <!--<p class="date pull-right">JAN 2016</p>-->
  </div>
</div>
@endforeach