@extends('front.layout')
@section('title', 'Contact us')
@section('content')
@section('head-js')
<script src='https://www.google.com/recaptcha/api.js'></script>
@stop()

<div class="clearfix"></div>
<div id="Breadcrumps">
    <div class="col-md-10 col-md-offset-1">
        <a href="<?= url('.');?>">HOME</a> > <?= strtoupper('CONTACT US');?>
    </div>
    <div class="clearfix"></div>
</div>

<div id="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="contentWithBGInner clearfix">
            <?php
            use App\Http\Controllers\Common\Helpers;
            if(session('success')!='')
            $success = session('success') ;
            ?>
            <?php if(isset($validate_errors) or isset($success)){?>
            <?php if(isset($validate_errors)){?>

            <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php foreach($validate_errors as $error){?>
            <p><?php echo $error; ?></p>
            <?php }?>
            </div>
            <?php } ?>
            <?php if(!empty($success)){?>
            <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><?php echo $success; ?></p>
            </div>
            <?php } ?>

            <?php } ?>
                <div class="col-xs-12 col-sm-10">

                    <div class="">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="headTitleHolder">
                                        <h1 class="headTitle headTitleWzstyle">Contact us</h1>
                                    </div>
                                </div>
                            </div>


                            <div class="row contact-info-section">
                                <div class="col-md-3 col-md-offset-1">
                                    <div class="contact-info address">
                                        <h2>Address</h2>
                                        <h4>Head Office</h4>
                                        <p>Plot 67, Road 90 , <br> 5<sup>th</sup> Settlement New Cairo, Cairo, Egypt</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="contact-info address2">
                                        <h2>Address</h2>
                                        <h4>Sales Center</h4>
                                        <p>Plot 1, Road 90, <br> 5<sup>th</sup>Settlement, New Cairo, <br> Cairo, Egypt</p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="contact-info phone">
                                        <h2>Phones</h2>
                                        <h4>16696</h4>
                                    </div>
                                </div>

                            </div>
                            <div class="row contact-info-section">


                                <div class="col-md-3 col-md-offset-3">
                                      <div class="contact-info email">
                                        <h2>E-Mail</h2>
                                        <h4>info@hpd.com.eg</h4>
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    <div class="contact-info email">
                                        <h2>E-Mail</h2>
                                        <h4>Human Resources:<br>careers@hpd.com.eg</h4>
                                    </div>
                                </div>
                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <p style="margin-bottom: 40px;font-size: 16px;" class="text-center">For site visits, kindly contact our call center 16696 to schedule an appointment at your nearest convenience</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="headTitleHolder">
                            <h1 class="">Got a question? Get in touch</h1>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12" id="Form">
                    <form action="" method="post">
                        <div class="col-md-4">
                            <input type="text" name="name" class="col-md-12 Input" placeholder="Name" value="{{Helpers::issetPost('name', '')}}" required >
                        </div>
                        <div class="col-md-4">
                            <script>
                        function isNumber(evt) {
                            evt = (evt) ? evt : window.event;
                            var charCode = (evt.which) ? evt.which : evt.keyCode;
                            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                                return false;
                            }
                            return true;
                        }
                        </script>
                            <input type="text" name="phone" onkeypress="return isNumber(event)" class="col-md-12 Input" value="{{Helpers::issetPost('phone', '')}}" placeholder="Contact Number" required>
                        </div>
                        <div class="col-md-4">
                            <input type="email" name="email" class="col-md-12 Input" value="{{Helpers::issetPost('email', '')}}" placeholder="Email" required>
                        </div>
                        <div class="col-md-12">
                            <textarea class="Input" name="message" rows="12" placeholder="Subject" required>{{Helpers::issetPost('message', '')}}</textarea>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                        <div class="g-recaptcha" data-sitekey="6LfQGhEUAAAAAMSePHEZnRcgS5aMPmzxFN-4O2xH"></div>
                        </div>
                        <div class="clearfix" style="margin-bottom: 15px;"></div>

                        <div class="col-lg-12">
                            <div class="btn-container">
                                <input type="submit" name="send" id="ContactSubmit" class="btn">
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>

</div>
</div>



@stop()
