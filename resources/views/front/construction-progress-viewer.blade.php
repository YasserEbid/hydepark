@extends('front.layout')
@section('title', 'Construction Viewer')
@section('content')

<div class="clearfix"></div>
<div id="Breadcrumps">
    <div class="col-md-10 col-md-offset-1">
        <a href="<?= url('.'); ?>">HOME</a> > <?= strtoupper('CONSTRUCTION VIEWER'); ?>
    </div>
    <div class="clearfix"></div>
</div>

<div id="wrapper">
    <div class="container-fluid">
        <div class="row">
            
            <iframe src="http://pv.idea-sys.net/" frameborder="0" style="overflow:hidden;height:500px;width:100%" width="100%"></iframe>
            
        </div>

    </div>

</div>
<style>
footer{
	margin-top: -5px;
	border: 0 !important;
}
</style>

@stop()