@extends('front.layout')
@section('title', 'Presskit')
@section('content')

<div id="wrapper" class="aboutPage">
  <div class="container-fluid">
    <div class="row">
      <div class="contentWithBGInner clearfix">
        <div id="About" class="aboutSection col-xs-12 col-sm-10">
          <div class="headTitleHolder">
            <h1 class="headTitle headTitleWzstyle">Press Kit</h1>
          </div>

          <div class="galleryContent photos activeShow">

            @foreach($result as $row)
            <div class="BrochureItem col-md-6">
              <div class="image-holder">
                <img src="{{url('./uploads/'.$row->image)}}">
                <a href="{{url('uploads/' . $row->file)}}" target="_blank"></a>
              </div>
              <div class="item-text">
                <!--<p class="title pull-left">{{$row->title}}</p>-->
                <!--<p class="date pull-right">@if($row->date != '0000-00-00') {{date('M/Y', strtotime($row->date))}} @endif</p>-->

              </div>
            </div>
            @endforeach
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop()