@extends('front.layout')
@section('title', 'Press Release')
@section('content')
<div class="clearfix"></div>
<div id="Breadcrumps">
  <div class="col-md-10 col-md-offset-1">
    <a href="<?= url('.'); ?>">HOME</a> > <?= strtoupper('Press Release'); ?>
  </div>
  <div class="clearfix"></div>
</div>
<div id="wrapper" class="aboutPage">
  <div class="container-fluid">
    <div class="row">
      <div class="contentWithBGInner clearfix">
        <div id="About" class="aboutSection col-xs-12 col-sm-10">
          <div class="headTitleHolder">
            <h1 class="headTitle headTitleWzstyle">Press Release</h1>
          </div>

          <div class="filters clearfix">
            <div class="col-sm-6">
              <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">FILTER BY MONTH<span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?month=1'; ?>">Jan</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?month=2'; ?>">Feb</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?month=3'; ?>">Mar</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?month=4'; ?>">Apr</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?month=5'; ?>">May</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?month=6'; ?>">June</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?month=7'; ?>">July</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?month=8'; ?>">Aug</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?month=9'; ?>">Sep</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?month=10'; ?>">Oct</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?month=11'; ?>">Nov</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?month=12'; ?>">Dec</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">FILTER BY YEAR<span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                  @for($i = date('Y'); $i >= 2016; $i--)
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo \URL::Current() . '?year=' . $i; ?>"><?php echo $i; ?></a></li>
                  @endfor
                </ul>
              </div>
            </div>
          </div>


                @foreach($resultEN as $k=>$row)
                <div class="pressrelease press">
                    <div class="row">
                        <a href="{{url('uploads/'.$row->file)}}" target="_blank">
                            <img class="imgload3" src="{{url('./front/images/down.png')}}" />
                        </a>
                        <div class=" col-xs-2 col-md-1">
                          <div class="images-holder">
                            <img src="{{url('./front/images/logopark.jpg')}}">
                            <a href="{{url('uploads/'.$row->file)}}" target="_blank"></a>
                          </div>
                        </div>

                        <div class="col-xs-4 col-md-4">
                            <p>{{$row->title}}</p>
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <p class="text-center">@if($row->date != '0000-00-00') {{date('M/Y', strtotime($row->date))}} @endif</p>
                        </div>
                        <div class="col-xs-2 col-md-4">
                            <p class="text-right">{{$resultAR[$k]->title}}</p>
                        </div>

                        <div class="col-xs-2 col-md-1">
                          <div class="images-holder">
                            <img src="{{url('./front/images/logopark.jpg')}}">
                            <a href="{{url('uploads/'.$resultAR[$k]->file)}}" target="_blank"></a>
                          </div>
                          <a href="{{url('uploads/'.$resultAR[$k]->file)}}" target="_blank">
                              <img class="imgload2" src="{{url('./front/images/down.png')}}" />
                          </a>
                        </div>

                    </div>
                </div>
                @endforeach

            <?php /*
          <div class="row result_data result_scroll">
            <div class="col-md-6 col-xs-6 ">

              @foreach($resultEN as $row)
              <div class="pressrelease col-md-12">
                <a href="{{url('uploads/'.$row->file)}}" target="_blank">
                <img class="imgload" src="{{url('./front/images/down.png')}}" />
                </a>
                <div class=" col-md-2">
                  <div class="images-holder">
                    <img src="{{url('./front/images/logopark.jpg')}}">
                    <a href="{{url('uploads/'.$row->file)}}" target="_blank"></a>
                  </div>
                </div>
                <a href="{{url('uploads/'.$row->file)}}" target="_blank">
                  <div class="col-md-8 ">
                    <P>{{$row->title}}</P>
                  </div>
                  <div class="col-md-2">
                    <p>@if($row->date != '0000-00-00') {{date('M/Y', strtotime($row->date))}} @endif</p>
                  </div>
                </a>
              </div>
              @endforeach

            </div>

            <div class="col-md-6 col-xs-6 ">

              @foreach($resultAR as $row)
              <div class="pressrelease col-md-12">
                <a href="{{url('uploads/'.$row->file)}}" target="_blank">
                <img class="imgload2" src="{{url('./front/images/down.png')}}" />
                </a>

                <a href="{{url('uploads/'.$row->file)}}" target="_blank">
                  <div class="col-md-2">
                    <!--<p>@if($row->date != '0000-00-00') {{date('M/Y', strtotime($row->date))}} @endif</p>-->
                  </div>
                  <div class="col-md-8 ">
                      <p class="text-right">{{$row->title}}</p>
                  </div>

                </a>
                <div class=" col-md-2">
                  <div class="images-holder">
                    <img src="{{url('./front/images/logopark.jpg')}}">
                    <a href="{{url('uploads/'.$row->file)}}" target="_blank"></a>
                  </div>
                </div>
              </div>
              @endforeach

            </div>
            */ ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    var page = 2;
    var request_page = false;

    // get more result on scroll down
    $(window).on('scroll', function () {
      var page_url = '<?= url('./press/more/?month=' . urlencode(@$_GET["month"]) . '&year=' . @$_GET["year"]); ?>&page=' + page;
      if (request_page == false && $(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.7) {
        request_page = true;
        if (page_url === undefined) {
          return false;
        }
        //  alert("page "+page_url);
        $.ajax({
          url: page_url,
          method: 'GET',
          beforeSend: function () {
            $('.loading').show();
          },
          success: function (response) {
            page++;
            if (response !== '') {
              $('.result_data').append(response);
              $(window).resize();
              request_page = false;
            } else
            {
              $('.loading').hide();
            }
          },
          complete: function () {
            $('.loading').hide();
          }
        });
      }
    });
</script>

@stop()
