@foreach($resultEN as $row)
<div class="pressrelease col-md-12">
  <a href="{{url('uploads/'.$row->file)}}" target="_blank">
    <img class="imgload" src="{{url('./front/images/down.png')}}" />
  </a>
  <div class=" col-md-2">
    <div class="images-holder">
      <img src="{{url('./front/images/logopark.jpg')}}">
      <a href="{{url('uploads/'.$row->file)}}" target="_blank"></a>
    </div>
  </div>
  <a href="{{url('uploads/'.$row->file)}}" target="_blank">
    <div class="col-md-8 ">
      <P>{{$row->title}}</P>
    </div>
    <div class="col-md-2">
      <p>@if($row->date != '0000-00-00') {{date('M/Y', strtotime($row->date))}} @endif</p>
    </div>
  </a>
</div>
@endforeach
@foreach($resultAR as $row)
<div class="pressrelease col-md-12">
  <a href="{{url('uploads/'.$row->file)}}" target="_blank">
    <img class="imgload" src="{{url('./front/images/down.png')}}" />
  </a>
  <div class=" col-md-2">
    <div class="images-holder">
      <img src="{{url('./front/images/logopark.jpg')}}">
      <a href="{{url('uploads/'.$row->file)}}" target="_blank"></a>
    </div>
  </div>
  <a href="{{url('uploads/'.$row->file)}}" target="_blank">
    <div class="col-md-8 ">
        <p class="text-right">{{$row->title}}</p>
    </div>
    <div class="col-md-2">
      <p>@if($row->date != '0000-00-00') {{date('M/Y', strtotime($row->date))}} @endif</p>
    </div>
  </a>
</div>
@endforeach