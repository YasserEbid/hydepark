<!DOCTYPE html>
<html>
  <head>
    <base href='<?= URL::to('./'); ?>' />
    <script>var base = '<?= URL::to('./'); ?>';</script>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="fav.png"/>
    <meta name="author" content="Development: Tarek Magdy"/>
    <meta name="keywords" content="<?= $settings['meta_keywords']; ?>"/>
    <meta name="description" content="<?= $settings['meta_description']; ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title>@yield('title') | Hyde Park</title>
    <link type="text/css" rel="stylesheet" href="./front/node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <!--<link type="text/css" rel="stylesheet" href="./front/css/bootstrap-rtl.min.css" />-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="./front/js/html5shiv.min.js"></script>
    <script type="text/javascript" src="./front/js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="./front/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="./front/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!--//bootstrap3-->
    <link type="text/css" rel="stylesheet" href="./front/js/owl-carousel/owl.carousel.css" />
    <script type="text/javascript" src="./front/js/owl-carousel/owl.carousel.js"></script>
    <!-- Carousel -->
    <script type="text/javascript" src="./front/js/HomeSlider2/js/TweenMax.min.js"></script>
    <script type="text/javascript" src="./front/js/HomeSlider2/js/jquery.waterwheelCarousel.js"></script>

    <script type="text/javascript" src="./front/js/custom.js"></script>

    <link type="text/css" rel="stylesheet" href="./front/fonts/akkurat/stylesheet.css" />
    <link rel="stylesheet" type="text/css" href="./front/css/mainstyle.css">
    <link rel="stylesheet" type="text/css" media="screen and (min-width: 992px) and (max-width: 1199px)" href="./front/css/width992-1199.css" />
    <link rel="stylesheet" type="text/css" media="screen and (min-width: 768px) and (max-width:991px)" href="./front/css/width768-991.css" />
    <link rel="stylesheet" type="text/css" media="screen and (max-width: 480px)" href="./front/css/width480.css" />
    @yield('head-js')
  </head>    
  <body>


    <header id="header">
      <div class="container">

        <div class="logoHolder">
          <a href="<?= url('.'); ?>"><img src="./front/images/logo.png" /></a>
        </div>
        <div class="topLinks">
          <ul>
            <li class="search">
              <a href="#" title="search">search</a>
            </li>
            <li class="instagram">
              <a href="<?= $settings['instagram_link']; ?>" target="_blank"></a>
            </li>
            <li class="facebook">
              <a href="<?= $settings['facebook_link']; ?>" target="_blank"></a>
            </li>
            <li class="youtube">
              <a href="<?= $settings['youtube_link']; ?>" target="_blank"></a>
            </li>
            <li class="phone">
              <a>16696</a>
            </li>
          </ul>
        </div>
        <nav>
          <div>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="main-menu collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="clearfix">
                <li <?php
                if(Request::segment(1) == "" && Request::segment(2) == "")
                {
                    ?> class="active" <?php } ?>>
                  <a href="<?= url('.'); ?>">HOME </a>
                </li>

                <li <?php
                  if(Request::segment(1) == "about-us")
                  {
                      ?> class="active" <?php } ?>>
                  <a href="<?= url('./about-us'); ?>">ABOUT</a>
                  <ul>
                    <li>
                      <a href="<?= url('./about-us#About'); ?>" >About Hyde Park</a>
                    </li>
                    <li>
                      <a href="<?= url('./about-us#MessageCEO'); ?>" >Message from the CEO</a>
                    </li>
                    <li>
                      <a href="<?= url('./about-us#OurPeople'); ?>" >Our People</a>
                    </li>
                    <li>
                      <a href="<?= url('./about-us#OurValues'); ?>" >Our Values</a>
                    </li>
                    <li>
                      <a href="<?= url('./about-us#Shareholders'); ?>" >Shareholders</a>
                    </li>
                    <li>
                      <a href="<?= url('./about-us#Partners'); ?>" >Partners </a>
                    </li>
                    <li>
                      <a href="<?= url('./about-us/our-pattern'); ?>" >Our Pattern </a>
                    </li>
                  </ul>
                </li>

                <li <?php
                  if(Request::segment(1) == "our-development")
                  {
                      ?> class="active" <?php } ?>>
                  <a>OUR DEVELOPMENT</a>
                  <ul>
                    <li <?php
                  if(Request::segment(1) == "our-development" && Request::segment(2) == "masterplan")
                  {
                      ?> class="active" <?php } ?>>
                      <a href="<?= url('./our-development/masterplan'); ?>" >Masterplan</a>

                    </li>
                    <li class="dropdown dropdown-submenu">
                      <!--<a href="#" >Hyde Park </a>-->
                      <ul>
                        <li>
                          <a href="<?= url('./our-development/the-park'); ?>" >The Park</a>
                        </li>
                        <li>
                          <a href="<?= url('./our-development/office-park'); ?>" >Office Park</a>
                        </li>
                        <li>
                          <a href="<?= url('./our-development/retail-complex'); ?>" >Retail Complex</a>
                        </li>
                        <li>
                          <a href="<?= url('./our-development/facilities'); ?>"  >Facilities</a>
                        </li>
                        <li class="dropdown dropdown-submenu">
                          <a href="<?= url('./our-development/residential'); ?>"  >RESIDENTIAL</a>

                          <ul>
                            <li>
                              <a href="<?= url('./our-development/apartments'); ?>" >Apartments</a>
                            </li>
                            <li>
                              <a href="<?= url('./our-development/villas'); ?>" >Villas </a>
                            </li>
                          </ul>

                        </li>
                      </ul>

                    </li>

                  </ul>

                </li>

                <li <?php
                  if(Request::segment(1) == "construction-viewer")
                  {
                      ?> class="active" <?php } ?>>
                  <a href="<?= url('construction-viewer'); ?>">CONSTRUCTION VIEWER</a>

                </li>

                <li <?php
                if(Request::segment(1) == "media-center" || Request::segment(1) == "newsletter")
                {
                    ?> class="active" <?php } ?>>
                  <a>MEDIA CENTRE</a>

                  <ul>
                    <li class="dropdown dropdown-submenu">
                          <a href="<?= url('./gallery'); ?>"  >GALLERY</a>
                          <ul>
                            <li>
                              <a href="<?= url('./gallery#photos'); ?>" >PHOTOS</a>
                            </li>
                            <li>
                              <a href="<?= url('./gallery#videos'); ?>" >VIDEOS</a>
                            </li>
                          </ul>
                        </li>
                    <li>
                      <a href="<?= url('media'); ?>" >IN MEDIA</a>
                    </li>
                    <li>
                      <a href="<?= url('press'); ?>" >PRESS RELEASE</a>
                    </li>
                    <li>
                      <a href="<?= url('presskit'); ?>" >PRESS KIT</a>
                    </li>
                    <li>
                      <a href="<?= url('newsletter'); ?>" >NEWSLETTER</a>
                    </li>
                  </ul>

                </li>

                <li <?php
                if(Request::segment(1) == "contact-us")
                {
                    ?> class="active" <?php } ?>>
                  <a href="<?= url('contact-us'); ?>">CONTACT US </a>

                </li>

              </ul>

            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

        <div class="searchHolder">
          <div class="search-conatiner">
            <form action="<?= url('./search'); ?>" method="get">
              <input type="button" class="search-btn" />
              <input type="text" name="find" class="search-input" placeholder="search" required />
              <span class="search-close"> </span>
            </form>
          </div>
        </div>
      </div>
    </header>


    <?php if(!isset($_SESSION['image_pop'])){?>
    <!-- Trigger the modal with a button -->
    <button style="display: none;" type="button" class="btn btn-danger btn-lg pulsing" data-toggle="modal" data-target="#myPopup">Important Notice<br>بيان هام</button>
    <!-- Modal -->
    <!--<div id="myPopup" class="modal fade" role="dialog" style="z-index: 9999999;">
      <div class="modal-dialog modal-lg">
         
        <div class="modal-content">
            <div class="modal-body" style="padding:5px;">
              <button type="button" class="close pull-right" style="margin-right:-40px;" data-dismiss="modal">&times;</button>
            <a href="http://eps.hpd.com.eg/?source=Pop-up" target="_blank"><img src="{{url('./uploads/EPS.jpg')}}" /></a>
          </div>
        </div>
      </div>
    </div>-->
  <!--  <style>
      .pulsing {  
        background:red;
        height: 60px;
        width: 160px;
        border-radius: 5px;
        position: fixed;
        z-index:9999;
        right:20px;
        top:110px;
        -webkit-animation: pulsate 2s ease-out;
        -webkit-animation-iteration-count: infinite; 
        opacity: 0.5
      }
      @-webkit-keyframes pulsate {
        0% { opacity: 0.5;}
        50% {opacity: 1.0;}
        100% { opacity: 0.5;}
      }
    </style>-->
    <!--<script>
        $(document).ready(function () {
    $('.pulsing').trigger('click');
    });
    </script>-->
<?php $_SESSION['image_pop'] = true;}?>
    
    @yield('content')

    <footer id="footer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-2">
            <div class="logoHolder">
              <a href="<?= url('.'); ?>"><img src="./front/images/_logo.png" /></a>
            </div>
          </div>
          <div class="col-sm-8">
            <div class="row">

              <div class="col-sm-3">
                <h6 class="headTitle"><a href="<?= url('./about-us'); ?>">ABOUT US</a></h6>
                <ul class="ulList">
                  <li>
                    <a href="<?= url('./about-us#About'); ?>">About Hyde Park</a>
                  </li>
                  <li>
                    <a href="<?= url('./about-us#MessageCEO'); ?>">Message from the CEO</a>
                  </li>
                  <li>
                    <a href="<?= url('./about-us#OurPeople'); ?>">Our People</a>
                  </li>
                  <li>
                    <a href="<?= url('./about-us#OurValues'); ?>" >Our Values</a>
                  </li>
                  <li>
                    <a href="<?= url('./about-us#Shareholders'); ?>" >Shareholders</a>
                  </li>
                  <li>
                    <a href="<?= url('./about-us#Partners'); ?>" >Partners </a>
                  </li>
                </ul>
              </div>

              <div class="col-sm-3">
                <h6 class="headTitle"><a>OUR DEVELOPMENT</a></h6>
                <ul class="ulList">
                  <li>
                    <a href="<?= url('./our-development/masterplan'); ?>">Master Plan</a>
                  </li>
                  <li>
                    <a href="<?= url('./our-development/residential'); ?>">Residential</a>
                  </li>
                </ul>
              </div>
              <div class="col-sm-3">
                <h6 class="headTitle"><a href="<?= url('construction-viewer'); ?>">CONSTRUCTION VIEWER</a></h6>

              </div>

              <div class="col-sm-3">
                <h6 class="headTitle"><a>MEDIA CENTRE</a></h6>
                <ul class="ulList">
                  <li>
                    <a href="<?= url('gallery'); ?>" >Gallery</a>
                  </li>
                  <li>
                    <a href="<?= url('media'); ?>" >In Media</a>
                  </li>
                  <li>
                    <a href="<?= url('press'); ?>" >Press Release</a>
                  </li>
                  <li>
                    <a href="<?= url('presskit'); ?>" >Press Kit</a>
                  </li>
                  <li>
                    <a href="<?= url('newsletter'); ?>" >Newsletter</a>
                  </li>
                </ul>
                <h6 class="headTitle"><a href="<?= url('contact-us'); ?>">CONTACT US</a></h6>
              </div>

            </div>
          </div>

          <div class="col-sm-2">
            <div class="topLinks">
              <ul>
                <li class="instagram">
                  <a href="<?= $settings['instagram_link']; ?>" target="_blank"></a>
                </li>
                <li class="facebook">
                  <a href="<?= $settings['facebook_link']; ?>" target="_blank"></a>
                </li>
                <li class="youtube">
                  <a href="<?= $settings['youtube_link']; ?>" target="_blank"></a>
                </li>
                <li class="phone">
                  <a>16696</a>
                </li>
              </ul>
            </div>
            <div class="text-right">Copyrights Hyde Park 2019<br/>All Rights Reserved.<br /> <br />Development by <a href="http://www.media-sci.com" target="_blank">Media Sci</a></div>
          </div>
        </div>
      </div>
    </footer>


    <script>
        (function (i, s, o, g, r, a, m) {
          i['GoogleAnalyticsObject'] = r;
          i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
          }, i[r].l = 1 * new Date();
          a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0];
          a.async = 1;
          a.src = g;
          m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-85968168-1', 'auto');
        ga('send', 'pageview');

    </script>

  </body>

  <!-- Mirrored from egyweb.org/HYDEPARK/HPD/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Sep 2016 14:03:24 GMT -->
</html>