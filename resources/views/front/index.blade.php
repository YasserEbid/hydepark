@extends('front.layout')
@section('title', 'Home page')
@section('content')

<div id="banner">
    <div id="owl-demo" class="owl-bannerCarousel">
        
        <?php foreach($sliders as $slider){?>

        <div class="item"><img src="<?= url('uploads/'.$slider->image);?>" />
            <div class="caption">
                <div class="col-sm-6 col-sm-offset-3">
                    <h1></h1><p></p>
                </div>
            </div>
        </div>
        <?php }?>
        
    </div>
    <script>
        $(document).ready(function () {
            $("#owl-demo").owlCarousel({
                items: 1,
                navigation: false,
                singleItem: true,
                autoPlay: true,
                pagination: true
            });
        });
    </script>
</div>

<div id="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="contentWithBG homePage clearfix">
                <div class="col-xs-12 col-sm-10">
                    <div class="headTitleHolder">
                        <h1 class="headTitle headTitleWzstyle"><?= $sectionOne->title;?></h1>
                    </div>
                    <h2 class="subTitle"><?= $sectionOne->sub_title;?></h2>

                    <?= $sectionOne->description;?>
<?php if($sectionOne->link !=''){?>
                    <div class="learn-more">
                        <a href="<?= $sectionOne->link;?>">Learn More</a>
                    </div>
<?php }?>
                </div>
            </div>
        </div>
    </div>
    <div class="projectsSec">
        <div class="clearfix">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="headTitleHolder">
                            <h1 class="headTitle headTitleWzstyle"><?= $sectionTwo->title;?></h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <?= $sectionTwo->description;?>
                    </div>
                </div>
                <!-- Carousel -->

                <section class="center slider">
                    <div id="carousel">
                        <?php foreach($banners as $banner){?>
                        <div>
                            <img src="<?= url('uploads/'.$banner->image);?>" alt="<?= $banner->name;?>"  id="item-1">
                            <h2><?= $banner->name;?></h2>
                        </div>
                        <?php }?>
                    </div>
                    <a href="#" id="prev"></a> <a href="#" id="next"></a>

                </section>

                <script type="text/javascript">
                    $(document).ready(function () {
                        var carousel = $("#carousel").waterwheelCarousel({
                            flankingItems: 5,
                            separation: 400,
                            opacityMultiplier: .7,
                            autoPlay: 5000,
                            separationMultiplier: .8,
                            movedToCenter: function ($newCenterItem) {
                                $('#carousel_description').html($newCenterItem.attr('title'));
                            },
                            movingToCenter: function ($item) {
                                $('#callback-output').prepend('movingToCenter: ' + $item.attr('id') + '<br/>');
                            },
                            movedToCenter : function ($item) {
                                $('#callback-output').prepend('movedToCenter: ' + $item.attr('id') + '<br/>');
                            },
                                    movingFromCenter: function ($item) {
                                        $('#callback-output').prepend('movingFromCenter: ' + $item.attr('id') + '<br/>');
                                    },
                            movedFromCenter: function ($item) {
                                $('#callback-output').prepend('movedFromCenter: ' + $item.attr('id') + '<br/>');
                            },
                            clickedCenter: function ($item) {
                                $('#callback-output').prepend('clickedCenter: ' + $item.attr('id') + '<br/>');
                            }
                        });

                        $('#prev').bind('click', function () {
                            carousel.prev();
                            return false
                        });

                        $('#next').bind('click', function () {
                            carousel.next();
                            return false;
                        });

                        $('#reload').bind('click', function () {
                            newOptions = eval("(" + $('#newoptions').val() + ")");
                            carousel.reload(newOptions);
                            return false;
                        });

                    });
                </script>

            </div>
        </div>
    </div>
</div>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1704028079814855'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=1704028079814855&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
@stop()