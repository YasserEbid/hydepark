@extends('front.layout')
@section('title', 'Gallery')
@section('content')
<!--<link type="text/css" rel="stylesheet" href="./front/About us _ Hyde Park_files/bootstrap.min.css">-->
<!--<link type="text/css" rel="stylesheet" href="./front/css/bootstrap-rtl.min.css" />-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script type="text/javascript" src="./front/js/html5shiv.min.js"></script>
    <script type="text/javascript" src="./front/js/respond.min.js"></script>
    <![endif]-->
<link type="text/css" rel="stylesheet" href="./front/About us _ Hyde Park_files/owl.carousel.css">
<script type="text/javascript" src="./front/About us _ Hyde Park_files/owl.carousel.js"></script>
<!-- Carousel -->
<script type="text/javascript" src="./front/About us _ Hyde Park_files/TweenMax.min.js"></script>
<script type="text/javascript" src="./front/About us _ Hyde Park_files/jquery.waterwheelCarousel.js"></script>
<script type="text/javascript" src="./front/About us _ Hyde Park_files/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript" src="./front/About us _ Hyde Park_files/custom.js"></script>
<link rel="stylesheet" href="./front/About us _ Hyde Park_files/lightbox.css">
<link type="text/css" rel="stylesheet" href="./front/About us _ Hyde Park_files/stylesheet.css">
<link rel="stylesheet" type="text/css" href="./front/About us _ Hyde Park_files/mainstyle-gallery.css">
<link rel="stylesheet" type="text/css" href="./front/About us _ Hyde Park_files/mainstyle.css">
<link rel="stylesheet" type="text/css" media="screen and (min-width: 992px) and (max-width: 1199px)" href="./front/About us _ Hyde Park_files/width992-1199.css">
<link rel="stylesheet" type="text/css" media="screen and (min-width: 768px) and (max-width:991px)" href="./front/About us _ Hyde Park_files/width768-991.css">
<link rel="stylesheet" type="text/css" media="screen and (max-width: 480px)" href="./front/About us _ Hyde Park_files/width480.css">
<script type="text/javascript" src="./About us _ Hyde Park_files/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="./About us _ Hyde Park_files/bootstrap.min.js"></script>

<style>
    .contentWithBGInner .photos div.row a img {
        width: auto;
        transform: none;
        position: static;
    }
</style>
<div class="clearfix"></div>
<div id="Breadcrumps">
    <div class="col-md-10 col-md-offset-1">
        <a href="<?= url('.'); ?>">HOME</a> > <?= strtoupper('EVENTS'); ?>
    </div>
    <div class="clearfix"></div>
</div>

<div id="wrapper" class="aboutPage">
    <div class="container-fluid">
        <div class="row">
            <div class="contentWithBGInner clearfix">
                <div id="About" class="aboutSection col-xs-12 col-sm-10">
                    <div class="headTitleHolder">
                        <h1 class="headTitle headTitleWzstyle">Hyde Park Gallery</h1> </div>
                    <div class="switch">
                        <h3 class="photosSection active">PHOTOS</h3>
                        <div class="divider"></div>
                        <h3 class="videoSection">VIDEOS</h3>
                    </div>

                    <div class="switch sort">
                        <h3 class="active all tabs" ><a data-target="" href="<?= url(\URL::current()); ?>">ALL</a></h3>
                        <div class="divider"></div>
                        <?php foreach($sectionsGallery as $key=>$section){ ?>
                            <h3 class="tabs"><a data-id="<?= $section->id;?>" data-target="#<?= str_replace(' ','',$section->name); ?>" href="<?= url(\URL::current().'#'.$section->name); ?>" ><?= strtoupper($section->name); ?></a></h3>
                            <?php if($key != count($sectionsGallery)-1) { ?>
                                <div class="divider"></div>
                            <?php } ?>
                        <?php } ?>
                    </div>

                    <div class="filters clearfix">
                        <div class="filter-partial">
                            
                        </div>
                        
                        @include('front.gallery.filter-partial')
                        
                        <div class="col-sm-4">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">FILTER BY YEAR<span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                    <?php
                                    for($i = date('Y'); $i >= 2010; $i--)
                                    {
                                        ?>
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo \URL::Current() . '?year=' . $i; ?>"><?php echo $i; ?></a></li>
<?php } ?>
                                </ul>
                            </div>
                        </div>
                        
                    </div>

                    <div class="galleryContent videos videos_data videos_scroll">

                        <?php
                        foreach($videos as $video)
                        {
                            ?>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="item">
                                    <div>
                                        <?php
                                        if($video->type == 'youtube')
                                        {
                                            ?>
                                            <iframe src="https://www.youtube.com/embed/<?= $video->video; ?>" frameborder="0" allowfullscreen=""></iframe>
                                        <?php } ?>
                                        <?php
                                        if($video->type == 'facebook')
                                        {
                                            ?>

    <?php } ?>
                                    </div>
                                </div>
                                <div class="item-text">
                                  <!--<p class="title pull-left"><?= $video->name; ?></p>-->
                                    <p class="date pull-right"><?php if($video->date != '0000-00-00') echo date('M/Y', strtotime($video->date)); ?></p>
                                </div>
                            </div>
<?php } ?>

                    </div>

                    <div class="gallary-content">

                        <?php foreach($sectionsGallery as $section){ ?>
                        <div id="<?= str_replace(' ','',$section->name); ?>">
                            <div class="photos activeShow events_data events_scroll">

                                <?php
                                foreach($section->events as $event)
                                {
                                    ?>
                                    <h3 style="margin: 0 0 10px 22px;"><?= $event->name; ?> <span class="date pull-right" style="margin: 15px 20px 0 0;"><?php if($event->date != '0000-00-00') echo date('M/Y', strtotime($event->date)); ?></span></h3>
                                    <div class="row">
                                        <?php
                                    foreach($event->photos as $photos)
                                    {
                                        ?>
                                        <div class="col-md-4 col-xs-6">
                                            <div class="item">
                                                <div>
                                                    <a class="example-image-link" href="<?= url('./uploads/events/' . $photos->image); ?>" data-lightbox="example-set" data-title="<?= $photos->name; ?>"><img class="example-image" src="<?= url('./uploads/events/' . $photos->image); ?>" alt="" /> </a>
                                                </div>
                                                <div id="myModal" class="modal">

                                                    <!-- The Close Button -->
                                                    <span class="close" onclick="hide(this)">&times;</span>

                                                    <!-- Modal Content (The Image) -->
                                                    <img class="modal-content">

                                                </div>
                                            </div>
                                            <div class="item-text">
                                                <p class="title pull-left"><?= $photos->name; ?></p>
                                                <!--<p class="date pull-right">JAN 2016</p>-->
                                            </div>
                                        </div>
                                        <?php
                                    } ?>
                                    </div>
                                <?php }
                                ?>

                            </div>
                        </div>
                        <?php } ?>

                    </div>

                    <br />
                    <div class="loading">Loading</div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
//    (function (i, s, o, g, r, a, m) {
//      i['GoogleAnalyticsObject'] = r;
//      i[r] = i[r] || function () {
//        (i[r].q = i[r].q || []).push(arguments)
//      }, i[r].l = 1 * new Date();
//      a = s.createElement(o), m = s.getElementsByTagName(o)[0];
//      a.async = 1;
//      a.src = g;
//      m.parentNode.insertBefore(a, m)
//    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
//    ga('create', 'UA-85968168-1', 'auto');
//    ga('send', 'pageview');
    //        hide and show video & photo
    $(document).ready(function () {
      $('.videoSection').on('click', function () {
        $(".photos").removeClass("activeShow");
        $(".photosSection").removeClass("active");
        $(".videos").addClass("activeShow");
        $(".videoSection").addClass("active");
        $(".filters .col-sm-4:eq(0)").addClass("deactive");
        $(".filters .col-sm-4:eq(1)").addClass("col-sm-offset-4");
        $(".switch:eq(1)").addClass("deactive");
        $(".gallary-content > div").addClass("deactive");
      });

      $('.photosSection').on('click', function () {
        $(".videos").removeClass("activeShow");
        $(".videoSection").removeClass("active");
        $(".photos").addClass("activeShow");
        $(".photosSection").addClass("active");
        $(".filters .col-sm-4:eq(0)").removeClass("deactive");
        $(".filters .col-sm-4:eq(1)").removeClass("col-sm-offset-4");
        $(".switch:eq(1)").removeClass("deactive");
        $(".gallary-content > div").removeClass("deactive");
      });

      $(".sort h3 a").click(function(e){
        e.preventDefault();
        $(".sort h3").removeClass("active");
        $(this).parent().addClass("active");
        $(".gallary-content > div").addClass("deactive");
        $(".gallary-content > div").removeClass("gallary-active");
        $($(this).data("target")).addClass("gallary-active");
        
        if($(this).parent().hasClass("all")){
            $(".gallary-content > div").removeClass("gallary-active");
            $(".gallary-content > div").removeClass("deactive");
        }
        
        if($(this).attr('data-target') == "")
            location.hash = $(this).attr('data-target');
        $('html, body').scrollTop($(".sort h3").offset().top-100);
      });
      
      if(location.hash != "") {
        if(location.hash == "#photos")
            $('.photosSection').trigger('click');
        if(location.hash == "#videos")
            $('.videoSection').trigger('click');
      }
      
      $('#header a').click(function(e) {
            //e.preventDefault();
            setTimeout(function() {
                if(location.hash != "") {
                    if(location.hash == "#photos")
                        $('.photosSection').trigger('click');
                    if(location.hash == "#videos")
                        $('.videoSection').trigger('click');
                  //$('.sort h3 a[data-target='+location.hash+']').trigger('click');
                }
            }, 10);
      });
    });

    $(".modal").on('click', function () {
      $('.modal').css('display', 'none');
    });

    function myFunction(x) {
      var modal = x.parentElement.nextElementSibling;
      var modalimg = modal.lastElementChild;
      modal.style.display = "block";
      modalimg.src = x.src;
    }

    function hide(z) {
      var hide = z.parentElement;
      hide.style.display = "none";
    }
    $(document).ready(function () {
      loadGallery(true, 'a.thumbnail');
      //This function disables buttons when needed
      function disableButtons(counter_max, counter_current) {
        $('#show-previous-image, #show-next-image').show();
        if (counter_max == counter_current) {
          $('#show-next-image').hide();
        } else if (counter_current == 1) {
          $('#show-previous-image').hide();
        }
      }
      /**
       *
       * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
       * @param setClickAttr  Sets the attribute for the click handler.
       */
      function loadGallery(setIDs, setClickAttr) {
        var current_image, selector, counter = 0;
        $('#show-next-image, #show-previous-image').click(function () {
          if ($(this).attr('id') == 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }
          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

        function updateGallery(selector) {
          var $sel = selector;
          current_image = $sel.data('image-id');
          $('#image-gallery-caption').text($sel.data('caption'));
          $('#image-gallery-title').text($sel.data('title'));
          $('#image-gallery-image').attr('src', $sel.data('image'));
          disableButtons(counter, $sel.data('image-id'));
        }
        if (setIDs == true) {
          $('[data-image-id]').each(function () {
            counter++;
            $(this).attr('data-image-id', counter);
          });
        }
        $(setClickAttr).on('click', function () {
          updateGallery($(this));
        });
      }
    });
</script>

<script>
    var page = 2;
    var videos_page = 2;
    var request_page = false;
    var request_video_page = false;

    // get more events on scroll down
    $(window).on('scroll', function () {
        var page_event_url = '<?= url('./gallery/events/?month=' . urlencode(@$_GET["month"]) . '&year=' . @$_GET["year"]); ?>&page=' + page;
        var page_videos_url = '<?= url('./gallery/videos/?month=' . urlencode(@$_GET["month"]) . '&year=' . @$_GET["year"]); ?>&page=' + videos_page;
        if (request_page == false && $('.events_scroll').innerHeight() > 30 && $(this).scrollTop() >= $('.events_scroll').innerHeight() + 500) {
            request_page = true;
            if (page_event_url === undefined) {
                return false;
            }
            //  alert("page "+page_url);
            $.ajax({
                url: page_event_url,
                method: 'GET',
                beforeSend: function () {
                    $('.loading').show();
                },
                success: function (response) {
                    page++;
                    if (response !== '') {
                        $('.events_data').append(response);
                        $(window).resize();
                        request_page = false;
                    } else
                    {
                        $('.loading').hide();
                    }
                },
                complete: function () {
                    $('.loading').hide();
                }
            });
        }

        if (request_video_page == false && $(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.7) {
            request_video_page = true;
            if (page_videos_url === undefined) {
                return false;
            }
            $.ajax({
                url: page_videos_url,
                method: 'GET',
                beforeSend: function () {
                    $('.loading').show();
                },
                success: function (response) {
                    console.log(response);
                    videos_page++;
                    if (response !== '') {
                        $('.videos_data').append(response);
                        $(window).resize();
                        request_video_page = false;
                    } else
                    {
                        $('.loading').hide();
                    }
                },
                complete: function () {
                    $('.loading').hide();
                }
            });
        }
    });
</script>

@stop()
