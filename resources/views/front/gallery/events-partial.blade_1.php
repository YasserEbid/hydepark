<?php foreach ($events as $event)
{ ?>
    <h3 style="margin: 0 0 10px 22px;"><?= $event->name; ?> <span class="date pull-right" style="margin: 15px 20px 0 0;"><?php if($event->date != '0000-00-00') echo date('M/Y', strtotime($event->date)); ?></span></h3>
    <?php foreach ($event->photos as $photos)
    { ?>
        <div class="col-md-4 col-xs-6">
          <div class="item">
            <div>
              <a class="example-image-link" href="<?= url('./uploads/events/' . $photos->image); ?>" data-lightbox="example-set" data-title="<?= $photos->name; ?>"><img class="example-image" src="<?= url('./uploads/events/' . $photos->image); ?>" alt="" /> </a>
            </div>
            <div id="myModal" class="modal">

              <!-- The Close Button -->
              <span class="close" onclick="hide(this)">&times;</span>

              <!-- Modal Content (The Image) -->
              <img class="modal-content">

            </div>
          </div>
          <div class="item-text">
            <p class="title pull-left"><?= $photos->name; ?></p>
            <!--<p class="date pull-right">JAN 2016</p>-->
          </div>
        </div>
    <?php }
} ?> 