<?php if($is_month == false)
{ ?>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">FILTER BY NAME<span class="caret"></span></button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
            <?php foreach($events as $event)
            { ?>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="<?= '?name='.$event->name; ?>" data-section="<?= $event->section_id;?>" class="filter-name"><?= $event->name; ?></a></li>
    <?php } ?>
        </ul>
    </div>
<?php }
else
{ ?>
    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">FILTER BY MONTH<span class="caret"></span></button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="?month=1" data-section="<?= $events->first()->section->id;?>" class="filter-name">Jan</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="?month=2" data-section="<?= $events->first()->section->id;?>" class="filter-name">Feb</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="?month=3" data-section="<?= $events->first()->section->id;?>" class="filter-name">Mar</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="?month=4" data-section="<?= $events->first()->section->id;?>" class="filter-name">Apr</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="?month=5" data-section="<?= $events->first()->section->id;?>" class="filter-name">May</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="?month=6" data-section="<?= $events->first()->section->id;?>" class="filter-name">June</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="?month=7" data-section="<?= $events->first()->section->id;?>" class="filter-name">July</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="?month=8" data-section="<?= $events->first()->section->id;?>" class="filter-name">Aug</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="?month=9" data-section="<?= $events->first()->section->id;?>" class="filter-name">Sep</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="?month=10" data-section="<?= $events->first()->section->id;?>" class="filter-name">Oct</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="?month=11" data-section="<?= $events->first()->section->id;?>" class="filter-name">Nov</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-name="?month=12" data-section="<?= $events->first()->section->id;?>" class="filter-name">Dec</a></li>
        </ul>
    </div>
<?php } ?>
