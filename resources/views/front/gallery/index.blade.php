@extends('front.layout')
@section('title', 'Gallery')
@section('content')
<!--<link type="text/css" rel="stylesheet" href="./front/About us _ Hyde Park_files/bootstrap.min.css">-->
<!--<link type="text/css" rel="stylesheet" href="./front/css/bootstrap-rtl.min.css" />-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script type="text/javascript" src="./front/js/html5shiv.min.js"></script>
    <script type="text/javascript" src="./front/js/respond.min.js"></script>
    <![endif]-->
<link type="text/css" rel="stylesheet" href="./front/About us _ Hyde Park_files/owl.carousel.css">
<script type="text/javascript" src="./front/About us _ Hyde Park_files/owl.carousel.js"></script>
<!-- Carousel -->
<script type="text/javascript" src="./front/About us _ Hyde Park_files/TweenMax.min.js"></script>
<script type="text/javascript" src="./front/About us _ Hyde Park_files/jquery.waterwheelCarousel.js"></script>
<script type="text/javascript" src="./front/About us _ Hyde Park_files/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript" src="./front/About us _ Hyde Park_files/custom.js"></script>
<link rel="stylesheet" href="./front/About us _ Hyde Park_files/lightbox.css">
<link type="text/css" rel="stylesheet" href="./front/About us _ Hyde Park_files/stylesheet.css">
<link rel="stylesheet" type="text/css" href="./front/About us _ Hyde Park_files/mainstyle-gallery.css">
<link rel="stylesheet" type="text/css" href="./front/About us _ Hyde Park_files/mainstyle.css">
<link rel="stylesheet" type="text/css" media="screen and (min-width: 992px) and (max-width: 1199px)" href="./front/About us _ Hyde Park_files/width992-1199.css">
<link rel="stylesheet" type="text/css" media="screen and (min-width: 768px) and (max-width:991px)" href="./front/About us _ Hyde Park_files/width768-991.css">
<link rel="stylesheet" type="text/css" media="screen and (max-width: 480px)" href="./front/About us _ Hyde Park_files/width480.css">
<script type="text/javascript" src="./About us _ Hyde Park_files/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="./About us _ Hyde Park_files/bootstrap.min.js"></script>

<style>
    .contentWithBGInner .photos div.row a img {
        width: auto;
        transform: none;
        position: static;
    }
</style>
<div class="clearfix"></div>
<div id="Breadcrumps">
    <div class="col-md-10 col-md-offset-1">
        <a href="<?= url('.'); ?>">HOME</a> > <?= strtoupper('EVENTS'); ?>
    </div>
    <div class="clearfix"></div>
</div>

<div id="wrapper" class="aboutPage">
    <div class="container-fluid">
        <div class="row">
            <div class="contentWithBGInner clearfix">
                <div id="About" class="aboutSection col-xs-12 col-sm-10">
                    <div class="headTitleHolder">
                        <h1 class="headTitle headTitleWzstyle">Hyde Park Gallery</h1> </div>
                    <div class="switch">
                        <h3 class="photosSection active">PHOTOS</h3>
                        <div class="divider"></div>
                        <h3 class="videoSection">VIDEOS</h3>
                    </div>

                    <div class="switch sort">
                        <h3 class="active all tabs" ><a data-id="0" href="#" class="section-name">ALL</a></h3>
                        <div class="divider"></div>
                        <?php foreach($sectionsGallery as $key=>$section){ ?>
                        <h3 class="tabs"><a data-id="<?= $section->id;?>" href="#" class="section-name"><?= strtoupper($section->name); ?></a></h3>
                            <?php if($key != count($sectionsGallery)-1) { ?>
                                <div class="divider"></div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    
                    <div class="filters clearfix">
                       <div id="name-events" class="col-sm-6 col-sm-offset-1">
                        
                       </div>
                        
                        <div class="col-sm-4">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">FILTER BY YEAR<span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                    <?php
                                    for($i = date('Y'); $i >= 2016; $i--)
                                    {
                                        ?>
                                    <li role="presentation" class="filter-photos"><a role="menuitem" tabindex="-1" href="#" data-name="<?= '?year='.$i;?>" data-section="0" class="filter-name"><?php echo $i; ?></a></li>
                                    <li role="presentation" class="filter-videos"><a role="menuitem" tabindex="-1" href="#" data-name="<?= '?year='.$i;?>" class="filter-videos-click"><?php echo $i; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        
                    </div>

                    <div class="galleryContent videos videos_data videos_scroll">


                    </div>

                    <div class="gallary-content">

                    </div>

                    <br />
                    <div class="loading">Loading</div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    //hide and show video & photo
    $(document).ready(function () {
        $(".filter-photos").show();
        $(".filter-videos").hide();
      $('.videoSection').on('click', function () {
        $(".filter-photos").hide();
        $(".filter-videos").show();
        $(".photos").removeClass("activeShow");
        $(".photosSection").removeClass("active");
        $(".gallary-content").addClass("deactive");
        $(".videos").addClass("activeShow");
        $(".videoSection").addClass("active");
        $(".filters .col-sm-4:eq(0)").addClass("deactive");
        $(".filters .col-sm-4:eq(1)").addClass("col-sm-offset-4");
        $(".switch:eq(1)").addClass("deactive");
        $(".gallary-content > div").addClass("deactive");
        $.ajax({
          url: '<?= url(\URL::current().'/videos'); ?>',
          method: 'GET',
          beforeSend: function () {
            $('.loading').show();
            $('.videos_data').empty();
          },
          success: function (response) {
            if (response !== '') {
              var data = jQuery.parseJSON(response);
              events_url = data.nexturl;
              $('.videos_data').html(data.data);
              $(window).resize();
            } else
            {
              $('.loading').hide();
            }
          },
          complete: function () {
            $('.loading').hide();
          }
        });
        
      });

      $('.photosSection').on('click', function () {
        $('.filter-photos').show();
        $(".filter-videos").hide();
        $(".videos").removeClass("activeShow");
        $(".videoSection").removeClass("active");
        $(".photos").addClass("activeShow");
        $(".photosSection").addClass("active");
        $(".gallary-content").removeClass("deactive");
        $(".filters .col-sm-4:eq(0)").removeClass("deactive");
        $(".filters .col-sm-4:eq(1)").removeClass("col-sm-offset-4");
        $(".switch:eq(1)").removeClass("deactive");
        $(".gallary-content > div").removeClass("deactive");
      });
      
      if(location.hash != "") {
        if(location.hash == "#photos")
            $('.photosSection').trigger('click');
        if(location.hash == "#videos")
            $('.videoSection').trigger('click');
      }
      $('#header a').click(function(e) {
            //e.preventDefault();
            setTimeout(function() {
                if(location.hash != "") {
                    if(location.hash == "#photos")
                        $('.photosSection').trigger('click');
                    if(location.hash == "#videos")
                        $('.videoSection').trigger('click');
                  //$('.sort h3 a[data-target='+location.hash+']').trigger('click');
                }
            }, 10);
      });
    });
    
    $(document).on('click' , '.section-name' ,function(e){
        e.preventDefault();
        var me = $(this);
        $(".sort h3").removeClass("active");
        me.parent().addClass("active");
        var section_id = me.data('id');
        $('.filter-name').attr('data-section',section_id);
        $.ajax({
          url: '<?= url(\URL::current().'/events'); ?>/'+section_id,
          method: 'GET',
          beforeSend: function () {
            $('.loading').show();
            $('.gallary-content').empty();
          },
          success: function (response) {
            if (response !== '') {
              var data = jQuery.parseJSON(response);
              $('.gallary-content').html(data.data);
              $(window).resize();
            } else
            {
              $('.loading').hide();
            }
          },
          complete: function () {
            $('.loading').hide();
          }
        });
        
        var name_events_url = '<?= url(\URL::current().'/name-events'); ?>/'+section_id;
        $.ajax({
          url: name_events_url,
          method: 'GET',
          beforeSend: function () {
            $('.loading').show();
//            $('#name-events').empty();
          },
          success: function (response) {
            if (response !== '') {
              var data = jQuery.parseJSON(response);
              $('#name-events').html(data.data);
              $(window).resize();
            } else
            {
              $('.loading').hide();
            }
          },
          complete: function () {
            $('.loading').hide();
          }
        });
    });
    
    setTimeout(function () {
            $('.section-name:first').trigger('click');
        }, 200);
    
    $(document).on('click' , '.filter-name' ,function(e){
        e.preventDefault();
        var me = $(this);
        var name = me.data('name');
        var section_id = me.data('section');
        $.ajax({
          url: '<?= url(\URL::current().'/events'); ?>/'+section_id+name,
          method: 'GET',
          beforeSend: function () {
            $('.loading').show();
            $('.gallary-content').empty();
          },
          success: function (response) {
            if (response !== '') {
              var data = jQuery.parseJSON(response);
              $('.gallary-content').html(data.data);
              $(window).resize();
            } else
            {
              $('.loading').hide();
            }
          },
          complete: function () {
            $('.loading').hide();
          }
        });
    });
    
    $(document).on('click' , '.filter-videos-click' ,function(e){
        e.preventDefault();
        var me = $(this);
        var name = me.data('name');
        $.ajax({
          url: '<?= url(\URL::current().'/videos'); ?>'+name,
          method: 'GET',
          beforeSend: function () {
            $('.loading').show();
            $('.videos_data').empty();
          },
          success: function (response) {
            if (response !== '') {
              var data = jQuery.parseJSON(response);
              $('.videos_data').html(data.data);
              $(window).resize();
            } else
            {
              $('.loading').hide();
            }
          },
          complete: function () {
            $('.loading').hide();
          }
        });
    });
    
    $(".modal").on('click', function () {
      $('.modal').css('display', 'none');
    });

    function myFunction(x) {
      var modal = x.parentElement.nextElementSibling;
      var modalimg = modal.lastElementChild;
      modal.style.display = "block";
      modalimg.src = x.src;
    }

    function hide(z) {
      var hide = z.parentElement;
      hide.style.display = "none";
    }
    $(document).ready(function () {
      loadGallery(true, 'a.thumbnail');
      //This function disables buttons when needed
      function disableButtons(counter_max, counter_current) {
        $('#show-previous-image, #show-next-image').show();
        if (counter_max == counter_current) {
          $('#show-next-image').hide();
        } else if (counter_current == 1) {
          $('#show-previous-image').hide();
        }
      }
      /**
       *
       * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
       * @param setClickAttr  Sets the attribute for the click handler.
       */
      function loadGallery(setIDs, setClickAttr) {
        var current_image, selector, counter = 0;
        $('#show-next-image, #show-previous-image').click(function () {
          if ($(this).attr('id') == 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }
          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

        function updateGallery(selector) {
          var $sel = selector;
          current_image = $sel.data('image-id');
          $('#image-gallery-caption').text($sel.data('caption'));
          $('#image-gallery-title').text($sel.data('title'));
          $('#image-gallery-image').attr('src', $sel.data('image'));
          disableButtons(counter, $sel.data('image-id'));
        }
        if (setIDs == true) {
          $('[data-image-id]').each(function () {
            counter++;
            $(this).attr('data-image-id', counter);
          });
        }
        $(setClickAttr).on('click', function () {
          updateGallery($(this));
        });
      }
    });
</script>

<script>
    var request_page = false;
    var request_video_page = false;
    var page = 2 ;

    // get more events on scroll down
   
    var page_videos_url = '<?= url(\URL::current().'/videos'); ?>?page=2';
    $(window).on('scroll', function () {
        if (request_page == false && $(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.7) {
            request_page = true;
           
            var section_id = $('.tabs.active a').data('id');
            var page_event_url = '<?= url(\URL::current().'/events'); ?>/'+section_id+'?page='+page;
            $.ajax({
                url: page_event_url,
                method: 'GET',
                beforeSend: function () {
//                    $('.loading').show();
                },
                success: function (response) {
                   if (response !== '') {
                    var data = jQuery.parseJSON(response);
                    page ++ ;
                    page_event_url = data.nexturl;
                    $('.gallary-content').append(data.data);
                    $(window).resize();
                    request_page = false;
                    } else
                    {
//                        $('.loading').hide();
                    }
                },
                complete: function () {
//                    $('.loading').hide();
                }
            });
        }

        if (request_video_page == false && $(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.7) {
            request_video_page = true;
            if (page_videos_url === undefined) {
                return false;
            }
            $.ajax({
                url: page_videos_url,
                method: 'GET',
                beforeSend: function () {
//                    $('.loading').show();
                },
                success: function (response) {
                    if (response !== '') {
                    var data = jQuery.parseJSON(response);
                    page_videos_url = data.nexturl;
                    $('.videos_data').append(data.data);
                    $(window).resize();
                    request_video_page = false;
                    } else
                    {
//                        $('.loading').hide();
                    }
                },
                complete: function () {
//                    $('.loading').hide();
                }
            });
        }
    });
    
</script>

@stop()
