@extends('front.layout')
@section('title', 'Newsletter')
@section('content')
@section('head-js')
<script src='https://www.google.com/recaptcha/api.js'></script>
@stop()

<style>
    .download-link {
        position: relative;
        display: inline-block;
        text-align: center;
        color: #4E5356;
        text-decoration: none !important;
        font-weight: bold;
    }
    .download-link .over-download {
        display: none;
        position: absolute;
        z-index: 1;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background-color: rgba(255, 255, 255, 0.5);
        text-align: center;
        padding: 7px;
    }
    .download-link:hover .over-download {display: block;}
    .download-link > div > p {
        margin-top: 12px;
        overflow: hidden;
    }
    
    .download-link.right-text > div > p {
        float: right;
    }
    .download-link.right-text > div > img {
        float: left;
        margin-right: 20px;
    }
    .download-link.left-text > div > p {
        float: left;
    }
    .download-link.left-text > div > img {
        float: right;
        margin-left: 20px;
    }
</style>


<div class="clearfix"></div>
<div id="Breadcrumps">
    <div class="col-md-10 col-md-offset-1">
        <a href="<?= url('.'); ?>">HOME</a> > <?= strtoupper('NEWSLETTER'); ?>
    </div>
    <div class="clearfix"></div>
</div>

<div id="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="contentWithBGInner  clearfix">
                <?php

                use App\Http\Controllers\Common\Helpers;

if(session('success') != '')
                    $success = session('success');
                ?>
                <?php if(isset($validate_errors) or isset($success))
                {
                    ?>
                    <?php if(isset($validate_errors))
                    {
                        ?>

                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php foreach($validate_errors as $error)
                            {
                                ?>
                                <p><?php echo $error; ?></p>
                        <?php } ?>
                        </div>
    <?php } ?>
    <?php if(!empty($success))
    {
        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><?php echo $success; ?></p>
                        </div>
    <?php } ?>

<?php } ?>
                <div class="col-xs-12 col-sm-10">

                    <div class="headTitleHolder">
                        <h1 class="headTitle headTitleWzstyle">Newsletter</h1>
                    </div>
                    <div class="row">
                        <div class="pressrelease press" style="border: none;">
                            <div class="row">
                                <a href="{{url('uploads/'.$newsletterPrevious->file)}}" target="_blank">
                                    <img class="imgload3" src="{{url('./front/images/down.png')}}" />
                                </a>
                                <div class=" col-xs-2 col-md-1">
                                    <div class="images-holder">
                                        <img src="{{url('./front/images/logopark.jpg')}}">
                                        <a href="{{url('uploads/'.$newsletterPrevious->file)}}" target="_blank"></a>
                                    </div>
                                </div>

                                <div class="col-xs-4 col-md-4">
                                    <p>{{$newsletterPrevious->title}}</p>
                                </div>
                                <div class="col-xs-2 col-md-2">
                                    <p class="text-center">@if($newsletterPrevious->date != '0000-00-00') {{date('M/Y', strtotime($newsletterPrevious->date))}} @endif</p>
                                </div>
                                <div class="col-xs-2 col-md-4">
                                    <p class="text-right">{{$newsletterPreviousArabic->title}}</p>
                                </div>

                                <div class="col-xs-2 col-md-1">
                                    <div class="images-holder">
                                        <img src="{{url('./front/images/logopark.jpg')}}">
                                        <a href="{{url('uploads/'.$newsletterPreviousArabic->file)}}" target="_blank"></a>
                                    </div>
                                    <a href="{{url('uploads/'.$newsletterPreviousArabic->file)}}" target="_blank">
                                        <img class="imgload2" src="{{url('./front/images/down.png')}}" />
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
<!--                    <br />
                    <div class="row">
                        <div class="col-xs-6 text-center">
                            <a href="{{url('uploads/'.$newsletterPrevious->file)}}" target="_blank" class="download-link right-text">
                                <div>
                                    <img src="{{url('./front/images/logopark.jpg')}}">
                                    <P>{{$newsletterPrevious->title}}</p>
                                </div>
                                <div class="over-download"><img src="{{url('./front/images/down.png')}}" /></div>
                            </a>
                        </div>
                        <div class="col-xs-6 text-center">
                            <a href="{{url('uploads/'.$newsletterPreviousArabic->file)}}" target="_blank" class="download-link left-text">
                                <div>
                                    <img src="{{url('./front/images/logopark.jpg')}}">
                                    <P>{{$newsletterPreviousArabic->title}}</p>
                                </div>
                                <div class="over-download"><img src="{{url('./front/images/down.png')}}" /></div>
                            </a>
                        </div>
                    </div>

                    <br />-->
                    <br />
                    <div class="clearfix"></div>
                    <h2 class="subTitle text-center">Subscribe to our newsletter and get our latest news, updates and announcements delivered straight to your inbox.</h2>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">

                            <form action="" method="post" class="newsletterform">
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Name" value="{{ Helpers::issetPost('name', '')}}" required />
                                </div>
                                <div class="form-group">
                                    <script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
                                    </script>
                                    <input type="text" name="phone" onkeypress="return isNumber(event)" value="{{ Helpers::issetPost('phone', '')}}" placeholder="Contact Number" required />
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" placeholder="Email" value="{{Helpers::issetPost('email', '')}}" required />
                                </div>
                                <div class="form-group">
                                    <div class="radio">
                                        <input name="type" value="1" type="radio" id="option1" required />
                                        <label for="option1">Homeowner</label>
                                    </div>
                                    <div class="radio">
                                        <input name="type" value="2" type="radio" id="option2" required />
                                        <label for="option2">Interested In Hyde Park</label>
                                    </div>
                                </div>
                                <div class="form-group unitNo hide">
                                    <label>Unit Number</label>
                                    <div class="clearfix">
                                        <select name="code">
                                            <option value="HPR">HPR</option>
                                            <option value="CVA">CVA</option>
                                            <option value="HPD">HPD</option>
                                            <option value="HPA">HPA</option>
                                        </select>
                                        <input type="text" name="unit_number" value="{{Helpers::issetPost('unit_number', '')}}" />
                                    </div>
                                </div>
                                <div class="g-recaptcha" data-sitekey="6LfQGhEUAAAAAMSePHEZnRcgS5aMPmzxFN-4O2xH"></div>
                                <div class="clearfix" style="margin-bottom: 15px;"></div>
                                <div class="btn-container">
                                    <button type="submit" name="send" class="btn">Subscribe</button>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>


@stop()
