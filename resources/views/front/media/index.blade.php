@extends('front.layout')
@section('title', 'IN MEDIA')
@section('content')
<div class="clearfix"></div>
<div id="Breadcrumps">
    <div class="col-md-10 col-md-offset-1">
        <a href="<?= url('.'); ?>">HOME</a> > <?= strtoupper('In media'); ?>
    </div>
    <div class="clearfix"></div>
</div>
<div id="wrapper" class="aboutPage">
    <div class="container-fluid">
        <div class="row">
            <div class="contentWithBGInner clearfix">
                <div id="About" class="aboutSection col-xs-12 col-sm-10">
                    <div class="headTitleHolder">
                        <h1 class="headTitle headTitleWzstyle">IN MEDIA</h1>
                    </div>

                    <div class="filters clearfix">
                        <div class="col-sm-6">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">FILTER BY NAME<span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                    @foreach($sections as $row)
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?name=' . $row->name; ?>">{{$row->name}}</a></li>
                                    @if(is_object($row->children))
                                    @foreach($row->children as $child)
                                    <li style="margin-left: 10px;" role="presentation"><a role="menuitem" tabindex="-1" href="<?= \URL::Current() . '?name=' . $child->name; ?>">{{$child->name}}</a></li>
                                    @endforeach
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">FILTER BY YEAR<span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                    @for($i = date('Y'); $i >= 2016; $i--)
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo \URL::Current() . '?year=' . $i; ?>"><?php echo $i; ?></a></li>
                                    @endfor
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="result_data result_scroll">
                        @foreach($result as $section)

                        <div class="media-doc-section">
                            <?php $count = 1; ?>
                            <h2 id="park">{{$section->name}}</h2>
                            @foreach($section->media as $key=>$row)
                            @if ($count%2 == 1)
                            <div class="row">
                                @endif
                                <div class="col-md-6 col-sm-12">
                                    <div class="media">
                                        <div class=" col-xs-2 col-md-2">
                                            <a href="{{url('uploads/'.$row->file)}}" target="_blank">
                                                <img class="imgload" src="{{url('./front/images/down.png')}}" />
                                            </a>
                                            <div class="images-holder">
                                                <img src="{{url('./front/images/logopark.jpg')}}">
                                                <a href="{{url('uploads/'.$row->file)}}" target="_blank"></a>
                                            </div>
                                        </div>
                                        <a href="{{url('uploads/'.$row->file)}}" target="_blank">
                                            <div class="col-xs-6 col-md-8 ">
                                                <P>{{$row->title}}</P>
                                            </div>
                                            <div class="col-xs-2 col-md-2">
                                                <p>@if($row->date != '0000-00-00') {{date('M/Y', strtotime($row->date))}} @endif</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                @if ($count%2 == 0 )
                            </div>
                            @elseif($key== count($section->media)-1)
                        </div>

                        @endif
                        <?php $count++; ?>
                        @endforeach
                        
                        
                        

                        @if(is_object($section->children))
                        @foreach($section->children as $child)
                        <div class="media-doc-section">
                            <?php $count = 1; ?>
                            <h3 style="margin-left: 20px; margin-top: 20px;" id="park">{{$child->name}}</h3>
                            @foreach($child->media as $key=>$row)
                            @if ($count%2 == 1)
                            <div class="row">
                                @endif
                                <div class="col-md-6 col-sm-12">
                                    <div class="media">
                                        <div class=" col-xs-2 col-md-2">
                                            <a href="{{url('uploads/'.$row->file)}}" target="_blank">
                                                <img class="imgload" src="{{url('./front/images/down.png')}}" />
                                            </a>
                                            <div class="images-holder">
                                                <img src="{{url('./front/images/logopark.jpg')}}">
                                                <a href="{{url('uploads/'.$row->file)}}" target="_blank"></a>
                                            </div>
                                        </div>
                                        <a href="{{url('uploads/'.$row->file)}}" target="_blank">
                                            <div class="col-xs-6 col-md-8 ">
                                                <P>{{$row->title}}</P>
                                            </div>
                                            <div class="col-xs-2 col-md-2">
                                                <p>@if($row->date != '0000-00-00') {{date('M/Y', strtotime($row->date))}} @endif</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                @if ($count%2 == 0 )
                            </div>
                            @elseif($key== count($child->media)-1)
                        </div>
                        @endif
                        <?php $count++; ?>
                        @endforeach
                        
                        @endforeach
                        @endif

                    </div>
                    @endforeach
                </div>


            </div>
        </div>
    </div>
</div>
</div>
<script>
    var page = 2;
    var request_page = false;

    // get more result on scroll down
    $(window).on('scroll', function () {
        var page_url = '<?= url('./press/more/?name=' . urlencode(@$_GET["name"]) . '&year=' . @$_GET["year"]); ?>&page=' + page;
        if (request_page == false && $(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.7) {
            request_page = true;
            if (page_url === undefined) {
                return false;
            }
            //  alert("page "+page_url);
            $.ajax({
                url: page_url,
                method: 'GET',
                beforeSend: function () {
                    $('.loading').show();
                },
                success: function (response) {
                    page++;
                    if (response !== '') {
                        $('.result_data').append(response);
                        $(window).resize();
                        request_page = false;
                    } else
                    {
                        $('.loading').hide();
                    }
                },
                complete: function () {
                    $('.loading').hide();
                }
            });
        }
    });
</script>

@stop()
