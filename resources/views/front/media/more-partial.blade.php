@foreach($result as $section)

<div class="media-doc-section">
    <?php $count = 1; ?>
    <h2 id="park">{{$section->name}}</h2>
    @foreach($section->media as $key=>$row)
    @if ($count%2 == 1)
    <div class="row">
        @endif
        <div class="col-md-6 col-sm-12">
            <div class="media">
                <div class=" col-xs-2 col-md-2">
                    <a href="{{url('uploads/'.$row->file)}}" target="_blank">
                        <img class="imgload" src="{{url('./front/images/down.png')}}" />
                    </a>
                    <div class="images-holder">
                        <img src="{{url('./front/images/logopark.jpg')}}">
                        <a href="{{url('uploads/'.$row->file)}}" target="_blank"></a>
                    </div>
                </div>
                <a href="{{url('uploads/'.$row->file)}}" target="_blank">
                    <div class="col-xs-8 col-md-8 ">
                        <P>{{$row->title}}</P>
                    </div>
                    <div class="col-xs-2 col-md-2">
                        <p>@if($row->date != '0000-00-00') {{date('M/Y', strtotime($row->date))}} @endif</p>
                    </div>
                </a>
            </div>
        </div>
        @if ($count%2 == 0 )
    </div>
    @elseif($key== count($section->media)-1)
</div>
@endif
<?php $count++; ?>
@endforeach

</div>
@endforeach