@extends('front.layout')
@section('description',trans('main.Description'))
@section('keywords',trans('main.Keywords'))
@section('title', trans('main.Error_404') .' | '.trans('main.NameSite'))
@section('content')
<style>
    .error-page {
        text-align: center;
        padding: 100px 0;
    }
</style>
<div class="error-page">
    <h1><?= trans('main.Error_404'); ?></h1>
    <p><?= trans('main.Sorry_The_page_not_exist'); ?></p>
</div>






@stop()