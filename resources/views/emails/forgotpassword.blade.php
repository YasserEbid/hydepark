<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <meta name="viewport" content="width=device-width">
                </head>

                <body>
                    <table class="body" data-made-with-foundation="" style="border-spacing:0;border-collapse:collapse;vertical-align:top;background-color:#f3f3f3;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;height:100%;width:100%;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:19px;" >
                        <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;" >
                            <td class="center" align="center" valign="top" style="word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:19px;" >
                                <center data-parsed="" style="width:100%;min-width:580px;" >
                                    <table class="container text-center" style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;background-color:#fefefe;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;width:580px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;Margin:0 auto;text-align:center;" >
                                        <tbody>
                                            <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;" >
                                                <td style="word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:19px;" >
                                                    <table class="row header" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;background-color:#8a8a8a;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;width:100%;position:relative;display:table;" >
                                                        <tbody>
                                                            <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;" >
                                                                <th class="small-12 large-12 columns first last" style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;text-align:left;font-size:16px;line-height:19px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;Margin:0 auto;width:564px;padding-left:8px;padding-right:8px;padding-bottom:0;" >
                                                                    <table style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;width:100%;" >
                                                                        <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;" >
                                                                            <th style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:19px;" >
                                                                                <h4 class="text-center" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-right:0;margin-left:0;Margin:0;line-height:1.3;color:inherit;word-wrap:normal;font-family:Helvetica, Arial, sans-serif;font-weight:normal;margin-bottom:10px;Margin-bottom:10px;font-size:24px;text-align:center;" >Forgot Your Password?</h4> </th>
                                                                            <th class="expander" style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:19px;visibility:hidden;width:0;padding-top:0 !important;padding-bottom:0 !important;padding-right:0 !important;padding-left:0 !important;" ></th>
                                                                        </tr>
                                                                    </table>
                                                                </th>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="row" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;width:100%;position:relative;display:table;" >
                                                        <tbody>
                                                            <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;" >
                                                                <th class="small-12 large-12 columns first last" style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;text-align:left;font-size:16px;line-height:19px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;Margin:0 auto;padding-bottom:16px;width:564px;padding-left:8px;padding-right:8px;" >
                                                                    <table style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;width:100%;" >
                                                                        <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;" >
                                                                            <th style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:19px;" >
                                                                                <center data-parsed="" style="width:100%;min-width:532px;" > <img src="<?= $reciver_image; ?>" align="center" class="text-center" style="outline-style:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:auto;max-width:100%;clear:both;display:block;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;Margin:0 auto;float:none;text-align:center;" /> </center>
                                                                                <h1 class="text-center" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-right:0;margin-left:0;Margin:0;line-height:1.3;color:inherit;word-wrap:normal;font-family:Helvetica, Arial, sans-serif;font-weight:normal;margin-bottom:10px;Margin-bottom:10px;font-size:34px;text-align:center;" ><?= $reciver_name; ?></h1>
                                                                                <p class="text-center" style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-right:0;margin-left:0;Margin:0;font-size:16px;line-height:19px;margin-bottom:10px;Margin-bottom:10px;text-align:center;" >It happens. Your informations.</p>
                                                                                <table class="button large expand" style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;margin-top:0;margin-bottom:16px;margin-right:0;margin-left:0;Margin:0 0 16px 0;width:100% !important;" >
                                                                                    <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;" >
                                                                                        <td style="word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:19px;" >
                                                                                            <table style="border-spacing:0;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;width:100%;" >
                                                                                                <tr style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;vertical-align:top;text-align:left;" >
                                                                                                    <td style="word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;font-size:16px;line-height:19px;width:auto !important;text-align:left;color:#fefefe;background-color:#2199e8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-width:2px;border-style:solid;border-color:#2199e8;" >Copy this code : <?= $reciver_code; ?></td>
                                                                                                    <td class="expander" style="word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse !important;vertical-align:top;font-family:Helvetica, Arial, sans-serif;font-weight:normal;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;font-size:16px;line-height:19px;visibility:hidden;padding-top:0 !important;padding-bottom:0 !important;padding-right:0 !important;padding-left:0 !important;width:auto !important;text-align:left;color:#fefefe;background-color:#2199e8;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-width:2px;border-style:solid;border-color:#2199e8;" ></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <hr style="max-width:580px;height:0;border-right-width:0;border-top-width:0;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#cacaca;border-left-width:0;margin-top:20px;margin-bottom:20px;margin-right:auto;margin-left:auto;Margin:20px auto;clear:both;" >
                                                                                    <p style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:19px;margin-bottom:10px;Margin-bottom:10px;" ><small style="font-size:80%;color:#cacaca;" >To login, <a href="<?= $url_login; ?>" target="_blank" style="font-family:Helvetica, Arial, sans-serif;font-weight:normal;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;line-height:1.3;color:#2199e8;text-decoration:none;" >Click Here</a>.</small></p>
                                                                            </th>
                                                                            <th class="expander" style="color:#0a0a0a;font-family:Helvetica, Arial, sans-serif;font-weight:normal;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;Margin:0;text-align:left;font-size:16px;line-height:19px;visibility:hidden;width:0;padding-top:0 !important;padding-bottom:0 !important;padding-right:0 !important;padding-left:0 !important;" ></th>
                                                                        </tr>
                                                                    </table>
                                                                </th>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </center>
                            </td>
                        </tr>
                    </table>

                </body>

                </html>
