@extends('backend.layout')
@section('title','Edit data | Home section')
@section('content')
@include('backend.message')
<?php
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Helpers;
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>About us</h2>
        <ol class="breadcrumb">
                <?php if(Roles::check('home.index')){ ?>
                <li>
                    <a href="<?= url('backend/home'); ?>">Home page</a>
                </li>
                <?php } ?>
            <li class="active">
                <strong>Edit data</strong>
            </li>
        </ol>
    </div>

</div>    

<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Home section data</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <form method="post" class="form-horizontal" id="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label">Title</label>
                                            <input type="text" name="title" placeholder="Please enter title" class="form-control" value="<?= Helpers::issetPost('title', $section->title); ?>" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label">Sub title</label>
                                            <input type="text" name="sub_title" placeholder="Please enter Sub title" class="form-control" value="<?= Helpers::issetPost('sub_title', $section->sub_title); ?>" >
                                        </div>
                                    </div>
                                    <?php if($section->link !=''){?>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label">Video link</label>
                                            <input type="text" name="link" placeholder="Please enter link" class="form-control" value="<?= Helpers::issetPost('link', $section->link); ?>" >
                                        </div>
                                    </div>
                                    <?php }?>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label">Description</label>
                                            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_css(); ?>
                                            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_editor('description', 'description', $section->description); ?>
                                            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_js(); ?>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
                                    </div>
                                </form>
                                <script>
                                    $(document).ready(function () {

                                        $("#form").validate({
                                            rules: {
                                                <?php if($section->link !=''){?>
                                                link: {
                                                    required: true
                                                },
                                                <?php }?>
                                                description: {
                                                    required: true
                                                }
                                            }
                                        });
                                    });
                                </script>  

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



</div>

@stop()