@extends('backend.layout')
@section('title','Section one')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Section one</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Section one</strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">
                    
                     <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Section one data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover example1">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Sub title</th>
                            <th>Link</th>
                            <th>Description</th>
                            <?php if(Roles::check('homesections.edit') || Roles::check('homesections.preview')){?>
                            <th>Settings</th>
                            <?php }?>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            <td><?= $section->title;?></td>
                            <td><?= $section->sub_title;?></td>
                            <td><a href="<?= $section->link;?>" target="_blank">Visit link</a></td>
                            <td><?= $section->description;?></td>
                            <?php if(Roles::check('homesections.edit') || Roles::check('homesections.preview')){?>
                            <td>
                                <?php if(Roles::check('homesections.edit')){?>
                                <a href="<?= url('backend/home-sections/edit/'.$section->id);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                                <?php }?>
                                
                                <?php if(Roles::check('homesections.preview')){?>
                                <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal<?= $section->id;?>"><i class="fa fa-twitch"></i> Preview</button>
                                <div class="modal inmodal" id="myModal<?= $section->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-laptop modal-icon"></i>
                                                <h4 class="modal-title">Preview all data</h4>
                                            </div>
                                            <div class="modal-body">
                                                <blockquote>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Title</strong> : <?= $section->title; ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Sub title</strong> : <?= $section->sub_title; ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Link</strong> : <a href="<?= $section->link;?>" target="_blank">Visit link</a>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Description</strong> : <?= $section->description; ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created date</strong> : <?= $section->createdAt(); ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Updated date</strong> : <?= $section->updatedAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created by</strong> : <?php if(is_object($section->admin)){echo $section->admin->name;} ?>
                                                    </p>
                                                </blockquote>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                
                            </td>
                            <?php }?>
                        </tr>
                       
                    </tbody>
                </table>
                    
                </div>
                </div>
            </div>
        </div>
                    
                    

                </div>

            </div>

        </div>
    </div>



</div>



@stop()