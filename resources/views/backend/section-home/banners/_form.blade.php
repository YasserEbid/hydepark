<?php
use App\Http\Controllers\Common\Helpers;

if(!isset($banner))
    $banner = new \App\Models\SectionTwoBanners;
?>
<form method="post" class="form-horizontal" id="form">
    
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Name</label>
            <input type="text" name="name" placeholder="Please enter name" class="form-control" value="<?= Helpers::issetPost('name', $banner->name); ?>" required >
        </div>
    </div>
    
    <div class="form-group">
    <div class="col-sm-12">
        <div class="btn btn-default btn-file">
            <input type='hidden' name='image' value='<?= $banner->image;?>' />
           <?php
           if($banner->image != '')
               $banner->image = './uploads/'.$banner->image ;
           else
               $banner->image = './backend/images/avatar_image.png' ;
           ?>
           <?= \App\Http\Controllers\Common\Components::uploader($banner->image) ?>
        </div>
         <p class="help-block">Upload image only , Max size image 2 MB</p>
    </div>
</div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                image: {
                    required: true
                }
            }
        });
    });
</script>