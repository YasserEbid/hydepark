@extends('backend.layout')
@section('title','Create new banner | Section two banners')
@section('content')
@include('backend.message')
<?php use App\Http\Controllers\Backend\Roles\Roles;?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Section two banners</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <?php if(Roles::check('backendusers.index')){?>
            <li>
                <a href="<?= url('backend/home-sections/banners');?>">Section two banners</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Create new banner</strong>
            </li>
        </ol>
    </div>
    
</div>    

<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Section two banners data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
               
              @include('backend.section-home.banners._form')  
                
            </div>
            </div>
        </div>
    </div>
 </div>

        </div>
    </div>



</div>

@stop()