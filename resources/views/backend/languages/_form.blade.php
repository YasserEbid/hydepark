<?php
use App\Http\Controllers\Common\Helpers;

if(!isset($language))
    $language = new \App\Models\Languages;
?>
<form method="post" class="form-horizontal" id="form">
    
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Name</label>
            <input type="text" name="name" placeholder="Please enter name" class="form-control" value="<?= Helpers::issetPost('name', $language->name); ?>" required >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Symbol</label>
            <input type="text" name="symbol" placeholder="Please enter symbol" class="form-control" value="<?= Helpers::issetPost('symbol', $language->symbol); ?>" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">RTL</label>
            <div class="i-checks"><label> <input type="radio" name="rtl" <?= Helpers::checkedPost('rtl', $language->rtl, true);?> value="1" required> <i></i> Yes </label></div>
            <div class="i-checks"><label> <input type="radio" name="rtl" <?= Helpers::checkedPost('rtl', $language->rtl, false);?> value="0" required> <i></i> No </label></div>
        </div>
    </div>
    <div class="form-group">
    <div class="col-sm-12">
        <div class="btn btn-default btn-file">
            <input type='hidden' name='image' value='<?= $language->image;?>' />
           <?php
           if($language->image != '')
               $language->image = './uploads/languages/'.$language->image ;
           else
               $language->image = './backend/images/avatar_image.png' ;
           ?>
           <?= \App\Http\Controllers\Common\Components::uploader($language->image) ?>
        </div>
         <p class="help-block">Upload image only , Max size image 2 MB</p>
    </div>
</div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                name: {
                    required: true
                },
                symbol: {
                    required: true
                },
                rtl: {
                    required: true
                }
            }
        });
    });
</script>