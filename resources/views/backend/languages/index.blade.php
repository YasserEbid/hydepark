@extends('backend.layout')
@section('title','Languages')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Languages</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Languages</strong>
            </li>
        </ol>
    </div>
    <?php if(Roles::check('languages.create')){?>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?= url(\URL::Current().'/create');?>" class="btn btn-outline btn-primary">Create new Language</a>
        </div>
    </div>
    <?php }?>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Languages data</h5>
                                 
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">    
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">Sorting</button>
                                <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated flipInY">
                                            <form action="" method="post">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="modal-title">Sorting</h4>
                                                <small class="font-bold"><i class="fa fa-hand-o-up"></i> Drag languages between list</small>
                                            </div>
                                            <div class="modal-body">
                                                
                                                <ul class="sortable-list connectList agile-list" id="todo">
                                                    <?php foreach($sorting as $sort){?>
                                                    <li class="success-element col-sm-3" id="task<?= $sort->id; ?>">
                                                       <?= $sort->name;?>
                                                        <div class="agile-detail">
                                                            <img src="<?= url('uploads/languages/'.$sort->image);?>" />
                                                        </div>
                                                        <input type="hidden" name="sort[]" value="<?= $sort->id; ?>" />
                                                    </li>
                                                    <?php }?>
                                                   
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                <button type="submit" name="save" class="btn btn-primary">Save changes</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                 <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover example1">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Symbol</th>
                                            <th>RTL</th>
                                            <th>Icon</th>
                                            <th>Is default</th>
                                            <?php if(Roles::check('languages.edit') || Roles::check('languages.delete') || Roles::check('languages.activated')){?>
                                            <th>Settings</th>
                                            <?php }?>
                                        </tr>
                                    </thead>
                                    <tbody>
                        <?php foreach($languages as $language){?>
                        <tr>
                            <td><?= $language->name;?></td>
                            <td><?= $language->symbol;?></td>
                            <td><?php if($language->rtl == 1){?> <span class="label label-primary font-bold">True</span> <?php }else{?> <span class="label label-danger font-bold">False</span> <?php }?></td>
                            <td><img src="<?= url('uploads/languages/'.$language->image);?>" /></td>
                            <td><?= Components::radioInput('is_default', $language->id, url(\URL::Current().'/defaulting/'), Helpers::checked($language->is_default, true))?></td>
                            <?php if(Roles::check('languages.edit') || Roles::check('languages.delete') || Roles::check('languages.activated')){?>
                            <td>
                                <?php if(Roles::check('languages.edit')){?>
                                <a href="<?= url(\URL::Current().'/edit/'.$language->id);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-paste"></i> Edit</a>
                                <?php }?>
                                
                                <?php if(Roles::check('languages.delete')){?>
                                <?= Components::deleteRow($language->id, url(\URL::Current().'/delete'));?>
                                <?php }?>
                                
                                <?php if(Roles::check('languages.activated')){?>
                                <?= Components::checkboxInput($language->is_active, $language->id, url(\URL::Current().'/activated/'),  Helpers::checked($language->is_active, true));?>
                                <?php }?>
                                
                            </td>
                            <?php }?>
                        </tr>
                        <?php }?>
                    </tbody>
                                    </table>
                                <?= $languages->render();?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



</div>





@stop()