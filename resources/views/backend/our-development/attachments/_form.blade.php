<?php
use App\Http\Controllers\Common\Helpers;

if(!isset($attachments))
    $attachments = new \App\Models\OurDevelopmentAttachments;
?>
<form method="post" class="form-horizontal" id="form">
    
    <div class="form-group">
    <div class="col-sm-12">
        <label class="control-label">Choose developement</label>
        <select name="developement_id" class="select2clear form-control" required>
            <option selected disabled>Please choose</option>
            <?php foreach($our_developement as $developement){?>
            <option <?= Helpers::selectedPost('developement_id', $attachments->developement_id, $developement->id);?> value="<?= $developement->id;?>"><?= $developement->page_name;?></option>
            <?php }?>
        </select>
    </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Title</label>
            <input type="text" name="title" placeholder="Please enter title" class="form-control" value="<?= Helpers::issetPost('title', $attachments->title); ?>" required >
        </div>
    </div>
    
    <div class="col-sm-6">
    <div class="form-group">
        <div class="btn btn-default btn-file">
            <input type='hidden' name='image' value='<?= $attachments->image;?>' />
           <?php
           if($attachments->image != '')
               $attachments->image = './uploads/'.$attachments->image ;
           else
               $attachments->image = './backend/images/avatar_image.png' ;
           ?>
           <?= \App\Http\Controllers\Common\Components::uploader($attachments->image) ?>
        </div>
         <p class="help-block">Upload image</p>
    </div>
</div>
    
    <div class="col-sm-6">
    <div class="form-group">
        <div class="btn btn-default btn-file">
            <input type='hidden' name='file' value='<?= $attachments->file;?>' />
           <?php
           if($attachments->file != '')
               $attachments->file = './uploads/'.$attachments->file ;
           else
               $attachments->file = './backend/images/avatar_image.png' ;
           ?>
           <?= \App\Http\Controllers\Common\Components::uploaderFile($attachments->file) ?>
        </div>
         <p class="help-block">Upload file </p>
    </div>
</div>
    
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {

                title: {
                    required: true
                },
                image: {
                    required: true
                }
            }
        });
    });
</script>