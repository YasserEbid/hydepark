@extends('backend.layout')
@section('title','Attachments')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Attachments</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Attachments</strong>
            </li>
        </ol>
    </div>
    <?php if(Roles::check('ourdevelopment.createattachments')){?>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?= url('backend/our-development/create-attachments');?>" class="btn btn-outline btn-primary">Create new attachment</a>
        </div>
    </div>
    <?php }?>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                
                <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Attachments data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover example2">
                    <thead>
                        <tr>
                            <th>Page</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>File</th>
                            <?php if(Roles::check('ourdevelopment.editattachments') || Roles::check('ourdevelopment.previewattachments') || Roles::check('ourdevelopment.deleteattachments')){?>
                            <th>Settings</th>
                            <?php }?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($attachments as $attachment){?>
                        <tr>
                            <td><?= $attachment->pageName->page_name;?></td>
                            <td><?= $attachment->title;?></td>
                            <td><?= Components::imageFancy($attachment->title, url('uploads/'.$attachment->image));?></td>
                            <td><a href="<?= url('uploads/'.$attachment->file);?>" target="_blank">View</a></td>
                            <?php if(Roles::check('ourdevelopment.editattachments') || Roles::check('ourdevelopment.previewattachments') || Roles::check('ourdevelopment.deleteattachments')){?>
                            <td>
                                
                                <?php if(Roles::check('ourdevelopment.editattachments')){?>
                                <a href="<?= url('backend/our-development/edit-attachments/'.$attachment->id);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-paste"></i> Edit</a>
                                <?php }?>
                                
                                <?php if(Roles::check('ourdevelopment.previewattachments')){?>
                                <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal<?= $attachment->id;?>"><i class="fa fa-twitch"></i> Preview</button>
                                <div class="modal inmodal" id="myModal<?= $attachment->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-laptop modal-icon"></i>
                                                <h4 class="modal-title">Preview all data</h4>
                                            </div>
                                            <div class="modal-body">
                                                <blockquote>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Page name</strong> : <?= $attachment->pageName->page_name; ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Title</strong> : <?= $attachment->title; ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Created date</strong> : <?= $attachment->createdAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Updated date</strong> : <?= $attachment->updatedAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created by</strong> : <?= $attachment->admin->name; ?>
                                                    </p>
                                                </blockquote>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                
                                <?php if(Roles::check('ourdevelopment.deleteattachments')){?>
                                <?= Components::deleteRow($attachment->id, url('our-development/delete-attachments'));?>
                                <?php }?>
                                
                            </td>
                            <?php }?>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                    <?= $attachments->render();?>
                </div>
                </div>
            </div>
        </div>
    </div>
                
                 </div>

        </div>
    </div>



</div>
                
           

@stop()