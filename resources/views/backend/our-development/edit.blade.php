@extends('backend.layout')
@section('title','Edit data | Our development')
@section('content')
@include('backend.message')
<?php
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Helpers;
if(!isset($development))
    $development = new \App\Models\BackendUsers;
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Our development</h2>
        <ol class="breadcrumb">
                <?php if(Roles::check('home.index')){ ?>
                <li>
                    <a href="<?= url('backend/home'); ?>">Home page</a>
                </li>
                <?php } ?>
                <?php if(Roles::check('ourdevelopment.index')){ ?>
                <li>
                    <a href="<?= url('backend/our-development'); ?>">Our development</a>
                </li>
                <?php } ?>
            <li class="active">
                <strong>Edit data</strong>
            </li>
        </ol>
    </div>

</div>    

<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Our development data</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <form method="post" class="form-horizontal" id="form">
                                    
<!--                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label">Page name</label>
                                            <input type="text" name="page_name" placeholder="Please enter page name" class="form-control" value="<?= Helpers::issetPost('page_name', $development->page_name); ?>" required >
                                        </div>
                                    </div>-->
                                    
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label">Description</label>
                                            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_css(); ?>
                                            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_editor('description', 'description', $development->description); ?>
                                            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_js(); ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="btn btn-default btn-file">
                                            <input type='hidden' name='image' value='<?= $development->image;?>' />
                                           <?php
                                           if($development->image != '')
                                               $development->image = './uploads/'.$development->image ;
                                           else
                                               $development->image = './backend/images/avatar_image.png' ;
                                           ?>
                                           <?= \App\Http\Controllers\Common\Components::uploader($development->image) ?>
                                        </div>
                                         <p class="help-block">Upload image only , Max size image 2 MB</p>
                                    </div>
                                </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
                                    </div>
                                </form>
                                <script>
                                    $(document).ready(function () {

                                        $("#form").validate({
                                            rules: {
                                                page_name: {
                                                    required: true
                                                },
                                                description: {
                                                    required: true
                                                }
                                                image: {
                                                    required: true
                                                }
                                            }
                                        });
                                    });
                                </script>  

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



</div>

@stop()