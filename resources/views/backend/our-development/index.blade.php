@extends('backend.layout')
@section('title','Our development')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Our development</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Our development</strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">
                    
                     <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Our development data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover example1">
                    <thead>
                        <tr>
                            <th>Page name</th>
                            <th>Image</th>
                            <th>Description</th>
                            <?php if(Roles::check('ourdevelopment.edit') || Roles::check('ourdevelopment.preview')){?>
                            <th>Settings</th>
                            <?php }?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($developments as $development){?>
                        <tr>
                            <td><?= $development->page_name;?></td>
                            <td><?php if($development->image !=''){echo Components::imageFancy($development->page_name, url('uploads/'.$development->image));}?></td>
                            <td><?= $development->description;?></td>
                            <?php if(Roles::check('ourdevelopment.edit') || Roles::check('ourdevelopment.preview')){?>
                            <td>
                                <?php if(Roles::check('ourdevelopment.edit')){?>
                                <a href="<?= url(\URL::Current().'/edit/'.$development->id);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                                <?php }?>
                                
                                <?php if(Roles::check('ourdevelopment.preview')){?>
                                <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal<?= $development->id;?>"><i class="fa fa-twitch"></i> Preview</button>
                                <div class="modal inmodal" id="myModal<?= $development->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-laptop modal-icon"></i>
                                                <h4 class="modal-title">Preview all data</h4>
                                            </div>
                                            <div class="modal-body">
                                                <blockquote>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Page name</strong> : <?= $development->page_name; ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Description</strong> : <?= $development->description; ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created date</strong> : <?= $development->createdAt(); ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Updated date</strong> : <?= $development->updatedAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created by</strong> : <?php if(is_object($development->admin)){echo $development->admin->name;} ?>
                                                    </p>
                                                </blockquote>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                
                            </td>
                            <?php }?>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                    
                </div>
                </div>
            </div>
        </div>
                    
                    

                </div>

            </div>

        </div>
    </div>



</div>



@stop()