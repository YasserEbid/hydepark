<?php
use App\Http\Controllers\Common\Helpers;

if(!isset($page))
    $page = new \App\Models\PagesActions;
?>
<form method="post" class="form-horizontal" id="form">
    
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Action name</label>
            <input type="text" name="name" placeholder="Please enter action name" class="form-control" value="<?= Helpers::issetPost('name',$page->name);?>" required >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Action functionalty</label>
            <input type="text" name="action" placeholder="Please enter name class and function /* Example home.index */" class="form-control" value="<?= Helpers::issetPost('action',$page->action);?>" required >
        </div>
        </div>
    
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                action: {
                    required: true,
                    minlength: 3
                }
            }
        });
    });
</script>