<?php
use App\Http\Controllers\Common\Helpers;

if(!isset($page))
    $page = new \App\Models\Pages;
?>
<form method="post" class="form-horizontal" id="form">
    
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Page name</label>
            <input type="text" name="page" placeholder="Please enter page name" class="form-control" value="<?= Helpers::issetPost('page',$page->page);?>" required >
        </div>
        </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Link</label>
            <textarea name="link" class="form-control" placeholder="Please enter link page"><?= Helpers::issetPost('link', $page->link);?></textarea>
        </div>
        </div>
    
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                link: {
                    required: true,
                    minlength: 3
                }
            }
        });
    });
</script>