@extends('backend.layout')
@section('title','Pages')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Pages</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Pages</strong>
            </li>
        </ol>
    </div>
    <?php if(Roles::check('pages.create')){?>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?= url(\URL::Current().'/create');?>" class="btn btn-outline btn-primary">Create new page</a>
        </div>
    </div>
    <?php }?>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Pages data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover example4">
                    <thead>
                        <tr>
                            <th>Page name</th>
                            <th>Settings</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($pages as $row){?>
                        <tr>
                            <td><?= $row->page;?></td>
                            <td>
                                <?php if(Roles::check('pages.edit')){?>
                                <a href="<?= url(\URL::Current().'/edit/'.$row->id);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-paste"></i> Edit</a>
                                <?php }?>
                                
                                <?php if(Roles::check('pages.preview')){?>
                                <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal<?= $row->id;?>"><i class="fa fa-twitch"></i> Preview</button>
                                <div class="modal inmodal" id="myModal<?= $row->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-laptop modal-icon"></i>
                                                <h4 class="modal-title">Preview all data</h4>
                                            </div>
                                            <div class="modal-body">
                                                <blockquote>
                                                    <p>
                                                        <strong style="font-size: 18px;">Page</strong> : <?= $row->page; ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created date</strong> : <?= $row->createdAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Updated date</strong> : <?= $row->updatedAt(); ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Created by</strong> : <?= $row->admin->name; ?>
                                                    </p>

                                                </blockquote>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                
                                <?php if(Roles::check('pages.pageactions')){?>
                                <a href="<?= url(\URL::Current().'/page-actions/'.$row->id);?>" class=" btn btn-xs btn-outline btn-info"><i class="fa fa-code"></i> Page actions</a>
                                <?php }?>
                                
                                <?php if(Roles::check('pages.delete')){?>
                                <?= Components::deleteRow($row->id, url(\URL::Current().'/delete'));?>
                                <?php }?>
                                
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                    <?= $pages->render();?>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

        </div>
    </div>



</div>

@stop()