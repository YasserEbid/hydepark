@extends('backend.layout')
@section('title',$titlPage.' | Setup mails')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2><?= $titlPage;?></h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <?php if(Roles::check('setupmails.index')){?>
            <li>
                <a href="<?= url('backend/setup-mails');?>">Setup mails</a>
            </li>
            <?php }?>
            <li class="active">
                <strong><?= $titlPage;?></strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                
                <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5><?= $dataPage;?></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover example2">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <?php if(Roles::check('usersbackend.edit') || Roles::check('usersbackend.preview') || Roles::check('usersbackend.delete') || Roles::check('usersbackend.activated')){?>
                            <th>Settings</th>
                            <?php }?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($allSetup as $setup){?>
                        <tr>
                            <td><?= $setup->email;?></td>
                            <?php if(Roles::check('setupmails.edit') || Roles::check('setupmails.preview') || Roles::check('setupmails.delete') || Roles::check('setupmails.activated')){?>
                            <td>
                                <?php if(Roles::check('setupmails.edit')){?>
                                <a href="<?= url('backend/setup-mails/edit/'.$setup->id);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-paste"></i> Edit</a>
                                <?php }?>
                                
                                <?php if(Roles::check('setupmails.preview')){?>
                                <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal<?= $setup->id;?>"><i class="fa fa-twitch"></i> Preview</button>
                                <div class="modal inmodal" id="myModal<?= $setup->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-laptop modal-icon"></i>
                                                <h4 class="modal-title">Preview all data</h4>
                                            </div>
                                            <div class="modal-body">
                                                <blockquote>
                                                    <p>
                                                        <strong style="font-size: 18px;">Email</strong> : <?= $setup->email; ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Type</strong> : <?= $setup->type;?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created date</strong> : <?= $setup->createdAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Updated date</strong> : <?= $setup->updatedAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created by</strong> : <?= $setup->admin->name; ?>
                                                    </p>
                                                </blockquote>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                
                                <?php if(Roles::check('setupmails.delete')){?>
                                <?= Components::deleteRow($setup->id, url('backend/setup-mails/delete'));?>
                                <?php }?>
                                
                                <?php if(Roles::check('setupmails.activated')){?>
                                <?= Components::checkboxInput($setup->is_active, $setup->id, url('backend/setup-mails/activated/').'/',  Helpers::checked($setup->is_active, true));?>
                                <?php }?>
                                
                            </td>
                            <?php }?>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                    <?= $allSetup->render();?>
                </div>
                </div>
            </div>
        </div>
    </div>
                
                 </div>

        </div>
    </div>



</div>
                
           

@stop()