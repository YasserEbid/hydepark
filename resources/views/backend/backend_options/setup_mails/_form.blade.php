<?php
if(!isset($setup))
    $setup = new \App\Models\MailBox;

use App\Http\Controllers\Common\Helpers;
?>
<form method="post" class="form-horizontal" id="form">
    
    <div class="form-group">
    <div class="col-sm-12">
        <label class="control-label">Choose type</label>
        <select name="type" class="select2clear form-control writedomain" required>
            <option selected disabled>Please choose</option>
            <option <?= Helpers::selectedPost('type', $setup->type, 'domain');?> value="domain">Mails domain</option>
            <option <?= Helpers::selectedPost('type', $setup->type, 'gmail');?> value="gmail">Gmail</option>
            <option <?= Helpers::selectedPost('type', $setup->type, 'yahoo');?> value="yahoo">Yahoo</option>
            <option <?= Helpers::selectedPost('type', $setup->type, 'outlook');?> value="outlook">Outlook</option>
        </select>
    </div>
        <script>
        $(document).ready(function(){
         <?php if($setup->type == 'domain'){?>   
            $('.domain').show();
         <?php }else{?>
            $('.domain').hide(); 
         <?php }?>
         
         $('.writedomain').on("change",function(){
            
                var val = $(this).val() ;
                if(val == 'domain')
                {
                    $('.domain').fadeIn("slow").show();
                }
                else
                {
                    $('.domain').fadeIn("slow").hide();
                }
                                    
            });
         
        });
        </script>
    </div>
    <div class="form-group domain">
        <div class="col-sm-12">
            <label class="control-label">Domain</label>
            <input type="url" name="domain" placeholder="Please enter your domain" class="form-control domain" value="<?= Helpers::issetPost('domain', $setup->domain);?>" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Email</label>
            <input type="email" name="email" placeholder="Please enter your email" class="form-control" value="<?= Helpers::issetPost('email', $setup->email);?>" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Password</label>
            <input type="password" name="password" placeholder="Please enter password" class="form-control">
        </div>
        </div>
    
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                
               type: {
                    required: true,
                },
                password: {
                    required: <?php if($setup->id >0){echo 'false';}else{echo 'true';}?>,
                    minlength: 5
                },
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                }
            }
        });
    });
</script>