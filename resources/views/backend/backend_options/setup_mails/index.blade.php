@extends('backend.layout')
@section('title','Setup mails')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Setup mails</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Setup mails</strong>
            </li>
        </ol>
    </div>
    <?php if(Roles::check('setupmails.create')){?>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?= url('backend/setup-mails/create');?>" class="btn btn-outline btn-primary">Create new mail</a>
        </div>
    </div>
    <?php }?>
    
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">
                    
                    <div class="col-md-3">
                        <div class="ibox">
                            <div class="ibox-content">
                                <a href="<?= url('backend/setup-mails/mails-domain'); ?>" class="btn-link"> 
                                    <div class="text-center">
                                        <img alt="image" class="img-circle m-t-xs img-responsive" src="<?= url('backend/images/image_mails/Maildomain.png'); ?>">
                                        <div class="m-t-xs font-bold">Mails domain</div>
                                    </div>

                                    <div class="clearfix"></div>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="ibox">
                            <div class="ibox-content">
                                <a href="<?= url('backend/setup-mails/gmail'); ?>" class="btn-link"> 
                                    <div class="text-center">
                                        <img alt="image" class="img-circle m-t-xs img-responsive" src="<?= url('backend/images/image_mails/Gmail.png'); ?>">
                                        <div class="m-t-xs font-bold">Gmail</div>
                                    </div>

                                    <div class="clearfix"></div>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="ibox">
                            <div class="ibox-content">
                                <a href="<?= url('backend/setup-mails/yahoo'); ?>" class="btn-link"> 
                                    <div class="text-center">
                                        <img alt="image" class="img-circle m-t-xs img-responsive" src="<?= url('backend/images/image_mails/Yahoo.png'); ?>">
                                        <div class="m-t-xs font-bold">Yahoo</div>
                                    </div>

                                    <div class="clearfix"></div>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="ibox">
                            <div class="ibox-content">
                                <a href="<?= url('backend/setup-mails/outlook'); ?>" class="btn-link"> 
                                    <div class="text-center">
                                        <img alt="image" class="img-circle m-t-xs img-responsive" src="<?= url('backend/images/image_mails/Outlook.png'); ?>">
                                        <div class="m-t-xs font-bold">Outlook</div>
                                    </div>

                                    <div class="clearfix"></div>
                                </a>

                            </div>
                        </div>
                    </div>
                    <p>To accesses : Allowing less secure apps to access your account</p>
                </div>

            </div>

        </div>
    </div>



</div>

           

@stop()