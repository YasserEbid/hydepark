@extends('backend.layout')
@section('title','Profile')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Profile</h2>
        <ol class="breadcrumb">
            <li data-action="home.index">
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php if(Roles::check('backendusers.index')){?>
            <li>
                <a href="<?= url('backend/backend-users');?>">Backend users</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Profile</strong>
            </li>
        </ol>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Profile data</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-5">
                            <dl class="dl-horizontal">

                                <dt>Name:</dt> <dd><?= $user->name;?></dd>
                                <dt>Email:</dt> <dd><?= $user->email;?></dd>
                                <dt>Phone:</dt> <dd><?= $user->phone;?></dd>
                                <dt>Role:</dt> <dd><?php if(!empty($user->role->name)) echo $user->role->name;?> </dd>
                                
                            </dl>
                        </div>
                        <div class="col-lg-7" id="cluster_info">
                            <dl class="dl-horizontal">

                                <dt>Last updated:</dt> <dd><?= $user->updatedAt(); ?></dd>
                                <dt>Created date:</dt> <dd><?= $user->createdAt(); ?></dd>
                                <dt>Created by:</dt> <dd><?= $user->admin->name; ?></dd>
                                
                            </dl>
                        </div>
                    </div>  
                    <div class="hr-line-dashed"></div>
                    
                    <form method="post" class="form-horizontal" id="form">

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label">Name</label>
                                <input type="text" name="name" placeholder="Please enter name" class="form-control" value="<?= Helpers::issetPost('name', $user->name); ?>" required >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label">Email</label>
                                <input type="email" name="email" placeholder="Please enter your email" class="form-control" value="<?= Helpers::issetPost('email', $user->email); ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label">Phone</label>
                                <input type="tel" name="phone" placeholder="Please enter phone" class="form-control" value="<?= Helpers::issetPost('phone', $user->phone); ?>" required >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label">Password</label>
                                <input type="password" name="password" placeholder="Please enter password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label">Confirm password</label>
                                <input type="password" name="c_password" placeholder="Please enter same password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="btn btn-default btn-file">
                                    <input type='hidden' name='image' value='<?= $user->image; ?>' />
                                    <?php
                                    if($user->image != '')
                                        $user->image = './uploads/backend_users/'.$user->image;
                                    else
                                        $user->image = './backend/images/avatar_image.png';
                                    ?>
                                    <?= Components::uploader($user->image);?>
                                </div>
                                <p class="help-block">Upload image only , Max size image 2 MB</p>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
                        </div>
                    </form>
                    <script>
                        $(document).ready(function () {

                            $("#form").validate({
                                rules: {
                                    name: {
                                        required: true,
                                        minlength: 3
                                    },
                                    password: {
                                        required: <?php if($user->id > 0){echo 'false';} else{ echo 'true';} ?>,
                                        minlength: 5
                                    },
                                    c_password: {
                                        minlength : 5,
                                        equalTo : '[name="password"]'
                                    },
                                    email: {
                                        required: true,
                                        email: true,
                                        minlength: 10
                                    },
                                    phone: {
                                    required: true,
                                    number: true
                                    },
                                    role_id: {
                                        required: true,
                                        number: true
                                    }
                                }
                            });
                        });
                    </script>


                </div>
            </div>
        </div>
    </div>
</div>

        </div>
    </div>



</div>

@stop()