<?php
use App\Http\Controllers\Common\Helpers;

if(!isset($user))
    $user = new \App\Models\BackendUsers;
?>
<form method="post" class="form-horizontal" id="form">
    
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Name</label>
            <input type="text" name="name" placeholder="Please enter name" class="form-control" value="<?= Helpers::issetPost('name', $user->name); ?>" required >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Email</label>
            <input type="email" name="email" placeholder="Please enter your email" class="form-control" value="<?= Helpers::issetPost('email', $user->email); ?>" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Phone</label>
            <input type="tel" name="phone" placeholder="Please enter phone" class="form-control" value="<?= Helpers::issetPost('phone', $user->phone); ?>" required >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Password</label>
            <input type="password" name="password" placeholder="Please enter password" class="form-control">
        </div>
        </div>
    <div class="form-group">
    <div class="col-sm-12">
        <label class="control-label">Confirm password</label>
        <input type="password" name="c_password" placeholder="Please enter same password" class="form-control">
    </div>
    </div>
    <div class="form-group">
    <div class="col-sm-12">
        <label class="control-label">Choose roles</label>
        <select name="role_id" class="select2clear form-control" required>
            <option selected disabled>Please choose</option>
            <?php foreach($roles as $role){?>
            <option <?= Helpers::selectedPost('role_id', $user->role_id, $role->id);?> value="<?= $role->id;?>"><?= $role->name;?></option>
            <?php }?>
        </select>
    </div>
    </div>
    <div class="form-group">
    <div class="col-sm-12">
        <div class="btn btn-default btn-file">
            <input type='hidden' name='image' value='<?= $user->image;?>' />
           <?php
           if($user->image != '')
               $user->image = './uploads/backend_users/'.$user->image ;
           else
               $user->image = './backend/images/avatar_image.png' ;
           ?>
           <?= \App\Http\Controllers\Common\Components::uploader($user->image) ?>
        </div>
         <p class="help-block">Upload image only , Max size image 2 MB</p>
    </div>
</div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                password: {
                    required: <?php if($user->id >0){echo 'false';}else{echo 'true';}?>,
                    minlength: 5
                },
                c_password: {
                    minlength : 5,
                    equalTo : '[name="password"]'
                },
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                phone: {
                required: true,
                number: true
                },
                role_id: {
                    required: true,
                    number: true
                }
            }
        });
    });
</script>