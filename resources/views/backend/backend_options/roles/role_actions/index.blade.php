@extends('backend.layout')
@section('title','Role actions | Roles')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Actions role</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <?php if(Roles::check('roles.index')){?>
            <li>
                <a href="<?= url('backend/roles');?>">Roles</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Role actions</strong>
            </li>
        </ol>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Role actions <span class="label label-primary pull-right font-bold"><?= $role->name;?></span></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                   <form method="post" class="form-horizontal">
                       <ul class="sortable-list connectList agile-list" id="todo">
                        <?php foreach($pages as $page){?>
                        <li class="success-element" id="task<?= $page->id;?>">
                            <p class="m-b-xs"><strong><?= $page->page?></strong></p>
                            <?php foreach($page->actions as $action){ 
                                    
                            $actionRole = $action->roleActions()->where('role_id' , $role->id)->first() ; 
                            if($actionRole)
                                $actionRole = $actionRole->action_id ;
                            else
                                $actionRole = 0 ;

                             ?> 
                            <div class="checkbox checkbox-success checkbox-inline">
                                <input type="checkbox" id="inlineCheckbox<?= $action->id;?>" name="actions[]" value="<?= $action->id;?>" <?= Helpers::checked($actionRole ,$action->id );?>>
                                <label for="inlineCheckbox<?= $action->id;?>"> <?= $action->name;?> </label>
                            </div>
                            <?php }?>
                            <div class="agile-detail">
                                <a target="_blank" href="<?php echo url($page->link); ?>" class="pull-right btn btn-xs btn-white">Link page</a>
                                <i class="fa fa-arrows-v"></i> Sotring page
                            </div>
                            <input type="hidden" name="sort[]" value="<?= $page->id; ?>" />
                        </li>
                        <?php }?>
                        </ul>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <input type="submit" name="save" class="btn btn-block btn-outline btn-primary" value="Save changes" />
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

        </div>
    </div>



</div>

@stop()