@extends('backend.layout')
@section('title','Mailbox')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Mailbox</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Mailbox</strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">
                    
                    <?php foreach($mails as $mail){?>

                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Messages <?= $mail->type;?></h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content ibox-heading">
                                <h4><i class="fa fa-envelope-o"></i> <?= $mail->email;?></h4>
                            </div>
                            <div class="ibox-content">
                                <div class="feed-activity-list">
                                    
                                    <?php 
                                    if($mail->type == 'domain'){$mail->setup($mail->type, $mail->domain, $mail->email, $mail->password , 'INBOX');}
                                    if($mail->type == 'gmail'){$mail->setup($mail->type, $mail->domain, $mail->email, $mail->password , 'INBOX');}
                                    if($mail->type == 'yahoo'){$mail->setup($mail->type, $mail->domain, $mail->email, $mail->password , 'INBOX');}
                                    if($mail->type == 'outlook'){$mail->setup($mail->type, $mail->domain, $mail->email, $mail->password , 'INBOX');}
                                    foreach($mail->getMails() as $email_id){
                                       $mailContent = $mail->header($email_id);
//                                       dd($mailContent);
                                    ?>
                                    <div class="feed-element">
                                        <a href="<?= $mail->url($email_id);?>">
                                            <div>
                                                <strong>From: <?= $mailContent->fromaddress;?></strong><br />
                                                <strong>Email: <?= $mail->mailName($email_id);?></strong>
                                                <div>Subject: <?php if(!empty($mailContent->subject)){echo $mailContent->subject;}?></div>
                                                <small class="text-muted"><i class="fa fa-clock-o"></i> <?= $mailContent->date;?></small>
                                            </div>
                                        </a>
                                    </div>
                                    
                                    <?php }?>
                                    
                                </div>
                                <a href="<?= url('backend/mailbox/mails/'.$mail->id);?>" class="btn btn-primary btn-block m-t"><i class="fa fa-inbox"></i> Show More</a>
                            </div>
                        </div>
                    </div>
                    
                    <?php } $mail->closeMail();?>

                </div>

            </div>

        </div>
    </div>



</div>



@stop()