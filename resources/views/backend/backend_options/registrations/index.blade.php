@extends('backend.layout')
@section('title','Registrations')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Registrations</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Registrations</strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">
                    
                     <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Registrations data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                    <?php if(Roles::check('registrations.export')){?>
                    <div class="btn-group">
                         <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Export <i class="fa fa-download"></i> <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="<?= url('backend/registrations/export');?>"><i class="fa fa-file-excel-o"></i> File excel </a></li>
                            <!--<li><a href="<?= url('backend/registrations/export');?>"><i class="fa fa-file-pdf-o"></i> File PDF </a></li>-->
                        </ul>
                    </div>
                    <?php }?>
                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover example1">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Message date</th>
                            <?php if(Roles::check('registrations.writenotes') || Roles::check('registrations.preview') || Roles::check('registrations.delete')){?>
                            <th>Settings</th>
                            <?php }?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($registrations as $registration){?>
                        <tr>
                            <td>{{$registration->name}}</td>
                            <td>{{$registration->phone_number}}</td>
                            <td>{{$registration->email}}</td>
                            <td>{{$registration->createdAt()}}</td>
                            <?php if(Roles::check('registrations.writenotes') || Roles::check('registrations.preview') || Roles::check('registrations.delete')){?>
                            <td>
                                <?php if(Roles::check('registrations.writenotes')){?>
                                <a href="<?= url(\URL::Current().'/write-notes/'.$registration->id);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-pencil"></i> Write notes</a>
                                <?php }?>
                                
                                <?php if(Roles::check('registrations.preview')){?>
                                <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal<?= $registration->id;?>"><i class="fa fa-twitch"></i> Preview</button>
                                <div class="modal inmodal" id="myModal<?= $registration->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-laptop modal-icon"></i>
                                                <h4 class="modal-title">Preview all data</h4>
                                            </div>
                                            <div class="modal-body">
                                                <blockquote>
                                                    <p>
                                                        <strong style="font-size: 18px;">Name</strong> : {{$registration->name}}
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Phone</strong> : {{$registration->phone_number}}
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Email</strong> : {{$registration->email}}
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Message</strong> : {{$registration->job_title}}
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Message date</strong> : {{$registration->createdAt()}}
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Note</strong> :
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">registrationed date</strong> : {{$registration->updatedAt()}}
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">registrationed by</strong> :
                                                    </p>
                                                </blockquote>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                            </td>
                            <?php }?>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                    <?= $registrations->render();?>
                </div>
                </div>
            </div>
        </div>
                    
                    

                </div>

            </div>

        </div>
    </div>



</div>



@stop()