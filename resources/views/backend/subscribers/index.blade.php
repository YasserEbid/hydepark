@extends('backend.layout')
@section('title','Subscribers')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Subscribers</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Subscribers</strong>
            </li>
        </ol>
    </div>
     <?php if(Roles::check('subscribers.sendemail')){?>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?= url('backend/subscribers/send-email');?>" class="btn btn-outline btn-primary">Send new email</a>
        </div>
    </div>
    <?php }?>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Subscribers data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                    <div class="">
            <a href="<?= url('backend/subscribers/delete-un-follower');?>" class="btn btn-danger">Delete unfollower</a>
            </div>
                
                    <div class="table-responsive">
                        
                <table class="table table-striped table-bordered table-hover example2">
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Date subscribe</th>
                            <th>States</th>
                            <?php if(Roles::check('subscribers.delete') || Roles::check('subscribers.activated')){?>
                            <th>Settings</th>
                            <?php }?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($subscribers as $subscribe){?>
                        <tr>
                            <td><?= $subscribe->email;?></td>
                            <td><?= $subscribe->createdAt();?></td>
                            <td><?php if($subscribe->follower == true){?><span class="label label-primary font-bold">Follower</span><?php }else{?><span class="label label-danger font-bold">Unfollow</span><?php }?> </td>
                            <?php if(Roles::check('subscribers.delete') || Roles::check('subscribers.activated')){?>
                            <td>
                                
                                <?php if(Roles::check('subscribers.delete')){?>
                                <?= Components::deleteRow($subscribe->id, url(\URL::Current().'/delete'));?>
                                <?php }?>
                                
                                <?php if(Roles::check('subscribers.activated')){?>
                                <?= Components::checkboxInput($subscribe->is_active, $subscribe->id, url(\URL::Current().'/activated/'),  Helpers::checked($subscribe->follower, true));?>
                                <?php }?>
                                
                            </td>
                            <?php }?>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                    <?= $subscribers->render();?>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

        </div>
    </div>



</div>

@stop()