@extends('backend.layout')
@section('title','Send email | Subscribers')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Helpers;
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Subscribers</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <?php if(Roles::check('subscribers.index')){?>
            <li>
                <a href="<?= url('backend/subscribers');?>">Subscribers</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Send new email</strong>
            </li>
        </ol>
    </div>
    
</div>    

<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Send mail data</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <form method="post" class="form-horizontal">
                        
                        <div class="form-group">
                        <div class="col-sm-12">
                            <label class="control-label">Choose themes</label>
                            <select name="themes" class="select2clear form-control" required>
                                <option selected disabled>Please choose themes</option>
                                <?php foreach($themes as $theme){?>
                                <option <?= Helpers::selectedPost('themes', $theme->id,$theme->id);?> value="<?= $theme->id;?>"><?= $theme->name;?></option>
                                <?php }?>
                            </select>
                        </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label">Subject</label>
                                <input type="text" name="subject" placeholder="Please enter subject" class="form-control" value="<?php if(isset($_POST['subject'])){echo $_POST['subject'];}?>" required >
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="control-label">Message</label>
                                
                                <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_css();?>
                                <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_editor('message');?>
                                <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_js();?>
                                
                            </div>
                        </div>
                       
                       
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <input type="submit" name="send" class="btn btn-outline btn-primary" value="Send" />
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>

        </div>
    </div>



</div>

@stop()