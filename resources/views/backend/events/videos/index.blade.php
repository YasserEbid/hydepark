@extends('backend.layout')
@section('title','Videos')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Videos data</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Videos data</strong>
            </li>
        </ol>
    </div>
    <?php if(Roles::check('events.index')){?>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?= url('backend/events/create-videos');?>" class="btn btn-outline btn-primary">Create new video</a>
        </div>
    </div>
    <?php }?>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">
                    
                     <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Videos data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2">Sorting</button>
                    <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content animated flipInY">
                                <form action="" method="post">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title">Sorting</h4>
                                    <small class="font-bold"><i class="fa fa-hand-o-up"></i> Drag videos between list</small>
                                </div>
                                <div class="modal-body">

                                    <ul class="sortable-list connectList agile-list" id="todo">
                                        <?php foreach($sorting as $sort){?>
                                        <li class="success-element col-sm-3" id="task<?= $sort->id; ?>">
                                            <div class="agile-detail">
                                                <img src="<?php echo url($sort->video_image); ?>" width="100" height="100" />
                                            </div>
                                            <input type="hidden" name="sort[]" value="<?= $sort->id; ?>" />
                                        </li>
                                        <?php }?>

                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                    <button type="submit" name="save" class="btn btn-primary">Save changes</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover example1">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Date</th>
                            <?php if(Roles::check('events.preview') || Roles::check('events.edit') || Roles::check('events.delete')){?>
                            <th>Settings</th>
                            <?php }?>
                        </tr>
                    </thead>
                    <tbody>
                       <?php foreach($videos as $event){?> 
                        <tr>
                            <td><?= $event->name;?></td>
                            <td><img src="<?php echo url($event->video_image); ?>" width="100" height="100" /></td>
                            <td><?php if($event->date !='0000-00-00' ) echo date('M/Y',strtotime($event->date));?></td>
                            <?php if(Roles::check('events.previewvideos') || Roles::check('events.editvideos') || Roles::check('events.deletevideos')){?>
                            <td>
                                
                                <?php if(Roles::check('events.editvideos')){?>
                                <a href="<?= url('backend/events/edit-videos/'.$event->id);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-paste"></i> Edit</a>
                                <?php }?>
                                
                                <?php if(Roles::check('events.previewvideos')){?>
                                <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal<?= $event->id;?>"><i class="fa fa-twitch"></i> Preview</button>
                                <div class="modal inmodal" id="myModal<?= $event->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-laptop modal-icon"></i>
                                                <h4 class="modal-title">Preview all data</h4>
                                            </div>
                                            <div class="modal-body">
                                                <blockquote>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Name</strong> : <?= $event->name; ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Date</strong> : <?php if($event->date !='0000-00-00' ) echo date('M/Y',strtotime($event->date));?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Created date</strong> : <?= $event->createdAt(); ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Updated date</strong> : <?= $event->updatedAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created by</strong> : <?php if(is_object($event->admin)){echo $event->admin->name;} ?>
                                                    </p>
                                                </blockquote>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                
                                <?php if(Roles::check('events.deletevideos')){?>
                                <?= Components::deleteRow($event->id, url('backend/events/delete-videos'));?>
                                <?php }?>
                                
                            </td>
                            <?php }?>
                        </tr>
                       <?php }?>
                    </tbody>
                </table>
                        <?= $videos->render();?>
                </div>
                </div>
            </div>
        </div>
                    
                </div>

            </div>

        </div>
    </div>



</div>



@stop()