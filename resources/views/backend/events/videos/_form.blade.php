<?php

use App\Http\Controllers\Common\Helpers;

if(!isset($event))
    $event = new \App\Models\EventVideos;
?>
<form method="post" class="form-horizontal" id="form">

  <div class="form-group" id="data_2">
    <div class="col-sm-6">
      <label class="control-label">Name</label>
      <input type="text" name="name" placeholder="Please enter name" class="form-control" value="<?= Helpers::issetPost('name', $event->name); ?>">
    </div>
    <div class="col-sm-6">
      <label class="control-label">Video link</label>
      <input type="text" name="video_link" placeholder="Please enter video link" class="form-control" value="<?= Helpers::issetPost('video_link', $event->video_link); ?>" required >
    </div>
    <div class="col-sm-6">
    <label class="control-label">Date</label>
    <div class="input-group date">
      <span class="input-group-addon">
        <i class="fa fa-calendar"></i></span>
      <input type="text" name="date" class="form-control" placeholder="Please enter date" value="<?= Helpers::issetPost('date', $event->date); ?>">
    </div>
    </div>
  </div>

  <div class="hr-line-dashed"></div>
  <div class="form-group">
    <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
  </div>
</form>
<script>
    $(document).ready(function () {

      $("#form").validate({
        rules: {
          video_link: {
            required: true,
          }
        }
      });
    });
</script>