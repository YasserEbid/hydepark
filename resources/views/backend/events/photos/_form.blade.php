<?php

use App\Http\Controllers\Common\Helpers;

if(!isset($event))
    $event = new \App\Models\EventPhotos;
?>
<form method="post" class="form-horizontal" id="form">

  <div class="form-group">
    <div class="col-sm-6">
      <label class="control-label">Name</label>
      <input type="text" name="name" placeholder="Please enter name" class="form-control" value="<?= Helpers::issetPost('name', $event->name); ?>" >
    </div>
    <div class="col-sm-6">
        <div class="btn btn-default btn-file">
            <input type='hidden' name='image' value='<?= $event->image;?>' />
           <?php
           if($event->image != '')
               $event->image = './uploads/events/'.$event->image ;
           else
               $event->image = './backend/images/avatar_image.png' ;
           ?>
           <?= \App\Http\Controllers\Common\Components::uploader($event->image) ?>
        </div>
         <p class="help-block">Upload image only , Max size image 2 MB</p>
    </div>
  </div>

  <div class="hr-line-dashed"></div>
  <div class="form-group">
    <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
  </div>
</form>
<script>
    $(document).ready(function () {

      $("#form").validate({
        rules: {
//          name: {
//            required: true,
//            minlength: 3
//          }
        }
      });
    });
</script>