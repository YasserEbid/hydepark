<?php
use App\Http\Controllers\Common\Helpers;

if(!isset($theme))
    $theme = new \App\Models\EmailThemes;
?>
<form method="post" class="form-horizontal" id="form">
    
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Name theme</label>
            <input type="text" name="name" placeholder="Please enter name theme" class="form-control" value="<?= Helpers::issetPost('name', $theme->name); ?>" required >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Header</label>
            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_css();?>
            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_editor('header','header',$theme->header);?>
            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_js();?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Footer</label>
            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_css();?>
            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_editor('footer','footer',$theme->footer);?>
            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_js();?>
        </div>
    </div>

    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                header: {
                    required: true,
                    minlength: 3
                },
                footer: {
                    required: true,
                    minlength: 3
                }
            }
        });
    });
</script>