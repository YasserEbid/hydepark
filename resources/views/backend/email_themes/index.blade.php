@extends('backend.layout')
@section('title','Email themes')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Email themes</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Email themes</strong>
            </li>
        </ol>
    </div>
     <?php if(Roles::check('emailthemes.create')){?>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?= url('backend/email-themes/create');?>" class="btn btn-outline btn-primary">Create new theme</a>
        </div>
    </div>
    <?php }?>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Email themes data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover example2">
                    <thead>
                        <tr>
                            <th>Name theme</th>
                            <?php if(Roles::check('emailthemes.edit') || Roles::check('emailthemes.preview') || Roles::check('emailthemes.delete')){?>
                            <th>Settings</th>
                            <?php }?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($themes as $row){?>
                        <tr>
                            <td><?= $row->name;?></td>
                            <?php if(Roles::check('emailthemes.edit') || Roles::check('emailthemes.preview') || Roles::check('emailthemes.delete')){?>
                            <td>
                                <?php if(Roles::check('emailthemes.edit')){?>
                                <a href="<?= url(\URL::Current().'/edit/'.$row->id);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-paste"></i> Edit</a>
                                <?php }?>
                                
                                <?php if(Roles::check('emailthemes.preview')){?>
                                <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal<?= $row->id;?>"><i class="fa fa-twitch"></i> Preview</button>
                                <div class="modal inmodal" id="myModal<?= $row->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-laptop modal-icon"></i>
                                                <h4 class="modal-title">Preview all data</h4>
                                            </div>
                                            <div class="modal-body">
                                                <blockquote>
                                                    <p>
                                                        <strong style="font-size: 18px;">Name theme</strong> : <?= $row->name; ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created date</strong> : <?= $row->createdAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Updated date</strong> : <?= $row->updatedAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created by</strong> : <?= $row->admin->name; ?>
                                                    </p>
                                                </blockquote>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                
                                <?php if(Roles::check('emailthemes.delete')){?>
                                <?= Components::deleteRow($row->id, url(\URL::Current().'/delete'));?>
                                <?php }?>
                                
                            </td>
                            <?php }?>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                    <?= $themes->render();?>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

        </div>
    </div>



</div>

@stop()