@extends('backend.layout')
@section('title','Edit data | About us')
@section('content')
@include('backend.message')
<?php
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Helpers;
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>About us</h2>
        <ol class="breadcrumb">
                <?php if(Roles::check('home.index')){ ?>
                <li>
                    <a href="<?= url('backend/home'); ?>">Home page</a>
                </li>
                <?php } ?>
                <?php if(Roles::check('aboutus.index')){ ?>
                <li>
                    <a href="<?= url('backend/about-us'); ?>">About us</a>
                </li>
                <?php } ?>
            <li class="active">
                <strong>Edit data</strong>
            </li>
        </ol>
    </div>

</div>    

<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>About us data</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <form method="post" class="form-horizontal" id="form">
                                    <?php if($about->video_link !=''){?>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label">Video link</label>
                                            <input type="text" name="video_link" placeholder="Please enter video link" class="form-control" value="<?= Helpers::issetPost('video_link', $about->video_link); ?>" required >
                                        </div>
                                    </div>
                                    <?php }?>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label">Description</label>
                                            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_css(); ?>
                                            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_editor('description', 'description', $about->description); ?>
                                            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_js(); ?>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
                                    </div>
                                </form>
                                <script>
                                    $(document).ready(function () {

                                        $("#form").validate({
                                            rules: {
                                                <?php if($about->video_link !=''){?>
                                                video_link: {
                                                    required: true
                                                },
                                                <?php }?>
                                                description: {
                                                    required: true
                                                }
                                            }
                                        });
                                    });
                                </script>  

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



</div>

@stop()