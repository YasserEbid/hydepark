<?php
use App\Http\Controllers\Common\Helpers;

if(!isset($slider))
    $slider = new \App\Models\Sliders;
?>
<form method="post" class="form-horizontal" id="form">
    
    <div class="form-group">
    <div class="col-sm-12">
        <div class="btn btn-default btn-file">
            <input type='hidden' name='image' value='<?= $slider->image;?>' />
           <?php
           if($slider->image != '')
               $slider->image = './uploads/'.$slider->image ;
           else
               $slider->image = './backend/images/avatar_image.png' ;
           ?>
           <?= \App\Http\Controllers\Common\Components::uploader($slider->image) ?>
        </div>
         <p class="help-block">Upload image only , Max size image 2 MB</p>
    </div>
</div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {

                image: {
                    required: true
                }
            }
        });
    });
</script>