@extends('backend.layout')
@section('title','Press release')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Press release</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Press release</strong>
            </li>
        </ol>
    </div>
    <?php if(Roles::check('press.index')){?>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?= \URL::current().'/create';?>" class="btn btn-outline btn-primary">Create new press release</a>
        </div>
    </div>
    <?php }?>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                
                <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Press release data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalSort">Sorting</button>
                    <div class="modal inmodal" id="myModalSort" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content animated flipInY">
                                <form action="" method="post">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title">Sorting</h4>
                                    <small class="font-bold"><i class="fa fa-hand-o-up"></i> Drag videos between list</small>
                                </div>
                                <div class="modal-body">

                                    <ul class="sortable-list connectList agile-list" id="todo">
                                        <?php foreach($sorting as $sort){?>
                                        <li class="success-element col-sm-3" id="task<?= $sort->id; ?>">
                                            <div class="agile-detail">
                                                <?= $sort->title;?>
                                            </div>
                                            <input type="hidden" name="sort[]" value="<?= $sort->id; ?>" />
                                        </li>
                                        <?php }?>

                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                    <button type="submit" name="save" class="btn btn-primary">Save changes</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover example3">
                    <thead>
                        <tr>
                            <th>Language</th>
                            <th>Title</th>
                            <th>File</th>
                            <th>Date</th>
                            <?php if(Roles::check('press.edit') || Roles::check('press.preview') || Roles::check('press.delete')){?>
                            <th>Settings</th>
                            <?php }?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($result as $row){?>
                        <tr>
                            <td><?= $row->language;?></td>
                            <td><?= $row->title;?></td>
                            <td><a href="<?= url('uploads/'.$row->file);?>" target="_blank">View</a></td>
                            <td><?php if($row->date !='0000-00-00' ) echo date('M/Y',strtotime($row->date));?></td>
                            <?php if(Roles::check('press.edit') || Roles::check('press.preview') || Roles::check('press.delete')){?>
                            <td>
                                
                                <?php if(Roles::check('press.edit')){?>
                                <a href="<?= url('backend/press/edit/'.$row->id);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-paste"></i> Edit</a>
                                <?php }?>
                                
                                <?php if(Roles::check('press.preview')){?>
                                <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal<?= $row->id;?>"><i class="fa fa-twitch"></i> Preview</button>
                                <div class="modal inmodal" id="myModal<?= $row->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-laptop modal-icon"></i>
                                                <h4 class="modal-title">Preview all data</h4>
                                            </div>
                                            <div class="modal-body">
                                                <blockquote>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Language</strong> : <?= $row->language;?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Title</strong> : <?= $row->title; ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Date</strong> : <?php if($row->date !='0000-00-00' ) echo date('M/Y',strtotime($row->date));?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Created date</strong> : <?= $row->createdAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Updated date</strong> : <?= $row->updatedAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created by</strong> : <?= $row->admin->name; ?>
                                                    </p>
                                                </blockquote>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                
                                <?php if(Roles::check('press.delete')){?>
                                <?= Components::deleteRow($row->id, url('backend/press/delete'));?>
                                <?php }?>
                                
                            </td>
                            <?php }?>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                    <?= $result->render();?>
                </div>
                </div>
            </div>
        </div>
    </div>
                
                 </div>

        </div>
    </div>



</div>
                
           

@stop()