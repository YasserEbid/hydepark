<?php

use App\Http\Controllers\Common\Helpers;

if(!isset($object))
    $object = new \App\Models\Presskit;
?>
<form method="post" class="form-horizontal" id="form">

  <div class="form-group" id="data_2">
      <div class="form-group">
    <div class="col-sm-12">
        <label class="control-label">Choose language</label>
        <select name="language" class="select2clear form-control" required>
            <option selected disabled>Please choose</option>
            <option <?= Helpers::selectedPost('language', $object->language, 'arabic');?> value="arabic">Arabic</option>
            <option <?= Helpers::selectedPost('language', $object->language, 'english');?> value="english">English</option>
        </select>
    </div>
    </div>
    <div class="col-sm-6">
      <label class="control-label">Title</label>
      <input type="text" name="title" placeholder="Please enter title" class="form-control" value="<?= Helpers::issetPost('title', $object->title); ?>" required >
    </div>
    <div class="col-sm-6">
      <label class="control-label">Date</label>
      <div class="input-group date">
        <span class="input-group-addon">
          <i class="fa fa-calendar"></i></span>
        <input type="text" name="date" class="form-control" placeholder="Please enter date" value="<?= Helpers::issetPost('date', $object->date); ?>">
      </div>
    </div>
  </div>

  <div class="col-sm-12">
    <div class="form-group">
      <div class="btn btn-default btn-file">
        <input type='hidden' name='file' value='<?= $object->file; ?>' />
        <?php
        if($object->file != '')
            $object->file = './uploads/' . $object->file;
        else
            $object->file = './backend/images/avatar_image.png';
        ?>
<?= \App\Http\Controllers\Common\Components::uploaderFile($object->file) ?>
      </div>
      <p class="help-block">Upload file </p>
    </div>
  </div>

  <div class="hr-line-dashed"></div>
  <div class="form-group">
    <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
  </div>
</form>
<script>
    $(document).ready(function () {

      $("#form").validate({
        rules: {
          title: {
            required: true
          },
          date: {
            required: true
          },
          file: {
            required: true
          }
        }
      });
    });
</script>