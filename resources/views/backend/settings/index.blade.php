@extends('backend.layout')
@section('title','Settings')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;

?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Settings</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Settings</strong>
            </li>
        </ol>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Settings data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                    
<form method="post" class="form-horizontal" id="form">
    
    <?php foreach($settings as $value){?>
    
    <?php if($value->type == 1){?>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label"><?php $string = str_replace('_', ' ', $value->key); echo ucwords($string);?></label>
            <input type="text" name="value[<?= $value->id;?>]" placeholder="Please enter <?= $value->key;?>" class="form-control" value="<?= $value->value;?>" />
        </div>
    </div>
    <?php }?>
    <?php if($value->type == 2){?>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label"><?php $string = str_replace('_', ' ', $value->key); echo ucwords($string);?></label>
            <input type="checkbox" name="value[<?= $value->id;?>]" required value="<?= $value->value;?>" checked />
        </div>
    </div>
    <?php }?>
    <?php if($value->type == 3){?>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label"><?php $string = str_replace('_', ' ', $value->key); echo ucwords($string);?></label>
            <textarea name="value[<?= $value->id;?>]" class="form-control" rows="3" placeholder="Enter <?= $value->value;?>"><?= $value->value;?></textarea>
        </div>
    </div>
    <?php }?>
    <?php if($value->type == 4){?>
    <div class="form-group">
    <div class="col-sm-12">
        <div class="btn btn-default btn-file">
            <input type='hidden' name='image' value='<?= $value->image;?>' />
           <?php
           if($value->image != '')
               $value->image = './uploads/'.$value->image ;
           else
               $value->image = './backend/images/avatar_image.png' ;
           ?>
           <?= \App\Http\Controllers\Common\Components::uploader($value->image) ?>
        </div>
         <p class="help-block">Upload image only , Max size image 2 MB</p>
    </div>
    </div>
    <?php }?>
    
    <?php }?>
    
    <?php if(Roles::check('settings.edit')){?>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
    </div>
    <?php }?>
    
</form>
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>

        </div>
    </div>



</div>

@stop()