<!DOCTYPE html>
<html>
    <head>
        <base href='<?= URL::to('./') ?>' />
        <script>var base = '<?= URL::to('./') ?>';</script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="fav.png"/>
        <meta name="author" content="Development: Tarek Magdy"/>
        <title>Lockscreen | New Version</title>
        <link href="./backend/css/bootstrap.min.css" rel="stylesheet">
        <link href="./backend/font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="./backend/css/animate.css" rel="stylesheet">
        <link href="./backend/css/style.css" rel="stylesheet">

    </head>
    <body class="gray-bg">
<?php
   if(session('error')!='')
       $error = session('error') ;
?>
        <div class="lock-word animated fadeInDown">
            <span class="first-word">LOCKED</span><span>SCREEN</span>
        </div>
        <div class="middle-box text-center lockscreen animated fadeInDown">
            <div>
                <div class="m-b-md">
                    <img alt="image" class="img-circle circle-border" src="<?php echo url('uploads/backend_users/'.$_SESSION['image']); ?>">
                </div>
                <h3><?= $_SESSION['name'];?></h3>
                <p><?php if(isset($error))echo $error;?></p>
                <a href="<?php echo url('backend/home');?>" class="btn btn-primary block full-width">Back to home page</a>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="./backend/js/jquery-2.1.1.js"></script>
        <script src="./backend/js/bootstrap.min.js"></script>

    </body>
</html>
