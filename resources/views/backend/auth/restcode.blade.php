<!DOCTYPE html>
<html>
    <head>
        <base href='<?= URL::to('./') ?>' />
        <script>var base = '<?= URL::to('./') ?>';</script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="fav.png"/>
        <meta name="author" content="Development: Tarek Magdy"/>
        <title>Rest code | New Version</title>
        <link href="./backend/css/bootstrap.min.css" rel="stylesheet">
        <link href="./backend/font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="./backend/css/animate.css" rel="stylesheet">
        <link href="./backend/css/style.css" rel="stylesheet">

    </head>
    <body class="gray-bg">

        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div>

                    <h1 class="logo-name">CMS</h1>

                </div>
                <h3>Welcome to CMS</h3>
                <p>Enter code</p>
                <?php if(!empty($error)){?>
       
                    <div class="alert alert-danger alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <?php echo $error;?>
                    </div>

                <?php }?>
                <?php if(!empty($warning)){?>

                    <div class="alert alert-warning alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <?php echo $warning;?>
                    </div>

                <?php }?>
                <form class="m-t" role="form" action="" method="post">
                    <div class="form-group">
                        <input type="text" name="code" class="form-control" placeholder="Enter code" required="">
                    </div>
                    <button type="submit" name="check" class="btn btn-primary block full-width m-b">Check</button>

                </form>
                <p class="m-t">Power By <strong><a target="_blank" href="http://www.media-sci.com">Mediasci</a></strong> </p>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="./backend/js/jquery-2.1.1.js"></script>
        <script src="./backend/js/bootstrap.min.js"></script>

    </body>
</html>
