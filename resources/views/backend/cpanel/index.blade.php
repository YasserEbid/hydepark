@extends('backend.layout')
@section('title','Cpanel')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Cpanel</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Cpanel</strong>
            </li>
        </ol>
    </div>
    <?php if(Roles::check('cpanel.createcpanel')){?>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?= url('backend/cpanel/create-cpanel');?>" class="btn btn-outline btn-primary">Add new cpanel</a>
        </div>
    </div>
    <?php }?>
    
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Cpanel data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover example2">
                    <thead>
                        <tr>
                            <th>IP</th>
                            <th>User</th>
                            <th>Domain</th>
                            <th>Port</th>
                            <th>States</th>
                            <?php if(Roles::check('cpanel.editcpanel') || Roles::check('cpanel.previewcpanel') || Roles::check('cpanel.deletecpanel')){?>
                            <th>Settings</th>
                            <?php }?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($cpanels as $cpanel){?>
                        <tr>
                            <td><?= $cpanel->ip;?></td>
                            <td><?= $cpanel->username;?></td>
                            <td><?= $cpanel->domain;?></td>
                            <td><?= $cpanel->port;?></td>
                            <td><?php if($cpanel->checkCpanelAuthentication($cpanel) == true){?><span class="label label-primary font-bold">It's working</span><?php }else{?><span class="label label-danger font-bold">It's not working</span><?php }?> </td>
                            <?php if(Roles::check('cpanel.editcpanel') || Roles::check('cpanel.previewcpanel') || Roles::check('cpanel.deletecpanel')){?>
                            <td>
                                <?php if(Roles::check('cpanel.editcpanel')){?>
                                <a href="<?= url(\URL::Current().'/edit-cpanel/'.$cpanel->id);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-paste"></i> Edit</a>
                                <?php }?>
                                
                                <?php if(Roles::check('cpanel.previewcpanel')){?>
                                <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal<?= $cpanel->id;?>"><i class="fa fa-twitch"></i> Preview</button>
                                <div class="modal inmodal" id="myModal<?= $cpanel->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-laptop modal-icon"></i>
                                                <h4 class="modal-title">Preview all data</h4>
                                            </div>
                                            <div class="modal-body">
                                                <blockquote>
                                                    <p>
                                                        <strong style="font-size: 18px;">IP</strong> : <?= $cpanel->ip; ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Username</strong> : <?= $cpanel->username; ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Domain</strong> : <?= $cpanel->domain; ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Port</strong> : <?= $cpanel->port; ?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">States</strong> : <?php if($cpanel->checkCpanelAuthentication($cpanel) == true){?><span class="label label-primary font-bold">It's working</span><?php }else{?><span class="label label-danger font-bold">It's not working</span><?php }?> 
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created date</strong> : <?= $cpanel->createdAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Updated date</strong> : <?= $cpanel->updatedAt(); ?>
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Created by</strong> : <?= $cpanel->admin->name; ?>
                                                    </p>
                                                </blockquote>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                
                                <?php if(Roles::check('cpanel.deletecpanel')){?>
                                <?= Components::deleteRow($cpanel->id, url(\URL::Current().'/delete-cpanel'));?>
                                <?php }?>
                                
                            </td>
                            <?php }?>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                    <?= $cpanels->render();?>
                </div>
                </div>
            </div>
        </div>
    </div>
                
            </div>

        </div>
    </div>



</div>

           

@stop()