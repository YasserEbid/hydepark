<?php
use App\Http\Controllers\Common\Helpers;

if(!isset($cpanel))
    $cpanel = new \App\Models\Cpanel;
?>
<form method="post" class="form-horizontal" id="form">
    
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">IP, to find ip your website <a target="_blank" href="https://www.site24x7.com/find-ip-address-of-web-site.html">click here</a></label>
            <input type="text" name="ip" placeholder="Please enter ip" class="form-control" value="<?= Helpers::issetPost('ip',$cpanel->ip);?>" required >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Domain</label>
            <input type="text" name="domain" placeholder="Please enter domain your website like: mydomain.com" class="form-control" value="<?= Helpers::issetPost('domain',$cpanel->domain);?>" required >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Username</label>
            <input type="text" name="username" placeholder="Please enter username" class="form-control" value="<?= Helpers::issetPost('username',$cpanel->username);?>" required >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Password</label>
            <input type="password" name="password" placeholder="Please enter password" class="form-control" value="<?= Helpers::issetPost('password',$cpanel->password);?>" required >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">port</label>
            <input type="text" name="port" placeholder="Please enter port" class="form-control" value="<?= Helpers::issetPost('port',$cpanel->port);?>" required >
        </div>
    </div>
    
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                ip: {
                    required: true
                },
                domain: {
                    required: true
                },
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                port: {
                    required: true
                }
            }
        });
    });
</script>