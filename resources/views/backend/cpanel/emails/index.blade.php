@extends('backend.layout')
@section('title','Emails | Cpanel')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Cpanel / Emails</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <?php if(Roles::check('cpanel.index')){?>
            <li>
                <a href="<?= url('backend/cpanel');?>">Cpanel</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Emails</strong>
            </li>
        </ol>
    </div>
    <?php if(Roles::check('cpanel.createemail')){?>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="<?= url('backend/cpanel/create-email');?>" class="btn btn-outline btn-primary">Create new mail</a>
        </div>
    </div>
    <?php }?>
    
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Emails data</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="m-b-lg">
                                    <form action="" method="get">
                                    <div class="input-group">
                                        <select name="cpanel_id" class="form-control">
                                            <option selected="" disabled="" value="">Chooose account</option>
                                            <?php foreach($cpanels as $cpanel){?>
                                            <option <?= Helpers::selectedGet('account_id','', $cpanel->id)?> value="<?= $cpanel->id;?>"><?= $cpanel->domain;?></option>
                                            <?php }?>
                                        </select>
                                        <span class="input-group-btn">
                                            <button type="submit" name="find" class="btn btn-white"> Search</button>
                                        </span>
                                    </div>
                                    </form>
                                    
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-hover issue-tracker">
                                        <tbody>
                                            <?php foreach($emails as $email){?>
                                            <tr>
                                                <td><?= $email->email;?></td>
                                                <td>Used qouta <?= $email->humandiskused;?></td>
                                                <td>Total qouta <?= $email->diskquota;?> MB</td>
                                                <!--<td></td>-->
                                                <td class="text-right">
                                                <?php if(Roles::check('cpanel.changepasswordemail')){?>
                                                <a href="<?= url('backend/cpanel/change-password-email/'.$cpanel->id.'/'.$email->user);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-paste"></i> Change password</a>
                                                <?php }?>
                                                <?php if(Roles::check('cpanel.editqouta')){?>
                                                <a href="<?= url('backend/cpanel/edit-qouta/'.$cpanel->id.'/'.$email->user);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-paste"></i> Edit qouta</a>
                                                <?php }?>
                                                <?php if(Roles::check('cpanel.deleteemail')){?>
                                                <?= Components::deleteRow($email->user, url('backend/cpanel/delete-email/'.$_GET['cpanel_id']));?>
                                                <?php }?>
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>



</div>

           

@stop()