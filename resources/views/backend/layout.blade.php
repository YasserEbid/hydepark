<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
        <base href='<?= URL::to('/public/') ?>' />
        <script>var base = '<?= URL::to('/public/') ?>';</script>
        
        <link rel="shortcut icon" href="fav.png"/>
        <meta name="author" content="Development: Tarek Magdy"/>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>@yield('title') | Hyde Park</title>
        <link href="./backend/css/bootstrap.min.css" rel="stylesheet">
        <link href="./backend/font-awesome/css/font-awesome.css" rel="stylesheet">
        <!-- Jquery 2.1.1 -->
        <script src="./backend/js/jquery-2.1.1.js"></script>
        <!-- Toastr style -->
        <link href="./backend/css/plugins/toastr/toastr.min.css" rel="stylesheet">
        <!-- Datatable -->
        <link href="./backend/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
        <!-- Sweet Alert -->
        <link href="./backend/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
        <!-- blueimp gallery -->
        <link href="./backend/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
        <!-- Select2 -->
        <link href="./backend/css/plugins/select2/select2.min.css" rel="stylesheet">
        
        <link href="./backend/css/plugins/iCheck/custom.css" rel="stylesheet">

        <link href="./backend/css/plugins/chosen/chosen.css" rel="stylesheet">

        <link href="./backend/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">

        <link href="./backend/css/plugins/cropper/cropper.min.css" rel="stylesheet">

        <link href="./backend/css/plugins/switchery/switchery.css" rel="stylesheet">

        <link href="./backend/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

        <link href="./backend/css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">

        <link href="./backend/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

        <link href="./backend/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
        <link href="./backend/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">

        <link href="./backend/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

        <link href="./backend/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

        <link href="./backend/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

        <link href="./backend/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    
        <link href="./backend/css/animate.css" rel="stylesheet">
        <link href="./backend/css/style.css" rel="stylesheet">
        <link href="./backend/css/plugins/codemirror/codemirror.css" rel="stylesheet">
        <link href="./backend/css/plugins/codemirror/ambiance.css" rel="stylesheet">
        <!-- Bootstrap Tour -->
        <link href="./backend/css/plugins/bootstrapTour/bootstrap-tour.min.css" rel="stylesheet">
        

    </head>    
    <body class="fixed-sidebar no-skin-config full-height-layout">
    <?php 
    use App\Http\Controllers\Backend\Roles\Roles; 
    use App\Http\Controllers\Common\Helpers;
    ?>

        <div id="wrapper">

            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element"> <span>
                                    <img class="img-circle" style="height: 80px;" src="<?php echo url('uploads/backend_users/'.$_SESSION['image']); ?>" />
                                </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?= $_SESSION['name'];?></strong>
                                        </span> <span class="text-muted text-xs block"><?= $_SESSION['role_name'];?> <b class="caret"></b></span> </span> </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a href="<?php echo url('/backend/backend-users/profile'); ?>">Profile</a></li>
                                    <?php if(Roles::check('contacts.index')){?>
                                    <li><a href="<?php echo url('/backend/contacts'); ?>">Contacts</a></li>
                                    <?php }?>
                                    <?php if(Roles::check('mailbox.index')){?>
                                    <li><a href="<?php echo url('/backend/mailbox'); ?>">Mailbox</a></li>
                                    <?php }?>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo url('/backend/auth/logout'); ?>">Sign out</a></li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                CMS
                            </div>
                        </li>
                        <li <?php if(Request::segment(2) == "home"){?> class="active" <?php }?>>
                            <a href="<?= url('backend/home');?>"><i class="fa fa-home"></i> <span class="nav-label">Home page</span></a>
                        </li>
                        
                        <?php if(Roles::check('sliders.index')){?>
                        <li <?php if(Request::segment(2) == "sliders"){?> class="active" <?php }?>>
                            <a href="<?= url('backend/sliders');?>"><i class="fa fa-image"></i> <span class="nav-label">Sliders</span></a>
                        </li>
                        <?php }?>
                        
                        <?php if(Roles::check('homesections.index') || Roles::check('homesections.two')){?>
                        <li>
                            <a href="#"><i class="fa fa-pencil"></i> <span class="nav-label">Home sections</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse <?php if(Request::segment(2) == "home-sections"){?> in <?php }?>">
                                <?php if(Roles::check('homesections.index')){?>
                                <li <?php if(Request::segment(2) == "home-sections" && Request::segment(3) == ""){?> class="active" <?php }?>><a href="<?= url('backend/home-sections');?>">Section one</a></li>
                                <?php }?>
                                <?php if(Roles::check('homesections.two')){?>
                                <li <?php if(Request::segment(2) == "home-sections" && Request::segment(3) == "two"){?> class="active" <?php }?>><a href="<?= url('backend/home-sections/two');?>">Section two</a></li>
                                <?php }?>
                                <?php if(Roles::check('homesections.banners')){?>
                                <li <?php if(Request::segment(3) == "banners" || Request::segment(3) == "create-banner"){?> class="active" <?php }?>><a href="<?= url('backend/home-sections/banners');?>">Section two banners</a></li>
                                <?php }?>
                            </ul>
                        </li>
                        <?php }?>
                        
                        <?php if(Roles::check('ourpattern.index')){?>
                        <li <?php if(Request::segment(2) == "our-pattern"){?> class="active" <?php }?>>
                            <a href="<?= url('backend/our-pattern');?>"><i class="fa fa-image"></i> <span class="nav-label">Our pattern</span></a>
                        </li>
                        <?php }?>
                        
                        <?php if(Roles::check('aboutus.index')){?>
                        <li <?php if(Request::segment(2) == "about-us"){?> class="active" <?php }?>>
                            <a href="<?= url('backend/about-us');?>"><i class="fa fa-pencil"></i> <span class="nav-label">About us</span></a>
                        </li>
                        <?php }?>
                        
                        <?php if(Roles::check('ourdevelopment.index') || Roles::check('ourdevelopment.attachments')){?>
                        <li>
                            <a href="#"><i class="fa fa-pencil"></i> <span class="nav-label">Our development</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse <?php if(Request::segment(2) == "our-development"){?> in <?php }?>">
                                <?php if(Roles::check('ourdevelopment.index')){?>
                                <li <?php if(Request::segment(2) == "our-development" && Request::segment(3) == ""){?> class="active" <?php }?>><a href="<?= url('backend/our-development');?>">Our development</a></li>
                                <?php }?>
                                <?php if(Roles::check('ourdevelopment.attachments')){?>
                                <li <?php if(Request::segment(2) == "our-development" && Request::segment(3) == "attachments" || (Request::segment(3) == "create-attachments" || Request::segment(3) == "edit-attachments")){?> class="active" <?php }?>><a href="<?= url('backend/our-development/attachments');?>">Attachments</a></li>
                                <?php }?>
                            </ul>
                        </li>
                        <?php }?>
                        
                        <?php if(Roles::check('newsletter.index')){?>
                        <li <?php if(Request::segment(2) == "newsletter"){?> class="active" <?php }?>>
                            <a href="<?= url('backend/newsletter');?>"><i class="fa fa-pencil"></i> <span class="nav-label">Newsletter</span></a>
                        </li>
                        <?php }?>
                        
                        <?php if(Roles::check('events.index') || Roles::check('presskit.index') || Roles::check('media.index')|| Roles::check('press.index')){?>
                        <li>
                            <a href="#"><i class="fa fa-video-camera"></i> <span class="nav-label">Media Center</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse <?php if(Request::segment(2) == "events" || Request::segment(2) == "presskit" || Request::segment(2) == "media" || Request::segment(2) == "press"){?> in <?php }?>">
                            <?php if(Roles::check('events.index') || Roles::check('events.photos') || Roles::check('events.videos')){?>
                            <li>
                                <a href="#"><i class="fa fa-image"></i> <span class="nav-label">Gallery</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse <?php if(Request::segment(2) == "events"){?> in <?php }?>">
                                    <?php if(Roles::check('events.sections')){?>
                                    <li <?php if(Request::segment(2) == "events" && Request::segment(3) == "sections"){?> class="active" <?php }?>><a href="<?= url('backend/events/sections');?>">Sections</a></li>
                                    <?php }?>
                                    <?php if(Roles::check('events.index')){?>
                                    <li <?php if(Request::segment(2) == "events" && Request::segment(3) == ""){?> class="active" <?php }?>><a href="<?= url('backend/events');?>">Gallery</a></li>
                                    <?php }?>
                                    <?php if(Roles::check('events.videos')){?>
                                    <li <?php if(Request::segment(2) == "events" && Request::segment(3) == "videos" || (Request::segment(3) == "create-videos" || Request::segment(3) == "edit-videos")){?> class="active" <?php }?>><a href="<?= url('backend/events/videos');?>">Videos</a></li>
                                    <?php }?>
                                </ul>
                            </li>
                            <?php }?>
                                <?php if(Roles::check('presskit.index')){?>
                                <li <?php if(Request::segment(2) == "presskit"){?> class="active" <?php }?>><a href="<?= url('backend/presskit');?>">Press kit</a></li>
                                <?php }?>
                                <?php if(Roles::check('media.sections')){?>
                                    <li <?php if(Request::segment(2) == "media" && Request::segment(3) == "sections"){?> class="active" <?php }?>><a href="<?= url('backend/media/sections');?>">Media sections</a></li>
                                <?php }?>
                                <?php if(Roles::check('media.index')){?>
                                <li <?php if(Request::segment(2) == "media"){?> class="active" <?php }?>><a href="<?= url('backend/media');?>">Media</a></li>
                                <?php }?>
                                <?php if(Roles::check('press.index')){?>
                                <li <?php if(Request::segment(2) == "press"){?> class="active" <?php }?>><a href="<?= url('backend/press');?>">Press Release</a></li>
                                <?php }?>
                            </ul>
                        </li>
                        <?php }?>
                        
                        <?php if(Roles::check('contacts.index')){?>
                        <li <?php if(Request::segment(2) == "contacts"){?> class="active" <?php }?>>
                            <a href="<?= url('backend/contacts');?>"><i class="fa fa-envelope"></i> <span class="nav-label">Contacts</span></a>
                        </li>
                        <?php }?>
                        
                        <?php if(Roles::check('subscribers.index') || Roles::check('emailthemes.index')){?>
                        <li>
                            <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">Emails</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse <?php if(Request::segment(2) == "subscribers" || Request::segment(2) == "email-themes"){?> in <?php }?>">
                                <?php if(Roles::check('subscribers.index')){?>
                                <li <?php if(Request::segment(2) == "subscribers"){?> class="active" <?php }?>><a href="<?= url('backend/subscribers');?>">Subscribers</a></li>
                                <?php }?>
                                <?php if(Roles::check('emailthemes.index')){?>
                                <li <?php if(Request::segment(2) == "email-themes"){?> class="active" <?php }?>><a href="<?= url('backend/email-themes');?>">Email themes</a></li>
                                <?php }?>
                            </ul>
                        </li>
                        <?php }?>
                        
                        <?php if(Roles::check('settings.index')){?>
                        <li <?php if(Request::segment(2) == "settings"){?> class="active" <?php }?>>
                            <a href="<?= url('backend/settings');?>"><i class="fa fa-pencil"></i> <span class="nav-label">Settings</span></a>
                        </li>
                        <?php }?>
                        
                        <?php if(Roles::check('languages.index')){?>
                        <li <?php if(Request::segment(2) == "languages"){?> class="active" <?php }?>>
                            <a href="<?= url('backend/languages');?>"><i class="fa fa-language"></i> <span class="nav-label">Languages</span></a>
                        </li>
                        <?php }?>
                        
                        <?php if(Roles::check('setupmails.index') || Roles::check('pages.index') || Roles::check('roles.index') || Roles::check('backendusers.index')){?>
                        <li class="special_link">
                            <a href="#"><i class="fa fa-user-secret"></i> <span class="nav-label">Backend options</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse <?php if(Request::segment(2) == "setup-mails" || Request::segment(2) == "pages" || Request::segment(2) == "roles" || Request::segment(2) == "backend-users"){?> in <?php }?>">
                                <?php if(Roles::check('setupmails.index')){?>
                                <li <?php if(Request::segment(2) == "setup-mails"){?> class="landing_link" <?php }?>><a href="<?= url('backend/setup-mails');?>">Setup mails</a></li>
                                <?php }?>
                                <?php if(Roles::check('pages.index')){?>
                                <li <?php if(Request::segment(2) == "pages"){?> class="landing_link" <?php }?>><a href="<?= url('backend/pages');?>">Pages</a></li>
                                <?php }?>
                                <?php if(Roles::check('roles.index')){?>
                                <li <?php if(Request::segment(2) == "roles"){?> class="landing_link" <?php }?>><a href="<?= url('backend/roles');?>">Roles</a></li>
                                <?php }?>
                                <?php if(Roles::check('backendusers.index')){?>
                                <li <?php if(Request::segment(2) == "backend-users"){?> class="landing_link" <?php }?>><a href="<?= url('backend/backend-users');?>">Backend users</a></li>
                                <?php }?>

                            </ul>
                        </li>
                        <?php }?>
                        
                        <?php if(Roles::check('cpanel.index') || Roles::check('cpanel.accounts') || Roles::check('cpanel.emails') || Roles::check('cpanel.domains') || Roles::check('cpanel.subdomains')){?>
                        <li class="special_link">
                            <a href="#"><i class="fa fa-magic"></i> <span class="nav-label">Cpanel</span><span class="fa arrow"></span> </a>
                            <ul class="nav nav-second-level collapse <?php if(Request::segment(2) == "cpanel"){?> in <?php }?>">
                                <?php if(Roles::check('cpanel.index')){?>
                                <li <?php if(Request::segment(2) == "cpanel" && Request::segment(3) == ""){?> class="landing_link" <?php }?>><a href="<?= url('backend/cpanel');?>">Cpanel</a></li>
                                <?php }?>
                                <?php if(Roles::check('cpanel.accounts')){?>
                                <li <?php if(Request::segment(2) == "cpanel" && Request::segment(3) == "accounts"){?> class="landing_link" <?php }?>><a href="<?= url('backend/cpanel/accounts');?>">Accounts</a></li>
                                <?php }?>
                                <?php if(Roles::check('cpanel.domains')){?>
                                <li <?php if(Request::segment(2) == "cpanel" && Request::segment(3) == "domains"){?> class="landing_link" <?php }?>><a href="<?= url('backend/cpanel/domains');?>">Domains</a></li>
                                <?php }?>
                                <?php if(Roles::check('cpanel.subdomains')){?>
                                <li <?php if(Request::segment(2) == "cpanel" && Request::segment(3) == "sub-domains"){?> class="landing_link" <?php }?>><a href="<?= url('backend/cpanel/sub-domains');?>">Sub domains</a></li>
                                <?php }?>
                                <?php if(Roles::check('cpanel.emails')){?>
                                <li <?php if(Request::segment(2) == "cpanel" && Request::segment(3) == "emails"){?> class="landing_link" <?php }?>><a href="<?= url('backend/cpanel/emails');?>">Emails</a></li>
                                <?php }?>
                            </ul>
                        </li>
                        <?php }?>
                        
                    </ul>

                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                     <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="<?php echo \URL::Current().'#';?>"><i class="fa fa-bars"></i> </a>
<!--                            <form role="search" class="navbar-form-custom" action="">
                                <div class="form-group">
                                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                                </div>
                            </form>-->
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <span class="m-r-sm text-muted welcome-message">Welcome back <?= $_SESSION['name'];?></span>
                            </li>
                            <?php if(Roles::check('contacts.index')){?>
                            <li class="dropdown">
                                <?php
                                $contacts = \App\Models\Contacts::where('contacted_by',0)->orderBy('id', 'desc')->limit(7)->get();
                                ?>
                                <a class="dropdown-toggle count-info" <?php if($contacts->count() >=0){?> data-toggle="dropdown" href="#" <?php }else{?> <?php }?>>
                                    <i class="fa <?php if($contacts->count() >=0){?> fa-envelope <?php }else{?> fa-envelope-o <?php }?> "></i>  <span class="label label-warning"><?= $contacts->count();?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-messages">
                                    <?php foreach($contacts as $contact){?>
                                    <li>
                                        <div class="dropdown-messages-box">
                                            <?php if(Roles::check('contacts.writenotes')){?>
                                            <a href="<?= url('backend/contacts/write-notes/'.$contact->id);?>" class="pull-left">
                                                <!--<img alt="image" class="img-circle" src="img/a7.jpg">-->
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <?php }?>
                                            <div class="media-body">
                                                <small class="pull-right"><?= Helpers::CalculateTime($contact->created_at);?></small>
                                                <strong><?= $contact->name;?></strong> <?= $contact->phone;?> <strong><?= $contact->email;?></strong><br>
                                                <small class="text-muted"><?= Helpers::CalculateDate($contact->created_at)?> ago at <?= $contact->createdAt();?></small>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="divider"></li>
                                    <?php }?>
                                    
                                    <li>
                                        <div class="text-center link-block">
                                            <a href="<?= url('backend/contacts');?>">
                                                <strong>See all messages</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <?php }?>
                            <li>
                                <a href="<?php echo url('/backend/auth/logout'); ?>">
                                    <i class="fa fa-sign-out"></i> Sign out
                                </a>
                            </li>
                        </ul>

                    </nav>
                </div>
                
                @yield('content')

                <div class="footer">
                    <div class="pull-right">
                       Power By <strong><a target="_blank" href="http://www.media-sci.com">Mediasci</a></strong>
                   </div>
                   <div>
                       <strong>Copyright</strong> <a target="_blank" href="http://www.hydepark-eg.com">Hyde Park</a> &copy; 2016-2018
                   </div>
                </div>

            </div>
        </div>



        <!-- Mainly scripts -->
        <script src="./backend/js/jquery-ui-1.10.4.min.js"></script>
        <script src="./backend/js/bootstrap.min.js"></script>
        <script src="./backend/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="./backend/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="./backend/js/inspinia.js"></script>
        <script src="./backend/js/plugins/pace/pace.min.js"></script>
        <!-- Sweet alert -->
        <script src="./backend/js/plugins/sweetalert/sweetalert.min.js"></script>
        
        <!-- Chosen -->
        <script src="./backend/js/plugins/chosen/chosen.jquery.js"></script>

        <!-- JSKnob -->
        <script src="./backend/js/plugins/jsKnob/jquery.knob.js"></script>
        
        <!-- DataTable -->
        <script src="./backend/js/plugins/dataTables/datatables.min.js"></script>
    
        <!-- Toastr script -->
        <script src="./backend/js/plugins/toastr/toastr.min.js"></script>
        
        <!-- blueimp gallery -->
        <script src="./backend/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
        
         <!-- Jquery Validate -->
        <script src="./backend/js/plugins/validate/jquery.validate.min.js"></script>
        
         <!-- Select2 -->
        <script src="./backend/js/plugins/select2/select2.full.min.js"></script>
        
        <!-- Input Mask-->
        <script src="./backend/js/plugins/jasny/jasny-bootstrap.min.js"></script>

       <!-- Data picker -->
       <script src="./backend/js/plugins/datapicker/bootstrap-datepicker.js"></script>

       <!-- NouSlider -->
       <script src="./backend/js/plugins/nouslider/jquery.nouislider.min.js"></script>

       <!-- Switchery -->
       <script src="./backend/js/plugins/switchery/switchery.js"></script>

        <!-- IonRangeSlider -->
        <script src="./backend/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

        <!-- iCheck -->
        <script src="./backend/js/plugins/iCheck/icheck.min.js"></script>

        <!-- MENU -->
        <script src="./backend/js/plugins/metisMenu/jquery.metisMenu.js"></script>

        <!-- Color picker -->
        <script src="./backend/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

        <!-- Clock picker -->
        <script src="./backend/js/plugins/clockpicker/clockpicker.js"></script>

        <!-- Image cropper -->
        <script src="./backend/js/plugins/cropper/cropper.min.js"></script>

        <!-- Date range use moment.js same as full calendar plugin -->
        <script src="./backend/js/plugins/fullcalendar/moment.min.js"></script>

        <!-- Date range picker -->
        <script src="./backend/js/plugins/daterangepicker/daterangepicker.js"></script>
        
        <!-- TouchSpin -->
        <script src="./backend/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>
        
        <!-- Bootstrap Tour -->
        <script src="./backend/js/plugins/bootstrapTour/bootstrap-tour.min.js"></script>
        
     <script>
        $(document).ready(function(){

            $("#todo, #inprogress, #completed").sortable({
                connectWith: ".connectList",
                update: function( event, ui ) {

                    var todo = $( "#todo" ).sortable( "toArray" );
                    var inprogress = $( "#inprogress" ).sortable( "toArray" );
                    var completed = $( "#completed" ).sortable( "toArray" );
                    $('.output').html("ToDo: " + window.JSON.stringify(todo) + "<br/>" + "In Progress: " + window.JSON.stringify(inprogress) + "<br/>" + "Completed: " + window.JSON.stringify(completed));
                }
            }).disableSelection();

        });
    </script>
         <!-- Page-Level Scripts -->
         <script>
            $(document).ready(function(){
            
                 /* Init DataTables */
                 $('.example1').DataTable({
                  "paging": false,
                  "lengthChange": false,
                  "searching": true,
                  "ordering": false,
                  "info": false,
                  "autoWidth": true
                });
            
                $('.example2').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": false,
                    "autoWidth": true
                });
                $('.example3').DataTable({
                  "paging": false,
                  "lengthChange": true,
                  "searching": false,
                  "ordering": false,
                  "info": false,
                  "autoWidth": true
                });
                $('.example4').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": true,
                    "info": false,
                    "autoWidth": true
                });
            
                /* Slider2 */
                $(".select2").select2();
                $(".select2clear").select2({
                    placeholder: "",
                    allowClear: true
                });
                $(".select2placeholder").select2({
                    placeholder: "Select one",
                    allowClear: true
                });
                
                var config = {
                    '.chosen-select'           : {},
                    '.chosen-select-deselect'  : {allow_single_deselect:true},
                    '.chosen-select-no-single' : {disable_search_threshold:10},
                    '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                    '.chosen-select-width'     : {width:"95%"}
                    }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }
                
                /* Crop Image */
                var $image = $(".image-crop > img")
                $($image).cropper({
                    aspectRatio: 1.618,
                    preview: ".img-preview",
                    done: function(data) {
                        // Output the result data for cropping image.
                    }
                });

                var $inputImage = $("#inputImage");
                if (window.FileReader) {
                    $inputImage.change(function() {
                        var fileReader = new FileReader(),
                                files = this.files,
                                file;

                        if (!files.length) {
                            return;
                        }

                        file = files[0];

                        if (/^image\/\w+$/.test(file.type)) {
                            fileReader.readAsDataURL(file);
                            fileReader.onload = function () {
                                $inputImage.val("");
                                $image.cropper("reset", true).cropper("replace", this.result);
                            };
                        } else {
                            showMessage("Please choose an image file.");
                        }
                    });
                } else {
                    $inputImage.addClass("hide");
                }

                $("#download").click(function() {
                    window.open($image.cropper("getDataURL"));
                });

                $("#zoomIn").click(function() {
                    $image.cropper("zoom", 0.1);
                });

                $("#zoomOut").click(function() {
                    $image.cropper("zoom", -0.1);
                });

                $("#rotateLeft").click(function() {
                    $image.cropper("rotate", 45);
                });

                $("#rotateRight").click(function() {
                    $image.cropper("rotate", -45);
                });

                $("#setDrag").click(function() {
                    $image.cropper("setDragMode", "crop");
                });

                /* Datepicker */
                $('#data_1 .input-group.date').datepicker({
                    todayBtn: "linked",
                    keyboardNavigation: false,
                    forceParse: false,
                    calendarWeeks: true,
                    autoclose: true
                });
                
                $('#data_2 .input-group.date').datepicker({
                    startView: 1,
                    todayBtn: "linked",
                    keyboardNavigation: false,
                    forceParse: false,
                    autoclose: true,
                    format: "yyyy/mm/dd"
                });

                $('#data_3 .input-group.date').datepicker({
                    startView: 2,
                    todayBtn: "linked",
                    keyboardNavigation: false,
                    forceParse: false,
                    autoclose: true
                });

                $('#data_4 .input-group.date').datepicker({
                    minViewMode: 1,
                    keyboardNavigation: false,
                    forceParse: false,
                    autoclose: true,
                    todayHighlight: true
                });

                $('#data_5 .input-daterange').datepicker({
                    keyboardNavigation: false,
                    forceParse: false,
                    autoclose: true
                });
                
                $('input[name="daterange"]').daterangepicker();

                $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

                $('#reportrange').daterangepicker({
                    format: 'MM/DD/YYYY',
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment(),
                    minDate: '01/01/2012',
                    maxDate: '12/31/2015',
                    dateLimit: { days: 60 },
                    showDropdowns: true,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: true,
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    opens: 'right',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default',
                    separator: ' to ',
                    locale: {
                        applyLabel: 'Submit',
                        cancelLabel: 'Cancel',
                        fromLabel: 'From',
                        toLabel: 'To',
                        customRangeLabel: 'Custom',
                        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                        firstDay: 1
                    }
                }, function(start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                });
                
                /* Colokpicker */
                $('.clockpicker').clockpicker();

                /* Switch */
                var elem = document.querySelector('.js-switch');
                var switchery = new Switchery(elem, { color: '#1AB394' });

                var elem_2 = document.querySelector('.js-switch_2');
                var switchery_2 = new Switchery(elem_2, { color: '#ED5565' });

                var elem_3 = document.querySelector('.js-switch_3');
                var switchery_3 = new Switchery(elem_3, { color: '#1AB394' });

                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'
                });
                
                /* Colorpicker */
                $('.colorpicker').colorpicker();
                
                var divStyle = $('.back-change')[0].style;
                $('#demo_apidemo').colorpicker({
                    color: divStyle.backgroundColor
                }).on('changeColor', function(ev) {
                            divStyle.backgroundColor = ev.color.toHex();
                        });
                        
                /* Touchspain */
                $(".touchspin1").TouchSpin({
                    buttondown_class: 'btn btn-white',
                    buttonup_class: 'btn btn-white'
                });

                $(".touchspin2").TouchSpin({
                    min: 0,
                    max: 100,
                    step: 0.1,
                    decimals: 2,
                    boostat: 5,
                    maxboostedstep: 10,
                    postfix: '%',
                    buttondown_class: 'btn btn-white',
                    buttonup_class: 'btn btn-white'
                });

                $(".touchspin3").TouchSpin({
                    verticalbuttons: true,
                    buttondown_class: 'btn btn-white',
                    buttonup_class: 'btn btn-white'
                });
                
                /* Range Slider */
                $("#ionrange_1").ionRangeSlider({
                    min: 0,
                    max: 5000,
                    type: 'double',
                    prefix: "$",
                    maxPostfix: "+",
                    prettify: false,
                    hasGrid: true
                });

                $("#ionrange_2").ionRangeSlider({
                    min: 0,
                    max: 10,
                    type: 'single',
                    step: 0.1,
                    postfix: " carats",
                    prettify: false,
                    hasGrid: true
                });

                $("#ionrange_3").ionRangeSlider({
                    min: -50,
                    max: 50,
                    from: 0,
                    postfix: "°",
                    prettify: false,
                    hasGrid: true
                });

                $("#ionrange_4").ionRangeSlider({
                    values: [
                        "January", "February", "March",
                        "April", "May", "June",
                        "July", "August", "September",
                        "October", "November", "December"
                    ],
                    type: 'single',
                    hasGrid: true
                });

                $("#ionrange_5").ionRangeSlider({
                    min: 10000,
                    max: 100000,
                    step: 100,
                    postfix: " km",
                    from: 55000,
                    hideMinMax: true,
                    hideFromTo: false
                });

                $(".dial").knob();

                $("#basic_slider").noUiSlider({
                    start: 40,
                    behaviour: 'tap',
                    connect: 'upper',
                    range: {
                        'min':  20,
                        'max':  80
                    }
                });

                $("#range_slider").noUiSlider({
                    start: [ 40, 60 ],
                    behaviour: 'drag',
                    connect: true,
                    range: {
                        'min':  20,
                        'max':  80
                    }
                });

                $("#drag-fixed").noUiSlider({
                start: [ 40, 60 ],
                behaviour: 'drag-fixed',
                connect: true,
                range: {
                    'min':  20,
                    'max':  80
                }
            });
            
                /* Sorting */
                $("#todo, #inprogress, #completed").sortable({
                connectWith: ".connectList",
                update: function( event, ui ) {

                    var todo = $( "#todo" ).sortable( "toArray" );
                    var inprogress = $( "#inprogress" ).sortable( "toArray" );
                    var completed = $( "#completed" ).sortable( "toArray" );
                    $('.output').html("ToDo: " + window.JSON.stringify(todo) + "<br/>" + "In Progress: " + window.JSON.stringify(inprogress) + "<br/>" + "Completed: " + window.JSON.stringify(completed));
                }
                }).disableSelection();


            });
            
            


         </script>

    </body>
</html>