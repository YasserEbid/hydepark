@extends('backend.layout')
@section('title','Home page')
@section('content')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>
<?php
   if(session('error')!='')
       $error = session('error') ;
?>
<?php if(!empty($error)){?>

<script type="text/javascript">
$(document).ready(function(){
    var errors = <?= json_encode($error); ?>;
        swal({
            title: "Error",
            text: errors,
            type: "error",
            html: true,
            showConfirmButton: true
        });
});
</script>

<?php } ?>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Home page</h2>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right">Monthly</span>
                                <h5>Income</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">40 886,200</h1>
                                <small>Total income</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">Annual</span>
                                <h5>Orders</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">275,800</h1>
                                <small>New orders</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Today</span>
                                <h5>Vistits</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">106,120</h1>
                                <small>New visits</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right">Low value</span>
                                <h5>User activity</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">80,600</h1>
                                <small>In first month</small>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Messages</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content ibox-heading">
                                <h3><i class="fa fa-envelope-o"></i> New messages</h3>
                                <small><i class="fa fa-tim"></i> You have 22 new messages and 16 waiting in draft folder.</small>
                            </div>
                            <div class="ibox-content">
                                <div class="feed-activity-list">

                                    <div class="feed-element">
                                        <a href="#">
                                            <div>
                                                <strong>Anna Legend</strong>
                                                <div>All the Lorem Ipsum generators on the Internet tend to repeat </div>
                                                <small class="text-muted"><i class="fa fa-clock-o"></i> Yesterday 8:48 pm - 10.06.2014</small>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="feed-element">
                                        <a href="#">
                                            <div>
                                                <strong>Anna Legend</strong>
                                                <div>All the Lorem Ipsum generators on the Internet tend to repeat </div>
                                                <small class="text-muted"><i class="fa fa-clock-o"></i> Yesterday 8:48 pm - 10.06.2014</small>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="feed-element">
                                        <a href="#">
                                            <div>
                                                <strong>Anna Legend</strong>
                                                <div>All the Lorem Ipsum generators on the Internet tend to repeat </div>
                                                <small class="text-muted"><i class="fa fa-clock-o"></i> Yesterday 8:48 pm - 10.06.2014</small>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>User project list</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-hover no-margins">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Date</th>
                                            <th>User</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><small>Pending...</small></td>
                                            <td><i class="fa fa-clock-o"></i> 11:20pm</td>
                                            <td>Samantha</td>
                                            <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                        </tr>
                                        <tr>
                                            <td><span class="label label-warning">Canceled</span> </td>
                                            <td><i class="fa fa-clock-o"></i> 10:40am</td>
                                            <td>Monica</td>
                                            <td class="text-navy"> <i class="fa fa-level-up"></i> 66% </td>
                                        </tr>
                                        <tr>
                                            <td><small>Pending...</small> </td>
                                            <td><i class="fa fa-clock-o"></i> 01:30pm</td>
                                            <td>John</td>
                                            <td class="text-navy"> <i class="fa fa-level-up"></i> 54% </td>
                                        </tr>
                                        <tr>
                                            <td><small>Pending...</small> </td>
                                            <td><i class="fa fa-clock-o"></i> 02:20pm</td>
                                            <td>Agnes</td>
                                            <td class="text-navy"> <i class="fa fa-level-up"></i> 12% </td>
                                        </tr>
                                        <tr>
                                            <td><small>Pending...</small> </td>
                                            <td><i class="fa fa-clock-o"></i> 09:40pm</td>
                                            <td>Janet</td>
                                            <td class="text-navy"> <i class="fa fa-level-up"></i> 22% </td>
                                        </tr>
                                        <tr>
                                            <td><span class="label label-primary">Completed</span> </td>
                                            <td><i class="fa fa-clock-o"></i> 04:10am</td>
                                            <td>Amelia</td>
                                            <td class="text-navy"> <i class="fa fa-level-up"></i> 66% </td>
                                        </tr>
                                        <tr>
                                            <td><small>Pending...</small> </td>
                                            <td><i class="fa fa-clock-o"></i> 12:08am</td>
                                            <td>Damian</td>
                                            <td class="text-navy"> <i class="fa fa-level-up"></i> 23% </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>  

            </div>

        </div>
    </div>



</div>




@stop()