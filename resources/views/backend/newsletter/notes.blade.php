@extends('backend.layout')
@section('title','Write notes | Newsletter')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
if(!isset($note))
    $note = new \App\Models\Newsletter;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Write notes</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <?php if(Roles::check('newsletter.index')){?>
            <li>
                <a href="<?= url('backend/newsletter');?>">Newsletter</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Write notes</strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">

                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Notes & Newsletter data</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                <div class="col-lg-5">
                                    <dl class="dl-horizontal">

                                        <dt>Name:</dt> <dd>{{$note->name}}</dd>
                                        <dt>Phone:</dt> <dd>{{$note->phone}}</dd>
                                        <dt>Email:</dt> <dd>{{$note->email}}</dd>
                                        <dt>Type:</dt> <dd>{{$note->type}}</dd>
                                        <dt>Code:</dt> <dd>{{$note->code}}</dd>

                                    </dl>
                                </div>
                                <div class="col-lg-7" id="cluster_info">
                                    <dl class="dl-horizontal">

                                        <dt>Unit number:</dt> <dd>{{$note->unit_number}}</dd>
                                        <dt>Message date:</dt> <dd><?= $note->createdAt(); ?></dd>
                                        <dt>Contacted date:</dt> <dd><?= $note->updatedAt(); ?></dd>
                                        <dt>Contacted by:</dt> <dd><?php if(is_object($note->admin)){echo $note->admin->name;} ?></dd>

                                    </dl>
                                </div>
                            </div>  
                    <div class="hr-line-dashed"></div>
                    <label class="control-label">Message:</label>
                    <p>{{$note->message}}</p>
                    
                    <div class="hr-line-dashed"></div>

<form method="post" class="form-horizontal" id="form">
    
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Write notes</label>
            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_css();?>
            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_editor('note','note',$note->note);?>
            <?= \App\Http\Controllers\Common\Froala\FroalaEditor::init_js();?>
        </div>
    </div>

    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                note: {
                    required: true,
                    minlength: 3
                }
            }
        });
    });
</script>

                            </div>
                        </div>
                    </div>



                </div>

            </div>

        </div>
    </div>



</div>



@stop()