@extends('backend.layout')
@section('title','Newsletter')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Backend\Roles\Roles;
use App\Http\Controllers\Common\Components;
use App\Http\Controllers\Common\Helpers;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Newsletter</h2>
        <ol class="breadcrumb">
            <?php if(Roles::check('home.index')){?>
            <li>
                <a href="<?= url('backend/home');?>">Home page</a>
            </li>
            <?php }?>
            <li class="active">
                <strong>Newsletter</strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">
                    
                     <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5>Newsletter data</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                    <?php if(Roles::check('newsletter.export')){?>
                    <div class="btn-group">
                         <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Export <i class="fa fa-download"></i> <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="<?= url('backend/newsletter/export');?>"><i class="fa fa-file-excel-o"></i> File excel </a></li>
                            <!--<li><a href="<?= url('backend/newsletter/export');?>"><i class="fa fa-file-pdf-o"></i> File PDF </a></li>-->
                        </ul>
                    </div>
                    <?php }?>
                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover example1">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Type</th>
                            <th>Code</th>
                            <th>Unit number</th>
                            <th>Message date</th>
                            <?php if(Roles::check('newsletter.writenotes') || Roles::check('newsletter.preview') || Roles::check('newsletter.delete')){?>
                            <th>Settings</th>
                            <?php }?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($newsletters as $newsletter){?>
                        <tr>
                            <td>{{$newsletter->name}}</td>
                            <td>{{$newsletter->phone}}</td>
                            <td>{{$newsletter->email}}</td>
                            <td><?php if($newsletter->type == 1){echo 'Homeowner';}else{echo 'Interested In Hyde Park';} ;?></td>
                            <td>{{$newsletter->code}}</td>
                            <td>{{$newsletter->unit_number}}</td>
                            <td>{{$newsletter->createdAt()}}</td>
                            <?php if(Roles::check('newsletter.writenotes') || Roles::check('newsletter.preview') || Roles::check('mediacenter.delete')){?>
                            <td>
                                <?php if(Roles::check('newsletter.writenotes')){?>
                                <a href="<?= url('backend/newsletter/write-notes/'.$newsletter->id);?>" class=" btn btn-xs btn-outline btn-primary"><i class="fa fa-pencil"></i> Write notes</a>
                                <?php }?>
                                
                                <?php if(Roles::check('newsletter.preview')){?>
                                <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal<?= $newsletter->id;?>"><i class="fa fa-twitch"></i> Preview</button>
                                <div class="modal inmodal" id="myModal<?= $newsletter->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-laptop modal-icon"></i>
                                                <h4 class="modal-title">Preview all data</h4>
                                            </div>
                                            <div class="modal-body">
                                                <blockquote>
                                                    <p>
                                                        <strong style="font-size: 18px;">Name</strong> : {{$newsletter->name}}
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Phone</strong> : {{$newsletter->phone}}
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Email</strong> : {{$newsletter->email}}
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Type</strong> : <?php if($newsletter->type == 1){echo 'Homeowner';}else{echo 'Interested In Hyde Park';} ;?>
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Code</strong> : {{$newsletter->code}}
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Unit number</strong> : {{$newsletter->unit_number}}
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Message date</strong> : {{$newsletter->createdAt()}}
                                                    </p>
                                                    
                                                    <p>
                                                        <strong style="font-size: 18px;">Note</strong> : {{$newsletter->note}}
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Contacted date</strong> : {{$newsletter->updatedAt()}}
                                                    </p>

                                                    <p>
                                                        <strong style="font-size: 18px;">Contacted by</strong> : <?php if(is_object($newsletter->admin)){echo $newsletter->admin->name;} ?>
                                                    </p>
                                                </blockquote>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                                
                                <?php if(Roles::check('newsletter.delete')){?>
                                <?= Components::deleteRow($newsletter->id, url('backend/newsletter/delete'));?>
                                <?php }?>
                                
                            </td>
                            <?php }?>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
                    <?= $newsletters->render();?>
                </div>
                </div>
            </div>
        </div>
                    
                    

                </div>

            </div>

        </div>
    </div>



</div>



@stop()