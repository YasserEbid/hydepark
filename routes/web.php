<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
//Event::listen('illuminate.query', function($query)
//{
//    var_dump($query);
//});
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */


/*
  |--------------------------------------------------------------------------
  | Common Routes
  |--------------------------------------------------------------------------
  |
 */
Route::get('controllerss', function() {
    $controllers = require_once base_path('vendor/composer/autoload_classmap.php');
    $controllers = array_keys($controllers);
    $controllers = array_filter($controllers, function ($controller) {
        return strpos($controller, 'App\Http\Controllers\Front') !== false;
    });
    $controllers = array_map(function ($controller) {
        return $controller;
    }, $controllers);

    $notincluddedmethods = ['__construct', 'middleware', 'getMiddleware', 'callAction', '__call'];
    foreach ($controllers as $class) {
        $methods = get_class_methods($class);
        
        $controller_name_complete = str_replace('App\Http\Controllers\Front\\', '', $class);
        $controller_name = str_replace('Controller', '', $controller_name_complete);
        $controller_name_array = preg_split('/(?=[A-Z])/', $controller_name, -1, PREG_SPLIT_NO_EMPTY);
        $cn = '/';
        foreach ($controller_name_array as $one) {
            $cn .= strtolower($one);
            if (array_search($one, $controller_name_array) != count($controller_name_array) - 1) {
                $cn .= '-';
            }
        }

        foreach ($methods as $method) {
            if (!in_array($method, $notincluddedmethods)) {
                $function = preg_split('/(?=[A-Z])/', $method, -1, PREG_SPLIT_NO_EMPTY);
                if (in_array($function[0], ['any', 'get', 'post'])) {
                    $route_name = '/';
                    for ($r = 1; $r < count($function); $r++) {
                        $route_name .= strtolower($function[$r]);
                        if (array_search($function[$r], $function) != count($function) - 1) {
                            $route_name .= '-';
                        } else {
                            if (strpos($route_name, "/edit") !== false || strpos($route_name, "/delete") !== false) {
                                $route_name .= "/{id}";
                            }
                        }
                        $route_name = ($route_name == "/index")?'':$route_name;
                    }
                    $route = "Route::" . $function[0] . "('" . $cn . $route_name . "','" . $controller_name_complete . "@" . $method . "');";
                    echo $route . '<br />';
                }
            }
        }
    }
});

Route::any('/ajax/upload', 'Common\AjaxController@anyUpload');
Route::any('/ajax/file-upload', 'Common\AjaxController@anyUploadFile');
Route::any('/ajax/editorupload', 'Common\AjaxController@anyEditorupload');
Route::any('/ajax/removeimage', 'Common\AjaxController@anyRemoveimage');
Route::any('/ajax/browse', 'Common\AjaxController@anyBrowse');


/*
  |--------------------------------------------------------------------------
  | Test Routes
  |--------------------------------------------------------------------------
  |
 */

/*
  |--------------------------------------------------------------------------
  | Back End Routes
  |--------------------------------------------------------------------------
  |
 */

Route::group(['namespace' => 'Backend', 'middleware' => ['web'], 'prefix' => '/backend'], function() {
//
//    Route::controller('/sliders', 'SlidersController');
//    Route::controller('/home-sections', 'HomeSectionsController');
//    Route::controller('/about-us', 'AboutUsController');
//    Route::controller('/our-pattern', 'OurPatternController');
//    Route::controller('/our-development', 'OurDevelopmentController');
//    Route::controller('/newsletter', 'NewsletterController');
//    Route::controller('/events', 'EventsController');
//    Route::controller('/presskit', 'PresskitController');
//    Route::controller('/media', 'MediaController');
//    Route::controller('/press', 'PressController');
//    Route::controller('/subscribers', 'SubscribersController');
//    Route::controller('/email-themes', 'EmailThemesController');
//    Route::controller('/languages', 'LanguagesController');
//    Route::controller('/settings', 'SettingsController');
//    Route::controller('/contacts', 'ContactsController');
//    Route::controller('/registrations', 'RegistrationsController');
//    Route::controller('/mailbox', 'MailboxController');
//    Route::controller('/setup-mails', 'SetupMailsController');
//    Route::controller('/pages', 'PagesController');
//    Route::controller('/roles', 'RolesController');
//    Route::controller('/backend-users', 'BackendUsersController');
//    Route::controller('/cpanel', 'CpanelController');
//    Route::controller('/home', 'HomeController');
//    Route::controller('/auth', 'AuthController');
//    
Route::any('/about-us', 'AboutUsController@anyIndex');
Route::any('/about-us/edit', 'AboutUsController@anyEdit');
Route::any('/backend-users', 'BackendUsersController@anyIndex');
Route::any('/backend-users/create', 'BackendUsersController@anyCreate');
Route::any('/backend-users/edit/{id}', 'BackendUsersController@anyEdit');
Route::any('/backend-users/delete/{id}', 'BackendUsersController@anyDelete');
Route::any('/backend-users/activated', 'BackendUsersController@anyActivated');
Route::any('/backend-users/profile', 'BackendUsersController@anyProfile');
Route::any('/contacts', 'ContactsController@anyIndex');
Route::any('/contacts/export', 'ContactsController@anyExport');
Route::any('/contacts/write-notes', 'ContactsController@anyWriteNotes');
Route::any('/contacts/delete/{id}', 'ContactsController@anyDelete');
Route::any('/cpanel', 'CpanelController@anyIndex');
Route::any('/cpanel/create-cpanel', 'CpanelController@anyCreateCpanel');
Route::any('/cpanel/edit-cpanel', 'CpanelController@anyEditCpanel');
Route::any('/cpanel/delete-cpanel', 'CpanelController@anyDeleteCpanel');
Route::any('/cpanel/accounts', 'CpanelController@anyAccounts');
Route::any('/cpanel/emails', 'CpanelController@anyEmails');
Route::any('/cpanel/create-email', 'CpanelController@anyCreateEmail');
Route::any('/cpanel/change-password-email', 'CpanelController@anyChangePasswordEmail');
Route::any('/cpanel/edit-qouta/{id}', 'CpanelController@anyEditQouta');
Route::any('/cpanel/delete-email/{id}', 'CpanelController@anyDeleteEmail');
Route::any('/email-themes', 'EmailThemesController@anyIndex');
Route::any('/email-themes/create', 'EmailThemesController@anyCreate');
Route::any('/email-themes/edit/{id}', 'EmailThemesController@anyEdit');
Route::any('/email-themes/delete/{id}', 'EmailThemesController@anyDelete');
Route::any('/events', 'EventsController@anyIndex');
Route::any('/events/create', 'EventsController@anyCreate');
Route::any('/events/edit/{id}', 'EventsController@anyEdit');
Route::any('/events/delete/{id}', 'EventsController@anyDelete');
Route::any('/events/photos', 'EventsController@anyPhotos');
Route::any('/events/create-photos', 'EventsController@anyCreatePhotos');
Route::any('/events/edit-photos/{id}', 'EventsController@anyEditPhotos');
Route::any('/events/delete-photos/{id}', 'EventsController@anyDeletePhotos');
Route::any('/events/videos', 'EventsController@anyVideos');
Route::any('/events/create-videos', 'EventsController@anyCreateVideos');
Route::any('/events/edit-videos/{id}', 'EventsController@anyEditVideos');
Route::any('/events/delete-videos/{id}', 'EventsController@anyDeleteVideos');
Route::any('/events/sections', 'EventsController@anySections');
Route::any('/events/create-section', 'EventsController@anyCreateSection');
Route::any('/events/edit-section/{id}', 'EventsController@anyEditSection');
Route::any('/events/delete-section/{id}', 'EventsController@anyDeleteSection');
Route::any('/home-sections', 'HomeSectionsController@anyIndex');
Route::any('/home-sections/two', 'HomeSectionsController@anyTwo');
Route::any('/home-sections/edit/{id}', 'HomeSectionsController@anyEdit');
Route::any('/home-sections/banners', 'HomeSectionsController@anyBanners');
Route::any('/home-sections/create-banner', 'HomeSectionsController@anyCreateBanner');
Route::any('/home-sections/edit-banner/{id}', 'HomeSectionsController@anyEditBanner');
Route::any('/home-sections/delete/{id}', 'HomeSectionsController@anyDelete');
Route::any('/languages', 'LanguagesController@anyIndex');
Route::any('/languages/create', 'LanguagesController@anyCreate');
Route::any('/languages/edit/{id}', 'LanguagesController@anyEdit');
Route::any('/languages/delete/{id}', 'LanguagesController@anyDelete');
Route::any('/languages/activated', 'LanguagesController@anyActivated');
Route::any('/languages/defaulting', 'LanguagesController@anyDefaulting');
Route::any('/mailbox', 'MailboxController@anyIndex');
Route::any('/media', 'MediaController@anyIndex');
Route::any('/media/create', 'MediaController@anyCreate');
Route::any('/media/edit/{id}', 'MediaController@anyEdit');
Route::any('/media/delete/{id}', 'MediaController@anyDelete');
Route::any('/media/sections', 'MediaController@anySections');
Route::any('/media/create-section', 'MediaController@anyCreateSection');
Route::any('/media/edit-section/{id}', 'MediaController@anyEditSection');
Route::any('/media/delete-section/{id}', 'MediaController@anyDeleteSection');
Route::any('/newsletter', 'NewsletterController@anyIndex');
Route::any('/newsletter/export', 'NewsletterController@anyExport');
Route::any('/newsletter/write-notes', 'NewsletterController@anyWriteNotes');
Route::any('/newsletter/delete/{id}', 'NewsletterController@anyDelete');
Route::any('/our-development', 'OurDevelopmentController@anyIndex');
Route::any('/our-development/create', 'OurDevelopmentController@anyCreate');
Route::any('/our-development/edit/{id}', 'OurDevelopmentController@anyEdit');
Route::any('/our-development/attachments', 'OurDevelopmentController@anyAttachments');
Route::any('/our-development/create-attachments', 'OurDevelopmentController@anyCreateAttachments');
Route::any('/our-development/edit-attachments/{id}', 'OurDevelopmentController@anyEditAttachments');
Route::any('/our-development/delete-attachments/{id}', 'OurDevelopmentController@anyDeleteAttachments');
Route::any('/our-pattern', 'OurPatternController@anyIndex');
Route::any('/our-pattern/create', 'OurPatternController@anyCreate');
Route::any('/our-pattern/edit/{id}', 'OurPatternController@anyEdit');
Route::any('/our-pattern/delete/{id}', 'OurPatternController@anyDelete');
Route::any('/pages', 'PagesController@anyIndex');
Route::any('/pages/create', 'PagesController@anyCreate');
Route::any('/pages/edit/{id}', 'PagesController@anyEdit');
Route::any('/pages/delete/{id}', 'PagesController@anyDelete');
Route::any('/pages/page-actions', 'PagesController@anyPageActions');
Route::any('/pages/create-page-actions', 'PagesController@anyCreatePageActions');
Route::any('/pages/edit-page-actions/{id}', 'PagesController@anyEditPageActions');
Route::any('/pages/delete-page-actions/{id}', 'PagesController@anyDeletePageActions');
Route::any('/press', 'PressController@anyIndex');
Route::any('/press/create', 'PressController@anyCreate');
Route::any('/press/edit/{id}', 'PressController@anyEdit');
Route::any('/press/delete/{id}', 'PressController@anyDelete');
Route::any('/presskit', 'PresskitController@anyIndex');
Route::any('/presskit/create', 'PresskitController@anyCreate');
Route::any('/presskit/edit/{id}', 'PresskitController@anyEdit');
Route::any('/presskit/delete/{id}', 'PresskitController@anyDelete');
Route::any('/registrations', 'RegistrationsController@anyIndex');
Route::any('/registrations/export', 'RegistrationsController@anyExport');
Route::any('/registrations/write-notes', 'RegistrationsController@anyWriteNotes');
Route::any('/registrations/delete/{id}', 'RegistrationsController@anyDelete');
Route::any('/roles', 'RolesController@anyIndex');
Route::any('/roles/create', 'RolesController@anyCreate');
Route::any('/roles/edit/{id}', 'RolesController@anyEdit');
Route::any('/roles/delete/{id}', 'RolesController@anyDelete');
Route::any('/roles/role-actions', 'RolesController@anyRoleActions');
Route::any('/settings', 'SettingsController@anyIndex');
Route::any('/setup-mails', 'SetupMailsController@anyIndex');
Route::any('/setup-mails/create', 'SetupMailsController@anyCreate');
Route::any('/setup-mails/edit/{id}', 'SetupMailsController@anyEdit');
Route::any('/setup-mails/activated', 'SetupMailsController@anyActivated');
Route::any('/setup-mails/delete/{id}', 'SetupMailsController@anyDelete');
Route::any('/setup-mails/mails-domain', 'SetupMailsController@anyMailsDomain');
Route::any('/setup-mails/gmail', 'SetupMailsController@anyGmail');
Route::any('/setup-mails/yahoo', 'SetupMailsController@anyYahoo');
Route::any('/setup-mails/outlook', 'SetupMailsController@anyOutlook');
Route::any('/sliders', 'SlidersController@anyIndex');
Route::any('/sliders/create', 'SlidersController@anyCreate');
Route::any('/sliders/delete/{id}', 'SlidersController@anyDelete');
Route::any('/subscribers', 'SubscribersController@anyIndex');
Route::any('/subscribers/send-email', 'SubscribersController@anySendEmail');
Route::any('/subscribers/delete/{id}', 'SubscribersController@anyDelete');
Route::any('/subscribers/delete-un-follower/{id}', 'SubscribersController@anyDeleteUnFollower');
Route::any('/subscribers/activated', 'SubscribersController@anyActivated');
Route::any('/auth', 'AuthController@anyIndex');
Route::any('/auth/login', 'AuthController@anyLogin');
Route::any('/auth/logout', 'AuthController@anyLogout');
Route::any('/auth/forgot-password', 'AuthController@anyForgotPassword');
Route::any('/auth/lockedscreen', 'AuthController@anyLockedscreen');
Route::any('/home', 'HomeController@anyIndex');
});

/*
  |--------------------------------------------------------------------------
  | Front End Routes
  |--------------------------------------------------------------------------
  |
 */

Route::group(['namespace' => 'Front', 'middleware' => ['web']], function() {

//    Route::controller('search','SearchController');
//    Route::controller('contact-us','ContactUsController');
//    Route::controller('newsletter','NewsletterController');
//    Route::controller('/gallery','GalleryController');
//    Route::controller('/presskit','PresskitController');
//    Route::controller('/media','MediaController');
//    Route::controller('/press','PressController');
//    Route::controller('construction-viewer','ConstructionProgressViewerController');
//    Route::get('our-development/masterplan','OurDevelopmentController@anyMasterplan');
//    Route::controller('our-development/{name}','OurDevelopmentController');
//    Route::controller('about-us','AboutUsController');
//    Route::controller('/','HomeController');

    Route::any('/about-us', 'AboutUsController@anyIndex');
    Route::any('/about-us/our-pattern', 'AboutUsController@anyOurPattern');
    Route::get('/about-us/more-photos', 'AboutUsController@getMorePhotos');
    Route::any('/construction-viewer', 'ConstructionProgressViewerController@anyIndex');
    Route::any('/contact-us', 'ContactUsController@anyIndex');
    Route::any('/gallery', 'GalleryController@anyIndex');
    Route::get('/gallery/events', 'GalleryController@getEvents');
    Route::get('/gallery/name-events', 'GalleryController@getNameEvents');
    Route::get('/gallery/videos', 'GalleryController@getVideos');
    Route::any('/media', 'MediaController@anyIndex');
    Route::get('/media/more', 'MediaController@getMore');
    Route::any('/newsletter', 'NewsletterController@anyIndex');
    Route::any('/our-development/{name}', 'OurDevelopmentController@anyIndex');
    Route::any('/our-development/masterplan', 'OurDevelopmentController@anyMasterplan');
    Route::any('/press', 'PressController@anyIndex');
    Route::get('/press/more', 'PressController@getMore');
    Route::any('/presskit', 'PresskitController@anyIndex');
    Route::get('/presskit/more', 'PresskitController@getMore');
    Route::any('/search', 'SearchController@anyIndex');
    Route::any('/', 'HomeController@anyIndex');
});
